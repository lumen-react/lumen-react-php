<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

class ResourceBindingOptions {

	private $isRequest = false;
	private $populatedKeys = [];

	/**
	 * ResourceBindingOptions constructor.
	 * @param array $options
	 */
	public function __construct($options = []) {
		if (!empty($options)) {
			foreach ($options as $key => $value) {
				$this->{$key} = $value;
			}
		}
	}

	/**
	 * @param array $options
	 * @return static
	 */
	public static function make($options = []) {
		return new static($options);
	}

	/**
	 * @param bool $isRequest
	 * @return ResourceBindingOptions
	 */
	public function setIsRequest(bool $isRequest): ResourceBindingOptions {
		$this->isRequest = $isRequest;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isRequest(): bool {
		return $this->isRequest;
	}

	/**
	 * @return array
	 */
	public function hasPopulatedKeys(): bool {
		return count($this->populatedKeys) > 0;
	}

	/**
	 * @return array
	 */
	public function getPopulatedKeys(): array {
		return $this->populatedKeys;
	}

	/**
	 * @param array $populatedKeys
	 */
	public function setPopulatedKeys(array $populatedKeys) {
		$this->populatedKeys = $populatedKeys;
	}


}