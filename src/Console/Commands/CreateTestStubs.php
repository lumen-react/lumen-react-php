<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

class CreateTestStubs extends AbstractCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'rs:lr:tests {users?* : Different user types}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create test stubs.';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function handle() {
		global $app;

		$routeCollection = property_exists($app, 'router') ? $app->router->getRoutes() : $app->getRoutes();
		$rows = [];
		foreach ($routeCollection as $route) {
			global $varNum;
			global $matches;

			$fullPath = $route['uri'];
			$varNum = 0;
			$matches = [];

			$path = preg_replace_callback("/\{(.*?)\}/", function($m) {
				global $varNum;
				global $matches;
				$varNum++;
				$matches[] = "[{$varNum}] " . str_replace("_", ": ", $m[1]);
				return "{{$varNum}}";
			}, $fullPath);

			$rows[] = [
				'verb'            => $route['method'],
				'path'            => $path,
				'namedRoute'      => $this->getNamedRoute($route['action']),
				'controller'      => str_replace('App\Http\Controllers\\', '', $this->getController($route['action'])),
				'controllerClass' => $this->getController($route['action']),
				'action'          => $this->getAction($route['action']),
				'middleware'      => $this->getMiddleware($route['action']),
			];
		}

		$files = [];
		foreach ($rows as $row) {
			$NamedRoute = ucfirst($row['namedRoute']);
			if (empty($NamedRoute)) {
				continue;
			}

			if (empty($files[$row['controller']])) {
				$files[$row['controller']] = ['class' => null, 'functions' => []];
				$files[$row['controller']]['class'] = <<<PHP
<?php

class {$row['controller']}Test extends TestCase {

[FUNCTIONS]
	
}

PHP;
			}

			if (array_search('fileUpload', $row['middleware'])) {
				//TODO fix this shit
				continue;
				$method = 'call';
			} else {
				$method = 'json';
			}

			$users = $this->hasArgument('users') ? $this->argument('users') : null;

			if (empty($users)) {
				$files[$row['controller']]['functions'][] = <<<PHP
	/**
	 * @return void
	 * @throws Exception
	 */
	public function test{$NamedRoute}() {
		\$path = \$url = '{$row['path']}';
		\$named = '$NamedRoute';
		\$verb = '{$row['verb']}';
		\$class = {$row['controllerClass']}::class;
		\$action = '{$row['action']}';
		try {
			\$this->getRequest(function(\$testArgs = []) use (\$path, \$verb) {
				global \$url;
				\$url = \$this->resolveUrl(\$path, !empty(\$testArgs['url']) ? \$testArgs['url'] : []); 
				\$self = !empty(\$testArgs['this']) ? \$testArgs['this'] : \$this;  
				\$self->{$method}(
					\$verb, 
					\$url, 
					!empty(\$testArgs['data']) ? \$testArgs['data'] : [], 
					!empty(\$testArgs['headers']) ? \$testArgs['headers'] : []
				);
				\$this->assertResponseStatus(
					!empty(\$testArgs['code']) ? \$testArgs['code'] : 200
				);
	        }, \$named, \$path, \$verb, \$class, \$action);
	    } catch (\Exception \$e) {
	        \$this->onError(\$e, __METHOD__, \$named, \$url, \$verb, \$class, \$action);
	    }
	}
PHP;
			} else {
				array_unshift($users, '');
				foreach ($users as $user) {
					$User = ucfirst($user);
					$acting = $user ? "->actingAs(\$this->{$user})" : '';
					$scope = $user ? "\$this->{$user}" : 'null';
					$files[$row['controller']]['functions'][] = <<<PHP
	/**
	 * @return void
	 * @throws Exception
	 */
	public function test{$NamedRoute}{$User}() {
		\$path = \$url = '{$row['path']}';
		\$named = '$NamedRoute';
		\$verb = '{$row['verb']}';
		\$class = {$row['controllerClass']}::class;
		\$action = '{$row['action']}';
		\$user = '{$user}';
		try {
			\$this->getRequest(function(\$testArgs = []) use (\$path, \$verb) {
				global \$url;
				\$url = \$this->resolveUrl(\$path, !empty(\$testArgs['url']) ? \$testArgs['url'] : []); 
				\$this{$acting}->{$method}(
					\$verb, 
					\$url, 
					!empty(\$testArgs['data']) ? \$testArgs['data'] : [], 
					!empty(\$testArgs['headers']) ? \$testArgs['headers'] : []
				);
				\$this->assertResponseStatus(
					!empty(\$testArgs['code']) ? \$testArgs['code'] : 200
				);
	        }, {$scope}, \$named, \$path, \$verb, \$class, \$action, \$user);
	    } catch (\Exception \$e) {
	        \$this->onError(\$e, __METHOD__, \$named, \$url, \$verb, \$class, \$action, \$user);
	    }
	}
PHP;
				}
			}
		}

		foreach ($files as $controller => $file) {
			$contents = str_replace('[FUNCTIONS]', implode(PHP_EOL . PHP_EOL, $file['functions']), $file['class']);
			file_put_contents(app()->path() . "/../tests/{$controller}Test.php", $contents);
		}
	}

	/**
	 * @param array $action
	 * @return string
	 */
	protected function getNamedRoute(array $action) {
		return (!isset($action['as'])) ? "" : $action['as'];
	}

	/**
	 * @param array $action
	 * @return mixed|string
	 */
	protected function getController(array $action) {
		if (empty($action['uses'])) {
			return 'None';
		}

		return current(explode("@", $action['uses']));
	}

	/**
	 * @param array $action
	 * @return string
	 */
	protected function getAction(array $action) {
		if (!empty($action['uses'])) {
			$data = $action['uses'];
			if (($pos = strpos($data, "@")) !== false) {
				return substr($data, $pos + 1);
			} else {
				return "METHOD NOT FOUND";
			}
		} else {
			return 'Closure';
		}
	}

	/**
	 * @param array $action
	 * @return array
	 */
	protected function getMiddleware(array $action) {
		return (isset($action['middleware']))
			? (is_array($action['middleware']))
				? $action['middleware']
				: [$action['middleware']] : [];
	}

}
