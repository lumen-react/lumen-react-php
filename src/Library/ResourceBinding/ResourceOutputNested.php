<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;

class ResourceOutputNested {

	private $nested;
	private $forceConversion = false;

	/**
	 * ResourceInputNested constructor.
	 * @param string $nested
	 */
	public function __construct($nested, $forceConversion = false) {
		$this->nested = $nested;
		$this->forceConversion = $forceConversion;
	}

	/**
	 * @param string $nested
	 * @return ResourceOutputNested
	 */
	public static function make($nested) {
		return new static($nested);
	}

	/**
	 * @return string
	 */
	public function getNestedClass() {
		return $this->nested;
	}

	/**
	 * @param bool $forceConversion
	 * @return $this
	 */
	public function setForceConversion($forceConversion = true) {
		$this->forceConversion = $forceConversion;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isForceConversion(): bool {
		return $this->forceConversion;
	}

	/**
	 * @return string
	 */
	public function getNestedResource() {
		if (is_subclass_of($this->nested, AbstractResource::class)) {
			return $this->nested;
		} else if (is_subclass_of($this->nested, AbstractCollection::class)) {
			return $this->nested::resourceClass();
		} else {
			return null;
		}
	}

}