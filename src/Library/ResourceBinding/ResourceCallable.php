<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

class ResourceCallable {

	private $callable;

	/**
	 * ResourceCallable constructor.
	 * @param $callable
	 */
	public function __construct(callable $callable) {
		$this->callable = $callable;
	}

	/**
	 * @param $callable
	 * @return static
	 */
	public static function make($callable) {
		return new static($callable);
	}

	/**
	 * @return callable
	 */
	public function getCallable(): callable {
		return $this->callable;
	}

	/**
	 * @param callable $callable
	 * @return ResourceCallable
	 */
	public function setCallable(callable $callable): ResourceCallable {
		$this->callable = $callable;
		return $this;
	}

}