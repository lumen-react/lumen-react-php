<?php

namespace RajeevSiewnath\LumenReact\Library\OAuth\User;

use RajeevSiewnath\LumenReact\Library\OAuth\OAuthBaseUser;

class OAuthUser extends OAuthBaseUser {

	protected $email;
	protected $familyName;
	protected $gender;
	protected $givenName;
	protected $link;
	protected $locale;
	protected $name;
	protected $picture;
	protected $verifiedEmail;
	protected $provider;
	private $rawData;

	public function __construct($data, $id, $accessToken, $provider) {
		$this->rawData = $data;
		parent::__construct($id, $accessToken, $provider, false);
	}

	public function getAuthIdentifierName() {
		return $this->name;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @return mixed
	 */
	public function getFamilyName() {
		return $this->familyName;
	}

	/**
	 * @return mixed
	 */
	public function getGender() {
		return $this->gender;
	}

	/**
	 * @return mixed
	 */
	public function getGivenName() {
		return $this->givenName;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getLink() {
		return $this->link;
	}

	/**
	 * @return mixed
	 */
	public function getLocale() {
		return $this->locale;
	}

	/**
	 * @return mixed
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @return mixed
	 */
	public function getPicture() {
		return $this->picture;
	}

	/**
	 * @return mixed
	 */
	public function getVerifiedEmail() {
		return $this->verifiedEmail;
	}

	/**
	 * @return mixed
	 */
	public function getProvider() {
		return $this->provider;
	}

	/**
	 * @return mixed
	 */
	public function getRawData() {
		return $this->rawData;
	}

	/**
	 * @return mixed
	 */
	public function getAccessToken() {
		return $this->accessToken;
	}

	public function toArray() {
		return [
			'id'            => $this->id,
			'email'         => $this->email,
			'familyName'    => $this->familyName,
			'gender'        => $this->gender,
			'givenName'     => $this->givenName,
			'link'          => $this->link,
			'locale'        => $this->locale,
			'name'          => $this->name,
			'picture'       => $this->picture,
			'verifiedEmail' => $this->verifiedEmail,
			'provider'      => $this->provider,
			'accessToken'   => $this->accessToken,
			'isClientUser'  => $this->isClientUser,
		];
	}

}