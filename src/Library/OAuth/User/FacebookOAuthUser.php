<?php

namespace RajeevSiewnath\LumenReact\Library\OAuth\User;

class FacebookOAuthUser extends OAuthUser {

	public function __construct($data, $accessToken, $provider) {
		parent::__construct($data, isset($data['id']) ? $data['id'] : null, $accessToken, $provider);
		$this->email = isset($data['email']) ? $data['email'] : null;
		$this->familyName = isset($data['last_name']) ? $data['last_name'] : null;
		$this->gender = isset($data['gender']) ? $data['gender'] : null;
		$this->givenName = isset($data['first_name']) ? $data['first_name'] : null;
		$this->link = isset($data['link']) ? $data['link'] : null;
		$this->locale = isset($data['locale']) ? $data['locale'] : null;
		$this->name = isset($data['name']) ? $data['name'] : null;
		$this->picture = isset($data['picture']) ? $data['picture']['data']['url'] : null;
		$this->verifiedEmail = isset($data['verified_email']) ? $data['verified_email'] : null;
	}

}