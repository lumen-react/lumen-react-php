<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

class DownloadGeoIPDatabase extends AbstractCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'rs:lr:geoipdb {--force : Force downloading}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Download Geo IP database';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 * @throws \Exception
	 */
	public function handle() {
		$dir = __DIR__;
		$geo = "${dir}/../../geo-ip-db.mmdb";
		if (!file_exists($geo) || $this->option('force')) {
			if ($this->option('force')) {
				$this->warn("Forcing redownloading the db...");
			}
			try {
				$this->exec("rm -rf ${dir}/tmp");
				$this->exec("mkdir ${dir}/tmp");
				$this->exec("curl http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz --output ${dir}/tmp/geo-ip-db.tar.gz");
				$this->exec("mkdir ${dir}/tmp/extracted");
				$this->exec("tar xvzf ${dir}/tmp/geo-ip-db.tar.gz -C ${dir}/tmp/extracted");

				$file = null;
				foreach (glob("${dir}/tmp/extracted/*/*.mmdb") as $f) {
					$file = $f;
					break;
				}

				if (!$file) {
					throw new \Exception("File not found");
				}

				$this->exec("cp ${file} $geo");

				$this->exec("rm -rf ${dir}/tmp");
			} catch (\Exception $e) {
				$this->exec("rm -rf ${dir}/tmp");
			}
		} else {
			$this->info("The database is already downloaded");
		}
	}

}
