<?php

namespace RajeevSiewnath\LumenReact\Providers;

use Illuminate\Support\ServiceProvider;
use RajeevSiewnath\LumenReact\Http\Middleware\GeoIPMiddleware;
use RajeevSiewnath\LumenReact\Library\GeoIP\GeoIP;

class GeoIPServiceProvider extends ServiceProvider {

	public function boot() {
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->singleton(GeoIP::class, function($app) {
			return new GeoIP();
		});

		$this->app->routeMiddleWare([
			'geoIP' => GeoIPMiddleware::class,
		]);
	}
}
