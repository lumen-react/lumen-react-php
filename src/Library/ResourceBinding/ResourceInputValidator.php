<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use Illuminate\Http\Request;

class ResourceInputValidator {

	private $validator;
	private $staticValidator = [];

	/**
	 * ResourceInputValidator constructor.
	 * @param $validator
	 */
	public function __construct($validator) {
		$this->validator = $validator;
	}

	/**
	 * @param $validator
	 * @return ResourceInputValidator
	 */
	public static function make($validator) {
		return new static($validator);
	}

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function retrieveValidator(Request $request, $action, $model) {
		if ($this->validator instanceof ResourceCallable) {
			$baseValidators = call_user_func($this->validator->getCallable(), $request, $action, $model);
		} else {
			$baseValidators = $this->validator;
		}
		if (!is_array($baseValidators)) {
			$baseValidators = [$baseValidators];
		}
		return array_merge($this->staticValidator, $baseValidators);
	}

	/**
	 * @return mixed
	 */
	public function getValidator() {
		return $this->validator;
	}

	/**
	 * @param mixed $validator
	 * @return ResourceInputValidator
	 */
	public function setValidator($validator) {
		$this->validator = $validator;
		return $this;
	}

	/**
	 * @param mixed $validator
	 * @return ResourceInputValidator
	 */
	public function addStaticValidator($validator) {
		if (!is_array($validator)) {
			$validator = [$validator];
		}
		$this->staticValidator = array_merge($this->staticValidator, $validator);
		return $this;
	}

}