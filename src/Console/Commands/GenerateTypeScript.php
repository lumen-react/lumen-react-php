<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

class GenerateTypeScript extends AbstractCommand {

	protected $signature = 'rs:lr:ts {outputPath? : Where typescript files go}';
	protected $description = 'Generate all typescript resources';

	public function handle() {
		if ($this->hasOption("outputPath")) {
			$out = trim($this->argument("outputPath", "/\\"));
		} else {
			$out = config('custom.typeScriptDefOutputFolder');
		}

		if (trim($out) == "") {
			$this->error("no output path defined");
			return 1;
		}

		$this->call('rs:lr:ts:types', [
			'outputPath' => $out,
		]);

		$this->call('rs:lr:ts:endpoints', [
			'outputPath' => $out,
		]);

		$this->call('rs:lr:ts:actions', [
			'outputPath' => $out,
		]);

		$this->call('rs:lr:ts:reducers', [
			'outputPath' => $out,
		]);

	}

}