<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

use RajeevSiewnath\LumenReact\Http\Controllers\AbstractController;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class RoutesList extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'rs:lr:routes';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Display all registered routes.';

	private static $isGenerating = false;

	public static function isGenerating() {
		return static::$isGenerating;
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function handle() {
		global $app;

		self::$isGenerating = true;

		$routeCollection = property_exists($app, 'router') ? $app->router->getRoutes() : $app->getRoutes();
		$rows = [];
		foreach ($routeCollection as $route) {
			global $varNum;
			global $matches;

			$fullPath = $route['uri'];
			$varNum = 0;
			$matches = [];

			$path = preg_replace_callback("/\{(.*?)\}/", function($m) {
				global $varNum;
				global $matches;
				$varNum++;
				$matches[] = "[{$varNum}] " . str_replace("_", ": ", $m[1]);
				return "{{$varNum}}";
			}, $fullPath);

			$rows[] = [
				'verb'       => $route['method'],
				'path'       => $path,
				'vars'       => $this->getVars($matches),
				'namedRoute' => $this->getNamedRoute($route['action']),
				'controller' => str_replace('RajeevSiewnath\LumenReact\Http\Controllers\\', '', $this->getController($route['action'])),
				'action'     => $this->getAction($route['action']),
				'input'      => str_replace("RajeevSiewnath\LumenReact\Http\Resources\\", "", $this->getActionInput($route['action'])),
				'validators' => $this->getActionValidators($route['action']),
				'output'     => str_replace("RajeevSiewnath\LumenReact\Http\Resources\\", "", $this->getActionOutput($route['action'])),
				'middleware' => $this->getMiddleware($route['action']),
			];
		}

		$headers = ['Verb', 'Path', 'Vars', 'NamedRoute', 'Controller', 'Action', 'Input', 'Validators', 'Output', 'Middleware'];
		$this->table($headers, $rows);
	}

	/**
	 * @param array $action
	 * @return string
	 */
	protected function getNamedRoute(array $action) {
		return (!isset($action['as'])) ? "" : $action['as'];
	}

	/**
	 * @param array $action
	 * @return mixed|string
	 */
	protected function getController(array $action) {
		if (empty($action['uses'])) {
			return 'None';
		}

		return current(explode("@", $action['uses']));
	}

	/**
	 * @param array $action
	 * @return string
	 */
	protected function getAction(array $action) {
		if (!empty($action['uses'])) {
			$data = $action['uses'];
			if (($pos = strpos($data, "@")) !== false) {
				return substr($data, $pos + 1);
			} else {
				return "METHOD NOT FOUND";
			}
		} else {
			return 'Closure';
		}
	}

	/**
	 * @param array $action
	 * @return string
	 */
	protected function getMiddleware(array $action) {
		return (isset($action['middleware']))
			? (is_array($action['middleware']))
				? join(PHP_EOL, $action['middleware'])
				: $action['middleware'] : '';
	}

	/**
	 * @param array $matches
	 * @return string
	 */
	protected function getVars(array $matches) {
		return implode(PHP_EOL, $matches);
	}

	protected function getActionInput($action) {
		$controller = $this->getController($action);
		$action = $this->getAction($action);
		if (is_subclass_of($controller, AbstractController::class)) {
			return $controller::input($action);
		} else {
			return null;
		}
	}

	protected function getActionOutput($action) {
		$controller = $this->getController($action);
		$action = $this->getAction($action);
		if (is_subclass_of($controller, AbstractController::class)) {
			return $controller::output($action);
		} else {
			return null;
		}
	}

	protected function getActionValidators($action) {
		$controller = $this->getController($action);
		$action = $this->getAction($action);
		if (is_subclass_of($controller, AbstractController::class)) {
			$resource = $controller::input($action);
			if ($resource) {
				$validators = $resource::validators(new Request(), $action, null);
				$return = [];
				foreach ($validators as $key => $validator) {
					array_walk($validator, function(&$v) {
						$v = (string)$v;
						$v = "- $v";
					});
					$return[] = "[$key]" . PHP_EOL . implode(PHP_EOL, $validator);
				}
				return implode(PHP_EOL, $return);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

}
