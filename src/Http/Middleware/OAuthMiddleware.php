<?php

namespace RajeevSiewnath\LumenReact\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use RajeevSiewnath\LumenReact\Exceptions\IllegalOperationException;
use RajeevSiewnath\LumenReact\Http\Middleware\OAuthMiddleware\OAuthMiddlewareImplementation;

class OAuthMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @param $action
	 * @return mixed
	 * @throws IllegalOperationException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function handle(Request $request, Closure $next, $action) {
		$implementation = OAuthMiddlewareImplementation::getOAuthProviderFromRequest($request, $action);
		/** @var OAuthMiddlewareImplementation $instance */
		$instance = new $implementation;
		return $instance->handle($request, $next, $action);
	}

}
