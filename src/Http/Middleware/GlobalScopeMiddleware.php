<?php

namespace RajeevSiewnath\LumenReact\Http\Middleware;

use RajeevSiewnath\LumenReact\Library\GlobalScope\GlobalScope;
use Closure;

class GlobalScopeMiddleware {

	private $globalScope;

	public function __construct(GlobalScope $globalScope) {
		$this->globalScope = $globalScope;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, ...$scopes) {
		$this->globalScope->enable();
		$this->globalScope->setScopes($scopes);
		return $next($request);
	}

}
