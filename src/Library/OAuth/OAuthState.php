<?php

namespace RajeevSiewnath\LumenReact\Library\OAuth;

use Illuminate\Support\Facades\Crypt;
use RajeevSiewnath\LumenReact\Exceptions\IllegalOperationException;
use RajeevSiewnath\LumenReact\Library\OAuth\Token\OAuthToken;

class OAuthState {

	private $redirect;
	private $provider;
	private $offline;

	/**
	 * OAuthState constructor.
	 * @param string $redirect
	 * @param string $provider
	 * @param bool $offline
	 * @throws IllegalOperationException
	 */
	public function __construct(string $redirect, string $provider, bool $offline) {
		if (!$redirect) {
			throw new IllegalOperationException("No valid redirect set");
		}
		$this->redirect = $redirect;
		$this->provider = $provider;
		$this->offline = $offline;
	}

	/**
	 * @param $encrypted
	 * @return OAuthState
	 * @throws IllegalOperationException
	 */
	public static function fromEncrypted($encrypted) {
		$data = json_decode(Crypt::decrypt($encrypted), true);
		return new OAuthState(
			$data['redirect'],
			$data['provider'],
			$data['offline']
		);
	}

	/**
	 * @return array
	 */
	public function toArray() {
		return [
			'redirect' => $this->redirect,
			'provider' => $this->provider,
			'offline'  => $this->offline,
		];
	}

	/**
	 * @return string
	 */
	public function encrypt() {
		return Crypt::encrypt(json_encode($this->toArray()));
	}

	/**
	 * @return mixed
	 */
	public function getRedirect() {
		return $this->redirect;
	}

	/**
	 * @return mixed
	 */
	public function getProvider() {
		return $this->provider;
	}

	/**
	 * @return mixed
	 */
	public function getOffline() {
		return $this->offline;
	}

	/**
	 * @param $accessToken
	 * @param $extra
	 * @return OAuthToken
	 */
	public function convertToOAuthToken($accessToken, $extra = null) {
		return new OAuthToken($accessToken, $this->provider, $this->offline, $extra);
	}

}