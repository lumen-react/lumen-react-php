<?php

namespace RajeevSiewnath\LumenReact\Library\ResponseScope;

class ResponseScope {

	private $outputHeaders = [];
	private $expiredToken = false;
	private $staticContainer = null;
	private $inputWithQueryCallbacks = [];

	public function createStaticContainer($resourceClass) {

	}

	public function hasValidStaticContainer() {
		return $this->staticContainer !== null;
	}

	/**
	 * @return array
	 */
	public function getOutputHeaders(): array {
		return $this->outputHeaders;
	}

	/**
	 * @param $key
	 * @param $value
	 * @return $this
	 */
	public function addHeader($key, $value) {
		$this->outputHeaders[$key] = json_encode($value);
		return $this;
	}

	/**
	 * @param array $outputHeaders
	 * @return ResponseScope
	 */
	public function setOutputHeaders(array $outputHeaders): ResponseScope {
		$this->outputHeaders = $outputHeaders;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isExpiredToken(): bool {
		return $this->expiredToken;
	}

	/**
	 * @param bool $expiredToken
	 */
	public function setExpiredToken(bool $expiredToken) {
		$this->expiredToken = $expiredToken;
	}

}