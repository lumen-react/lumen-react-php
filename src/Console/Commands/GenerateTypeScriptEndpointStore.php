<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

use RajeevSiewnath\LumenReact\Http\Controllers\AbstractController;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;

class GenerateTypeScriptEndpointStore extends AbstractEndpointReader {

	protected $signature = 'rs:lr:ts:endpoints {outputPath? : Where endpoints goes}';
	protected $description = 'Generate endpoints store';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		if ($this->hasOption("outputPath")) {
			$out = trim($this->argument("outputPath", "/\\"));
		} else {
			$out = config('custom.typeScriptDefOutputFolder');
		}

		if (trim($out) == "") {
			$this->error("no output path defined");

			return 1;
		}

		$output = $this->getFile($this->getRouteCollection(), $this->getDateFields());

		$dest = rtrim($out, "/\\") . "/endpoints.ts";
		file_put_contents($dest, $output);
		$this->info(PHP_EOL . "wrote endpoints to $dest");

		return 0;
	}

	private function getDateFields() {
		$fields = [];
		// Fetch all model files in folder
		$classes = [];
		foreach (glob(app()->path() . "/Http/Resources/*Resource.php") as $file) {
			require_once $file;

			// get the file name of the current file without the extension
			// which is essentially the class name
			$class = basename($file, '.php');
			if ($class == 'AbstractResource') {
				continue;
			}

			$fullClass = "\App\Http\Resources\\$class";
			if (class_exists($fullClass)) {
				$classes[] = $fullClass;
			} else {
				$this->error("class $class does not exists");
			}
		}

		foreach ($classes as $class) {
			$className = $class::getType();
			$outputs = $class::bindings()->getOutputs();
			foreach ($outputs as $output) {
				$key = $output->getKey();
				$type = $output->getTypeString();
				if (in_array($key, $fields) && $type != 'Date') {
					$this->error("key $key in class $className conflicts with other Dates");
				} else if ($type == 'Date') {
					$fields[] = $key;
				}
			}
		}

		return array_unique($fields);
	}

	public function getFile($rows, $dates) {
		$requests = [];
		$barSize = 0;
		foreach ($rows as $row) {
			if (!$row['namedRoute']) {
				continue;
			}

			$barSize++;
			$row['args'] = $this->generateJsArgumentMetadata($row['args'], $row['controller'], $row['action']);
			$requests[] = [
				'name'         => $row['namedRoute'],
				'method'       => strtolower($row['verb']),
				'uri'          => $this->getJsUriWithEscapedArguments($row['path'], $row['args']),
				'jsArgs'       => $this->getJsArguments($row['args'], $row['controller'], $row['action'], false),
				'jsArgsCalled' => $this->getJsArguments($row['args'], $row['controller'], $row['action'], true),
				'return'       => $this->getOutputType($row['controller'], $row['action']),
				'hasInput'     => $this->hasInput($row['controller'], $row['action']),
				'isFileUpload' => $this->getFileUpload($row['middlewareArray']),
			];
		}
		$bar = $this->output->createProgressBar(sizeof($barSize) * 4);
		$types = $this->getTypes();
		$output = <<<TEXT
import axios, {AxiosResponse, AxiosError, AxiosInstance} from 'axios';
import {AxiosRequestConfiguration, ExtendedResponseData, getUrl, parseHeadersToApiResponseHeaders} from 'lumen-react-javascript';
import _ from 'lodash';

TEXT;
		if ($types) {
			$types = implode(', ', $types);
			$output .= <<<TEXT
import { {$types} } from './types';

TEXT;
		}

		array_walk($dates, function(&$value) {
			$value = "    '{$value}',";
		});

		$dates = implode(PHP_EOL, $dates);
		$output .= <<<TEXT

const apiDateFields = [
$dates
];

export interface FilesInput {
	[key: string]: File | Array<File>;
}

export function convertApiFields<T>(object: T): T {
    if (_.isArray(object)) {
        let ret: any = [];
        let array: Array<any> = object as any;
        for (let i = 0; i < array.length; i++) {
            ret[i] = convertApiFields<T>(array[i]);
        }
        return ret as T;
    } else if (_.isObject(object) && !_.isNull(object)) {
        let ret: any = {};
        for (let key in object) {
            if (_.isNull(object[key])) {
                ret[key] = null;
            } else if (_.isArray(object[key])) {
                ret[key] = [];
                let array: Array<any> = object[key] as any;
                for (let i = 0; i < array.length; i++) {
                    ret[key][i] = convertApiFields<any>(array[i]);
                }
            } else if (_.isObject(object[key])) {
                ret[key] = convertApiFields<any>(object[key]);
            } else {
                ret[key] = convertApiField(key, object[key]);
            }
        }
        return ret as T;
    } else {
        return object;
    }
}

export function convertApiField(key: string, value: any): any {
	let ret = value;
	if (_.indexOf(apiDateFields, key) >= 0) {
		ret = new Date(value);
	}
	return ret;
}

export function createFormData(files, input?): FormData {
	let formData = new FormData();
	for (let key in files) {
		if (_.isArray(files[key])) {
			for (let i in files[key]) {
				formData.append(`\${key}[\${i}]`, files[key][i]);
			}
		} else {
			formData.append(`\${key}`, files[key]);
		}
	}
	if (input) {
		for (let key in input) {
			formData.append(key, input[key]);
		}
	}
	return formData;
}

TEXT;

		$output .= PHP_EOL;

		foreach ($requests as $r) {
			// delete is not supported generic in axios
			$method = $r['method'] == 'delete' ? 'delete' : "{$r['method']}<{$r['return']}>";
			if ($r['isFileUpload']) {
				if ($r['hasInput']) {
					$input = <<<TEXT

			createFormData(files, input),
TEXT;
				} else {
					$input = <<<TEXT

			createFormData(files),
TEXT;
				}
			} else {
				$input = !$r['hasInput'] ? "" : <<<TEXT

			input,
TEXT;
			}

			if ($r['isFileUpload']) {
				$jsArgs = implode(', ', array_merge($r['jsArgs'], ['files: FilesInput', 'requestConfig?: AxiosRequestConfiguration']));
			} else {
				$jsArgs = implode(', ', array_merge($r['jsArgs'], ['requestConfig?: AxiosRequestConfiguration']));
			}
			$output .= <<<TEXT
export function {$r['name']}Request({$jsArgs}): Promise<{$r['return']}> {
	let instance: AxiosInstance = (requestConfig && requestConfig.axios) ? requestConfig.axios : axios; 
	return new Promise<{$r['return']}>((resolve, reject) => {
		instance.$method(
			getUrl(`{$r['uri']}`, requestConfig),{$input}
			requestConfig,
		).then((response: AxiosResponse<{$r['return']}>) => {
			resolve(convertApiFields<{$r['return']}>(response.data));	
		}).catch((error: AxiosError) => {
			reject(error);
		});
	});
}

export function {$r['name']}RequestExtended({$jsArgs}): Promise<ExtendedResponseData<{$r['return']}>> {
	let instance: AxiosInstance = (requestConfig && requestConfig.axios) ? requestConfig.axios : axios; 
	return new Promise<ExtendedResponseData<{$r['return']}>>((resolve, reject) => {
		instance.$method(
			getUrl(`{$r['uri']}`, requestConfig),{$input}
			requestConfig,
		).then((response: AxiosResponse<{$r['return']}>) => {
			resolve({
				data: convertApiFields<{$r['return']}>(response.data),
				apiResponseHeaders: parseHeadersToApiResponseHeaders(response.headers),
			    allHeaders: response.headers,
			    code: response.status,
			});	
		}).catch((error: AxiosError) => {
			reject(error);
		});
	});
}


TEXT;
			$bar->advance();
		}

		foreach ($requests as $r) {
			if ($r['isFileUpload']) {
				$jsArgs = implode(', ', array_merge($r['jsArgs'], ['files: FilesInput', 'requestConfig?: AxiosRequestConfiguration']));
				$jsArgsCalled = implode(', ', array_merge($r['jsArgsCalled'], ['files', 'requestConfig']));
			} else {
				$jsArgs = implode(', ', array_merge($r['jsArgs'], ['requestConfig?: AxiosRequestConfiguration']));
				$jsArgsCalled = implode(', ', array_merge($r['jsArgsCalled'], ['requestConfig']));
			}
			$output .= <<<TEXT
export function {$r['name']}RequestLoaderChunk(request: (props, _: ({$jsArgs}) => Promise<{$r['return']}>) => Promise<{$r['return']}>): { [key: string]: (props) => Promise<{$r['return']}> } {
	return { {$r['name']}: (props) => request(props, {$r['name']}Request) };
}

export function {$r['name']}RequestExtendedLoaderChunk(request: (props, _: ({$jsArgs}) => Promise<ExtendedResponseData<{$r['return']}>>) => Promise<ExtendedResponseData<{$r['return']}>>): { [key: string]: (props) => Promise<ExtendedResponseData<{$r['return']}>> } {
	return { {$r['name']}Extended: (props) => request(props, {$r['name']}RequestExtended) };
}


TEXT;

			$bar->advance();
		}

		foreach ($requests as $r) {
			if ($r['isFileUpload']) {
				$jsArgs = implode(', ', array_merge($r['jsArgs'], ['files: FilesInput', 'requestConfig?: AxiosRequestConfiguration']));
				$jsArgsCalled = implode(', ', array_merge($r['jsArgsCalled'], ['files', 'requestConfig']));
			} else {
				$jsArgs = implode(', ', array_merge($r['jsArgs'], ['requestConfig?: AxiosRequestConfiguration']));
				$jsArgsCalled = implode(', ', array_merge($r['jsArgsCalled'], ['requestConfig']));
			}
			$output .= <<<TEXT
export function {$r['name']}RequestLoader(request: (props, _: ({$jsArgs}) => Promise<{$r['return']}>) => Promise<{$r['return']}>): { promises: { [key: string]: (props) => Promise<{$r['return']}> } } {
	return { promises: {$r['name']}RequestLoaderChunk(request) };
}

export function {$r['name']}RequestExtendedLoader(request: (props, _: ({$jsArgs}) => Promise<ExtendedResponseData<{$r['return']}>>) => Promise<ExtendedResponseData<{$r['return']}>>): { promises: { [key: string]: (props) => Promise<ExtendedResponseData<{$r['return']}>> } } {
	return { promises: {$r['name']}RequestExtendedLoaderChunk(request) };
}


TEXT;


			$bar->advance();
		}

		foreach ($requests as $r) {
			$name = ucfirst($r['name']);
			$output .= <<<TEXT
export interface {$name}RequestPropsMapper {
	{$r['name']}: {$r['return']}
}

export interface {$name}RequestExtendedPropsMapper {
	{$r['name']}Extended: ExtendedResponseData<{$r['return']}>
}


TEXT;


			$bar->advance();
		}

		return $output;
	}

	protected function getJsUriWithEscapedArguments(string $path, $args) {
		$argsNames = array_map(function($arg) {
			return $arg['uriPart'];
		}, $args);
		$i = 0;

		return preg_replace_callback('/{(.*?)}/', function() use (&$i, $argsNames) {
			$i++;

			return $argsNames[$i - 1];
		}, $path);
	}

	protected function getJsArguments($args, $controller, $action, $called) {
		$jsArgs = [];
		$resource = (is_subclass_of($controller, AbstractController::class)) ? $controller::input($action) : null;
		if ($resource) {
			$refResource = new \ReflectionClass($resource);
			$resourceName = null;
			$importName = null;
			if (is_subclass_of($resource, AbstractResource::class)) {
				$importName = $resourceName = str_replace("Resource", "", $refResource->getShortName());
			} else if (is_subclass_of($resource, AbstractCollection::class)) {
				$importName = str_replace("Collection", "", $refResource->getShortName());
				$resourceName = "Array<$importName>";
			} else {
				$importName = $resourceName = $refResource->getShortName();
			}

			if ($called) {
				$jsArgs[] = "input";
			} else {
				$jsArgs[] = "input: $resourceName";
			}
		}

		foreach ($args as $arg) {
			if ($arg['argument'] != null) {
				if ($called) {
					$jsArgs[] = "{$arg['argument']}";
				} else {
					$jsArgs[] = "{$arg['argument']}: {$arg['type']}";
				}
			}
		}

		return $jsArgs;
	}

	protected function getFileUpload($middlewares) {
		foreach ($middlewares as $middleware) {
			if ($middleware === 'fileUpload') {
				return true;
			}
		}
		return false;
	}

}
