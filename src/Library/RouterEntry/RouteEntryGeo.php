<?php

namespace RajeevSiewnath\LumenReact\Library\RouterEntry;

use Illuminate\Http\Request;

class RouteEntryGeo extends AbstractRouterEntry {

	/**
	 * @return mixed
	 */
	public function doRouteAction(Request $request, $router) {
		$geoIp = app(\RajeevSiewnath\LumenReact\Library\GeoIP\GeoIP::class);
		$data = $geoIp->get($this->getIp());
		return $data ? $data->toArray() : null;
	}

	/**
	 * @return string
	 */
	private function getIp() {
		if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ipAddresses = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
			return trim(end($ipAddresses));
		} else {
			return $_SERVER['REMOTE_ADDR'];
		}
	}

	/**
	 * @return string
	 */
	public function getRouteName() {
		return 'geo';
	}

	/**
	 * @return array
	 */
	public function getMiddlewares(): array {
		return ['geoIP'];
	}

}