<?php

namespace RajeevSiewnath\LumenReact\Http\Middleware;

use Closure;
use RajeevSiewnath\LumenReact\Library\GeoIP\GeoIP;

class GeoIPMiddleware {

	private $geoIP;

	public function __construct(GeoIP $geoIP) {
		$this->geoIP = $geoIP;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 * @throws \GeoIp2\Exception\AddressNotFoundException
	 * @throws \MaxMind\Db\Reader\InvalidDatabaseException
	 */
	public function handle($request, Closure $next) {
		$this->geoIP->init();
		return $next($request);
	}
}
