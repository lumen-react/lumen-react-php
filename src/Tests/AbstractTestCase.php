<?php

namespace RajeevSiewnath\LumenReact\Tests;

use \Illuminate\Support\Facades\File;

register_shutdown_function(function() {
	echo PHP_EOL;
	echo implode(PHP_EOL . PHP_EOL, AbstractTestCase::$output);
	echo PHP_EOL;
});

abstract class AbstractTestCase extends \Laravel\Lumen\Testing\TestCase {

	protected $faker;

	public static $output = [];

	protected function onError($e, $test, $named, $path, $verb, $class, $action) {
		$this->beforeApplicationDestroyedCallbacks[] = function() use ($test, $named, $path, $verb, $class, $action) {
			$output = <<<TEXT
****************************************** FAILURE ($test) ******************************************
[REQUEST]
{$verb} {$path} [{$named}] -> {$class}@{$action}
[RESPONSE]
{$this->response->getContent()}
TEXT;
			static::$output[] = $output;
		};
		throw($e);
	}

	public function setUp() {
		parent::setUp();

		$this->faker = \Faker\Factory::create();

		\Illuminate\Support\Facades\Storage::fake('testing');
		\Illuminate\Support\Facades\Storage::disk('testing')->put("test/img.jpg", File::get(__DIR__ . "/Data/img.jpg"));
		\Illuminate\Support\Facades\Storage::disk('testing')->put("test/pdf.pdf", File::get(__DIR__ . "/Data/pdf.pdf"));
	}

	protected function resolveUrl($url, $params) {
		if (!empty($params)) {
			if (!is_array($params)) {
				$params = [$params];
			}

			if (preg_match_all('/(\{\d+\})/', $url, $matches)) {
				for ($i = 1; $i < sizeof($matches); $i++) {
					$url = str_replace($matches[$i][0], $params[$i - 1], $url);
				}
			}
		}
		return $url;
	}

	/**
	 * @param $prefix
	 * @param $verb
	 * @param $controller
	 * @param $action
	 * @return string
	 * @throws \Exception
	 */
	private function getFunction($named, $path, $verb, $controller, $action, $user = null) {
		$controller = str_replace('App\\Http\\Controllers\\', '', $controller);
		$function = 'doTest' . ucfirst($named);
		$function = $user ? $function . ucfirst($user) : $function;
		if (method_exists($this, $function)) {
			return $function;
		}
		throw new \Exception("No test set for $named, $path, $verb, $controller, $action ($function)");
	}

	/**
	 * @param $callback
	 * @param $verb
	 * @param $controller
	 * @param $action
	 * @throws \Exception
	 */
	protected function getRequest($callback, $scope, $named, $path, $verb, $controller, $action, $user = null) {
		$function = $this->getFunction($named, $path, $verb, $controller, $action, $user);
		$this->{$function}($callback, $scope);
	}

}
