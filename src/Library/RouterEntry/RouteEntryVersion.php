<?php

namespace RajeevSiewnath\LumenReact\Library\RouterEntry;

use Illuminate\Http\Request;
use RajeevSiewnath\LumenReact\Library\AppSettings\AppSettings;

class RouteEntryVersion extends AbstractRouterEntry {


	/**
	 * @param Request $request
	 * @param $router
	 * @return array|mixed
	 */
	public function doRouteAction(Request $request, $router) {
		$appSettings = app(AppSettings::class);
		$version = [];
		$version['laravel'] = $router->app->version();
		$version['php'] = phpversion();
		$version['build'] = $appSettings->getBuildVersion();
		return $version;
	}

	/**
	 * @return string
	 */
	public function getRouteName() {
		return 'version';
	}

}