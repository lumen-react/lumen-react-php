<?php

namespace RajeevSiewnath\LumenReact\Library\AppSettings;

class AppSettings {

	private $errorReportCallback = null;
	private $routerEntries = [];

	/**
	 * @return null
	 */
	public function getErrorReportCallback() {
		return $this->errorReportCallback;
	}

	/**
	 * @param null $errorReportCallback
	 */
	public function setErrorReportCallback($errorReportCallback) {
		$this->errorReportCallback = $errorReportCallback;
	}

	public function getBuildVersion() {
		$externalBuildVersion = getenv('EXTERNAL_BUILD_VERSION');
		if ($externalBuildVersion) {
			return $externalBuildVersion;
		} else {
			$filename = base_path() . '/.version';
			if (file_exists($filename)) {
				return file_get_contents($filename);
			}
		}
		return '';
	}

}