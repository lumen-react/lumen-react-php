<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

use RajeevSiewnath\LumenReact\Http\Middleware\OAuthMiddleware\OAuthMiddlewareImplementation;

class OAuthTokenInfo extends AbstractCommand {

	protected $signature = 'rs:lr:oauth:token {token? : What token should be verified?} {--e|error : Throw an error if the token cannot be retrieved}';
	protected $description = 'Verify oAuth token';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$token = $this->argument("token");
		if (!$token) {
			$token = $this->secret("Please enter token");
		}

		if ($token) {
			try {
				$token = OAuthMiddlewareImplementation::getOAuthDecryptedToken($token);
			} catch (\Exception $e) {
				if (!$this->option('error')) {
					$this->error($e->getMessage());
				}
				$token = null;
			}
			if ($token) {
				$this->info((string)$token);
			} else {
				if ($this->option('error')) {
					$this->error("Invalid token");
					return 1;
				} else {
					$this->info(json_encode(null));
				}
			}
		} else {
			$this->error("No token given");
			return 1;
		}

		return 0;
	}

}