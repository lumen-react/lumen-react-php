<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

use RajeevSiewnath\LumenReact\Http\Controllers\AbstractController;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;

class GenerateTypeScriptReducerStore extends AbstractEndpointReader {

	protected $signature = 'rs:lr:ts:reducers {outputPath? : Where reducers goes}';
	protected $description = 'Generate redux reducers';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		if ($this->hasOption("outputPath")) {
			$out = trim($this->argument("outputPath", "/\\"));
		} else {
			$out = config('custom.typeScriptDefOutputFolder');
		}

		if (trim($out) == "") {
			$this->error("no output path defined");

			return 1;
		}

		$output = $this->getFile($this->getRouteCollection());

		$dest = rtrim($out, "/\\") . "/reducers.ts";
		file_put_contents($dest, $output);
		$this->info(PHP_EOL . "wrote reducers to $dest");

		return 0;
	}

	private function fromCamelCase($input) {
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
		$ret = $matches[0];
		foreach ($ret as &$match) {
			$match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
		}
		return implode('_', $ret);
	}

	private function getFile($routeCollection) {
		$requests = [];
		$imports = [];
		foreach ($routeCollection as $row) {
			if (!$row['namedRoute'] || !in_array('typeScriptActionable', $row['middlewareArray'])) {
				continue;
			}

			if (is_subclass_of($row['controller'], AbstractController::class)) {
				$row['args'] = $this->generateJsArgumentMetadata($row['args'], $row['controller'], $row['action']);
				$returnType = $this->getOutputType($row['controller'], $row['action']);
				$requests[] = [
					'name'          => $row['namedRoute'],
					'jsArgs'        => $this->getJsArguments($row['args'], $row['controller'], $row['action'], false),
					'jsArgsNoTypes' => $this->getJsArguments($row['args'], $row['controller'], $row['action'], true),
					'return'        => $returnType,
					'default'       => preg_match('/^Array\<.*\>$/', $returnType) ? '[]' : 'null',
					'hasInput'      => $this->hasInput($row['controller'], $row['action']),
				];
			}
		}

		$bar = $this->output->createProgressBar(sizeof($requests));
		$states = [];
		$stateTypes = [];
		$reducers = [];
		$actionPropsMapping = [];
		$actionProps = [];

		foreach ($requests as $request) {
			$NamedRoute = ucfirst($request['name']);
			$states[] = "{$request['name']}State";
			$stateTypes[] = "{$request['return']}";
			$actionType = strtoupper($this->fromCamelCase($request['name']));
			$reducers[] = <<<TEXT
export const {$request['name']}Reducer: Reducer<{$request['return']}> = (state: {$request['return']} = {$request['default']}, action: AnyAction) => {
    if (action.error) {
        return state;
    }

    switch (action.type) {
        case ActionType.{$actionType}:
            return action.payload;
        case GlobalActionType.REFRESH_STORE:
            return {...state};
        default:
            return state;
    }
};

TEXT;
			$actionPropsMapping[] = <<<TEXT
export function {$request['name']}StatePropsMapper(state: BaseAppState) {
	return {{$request['name']}State: state.{$request['name']}State};
}

TEXT;

			$actionProps[] = <<<TEXT
export interface ${NamedRoute}StateProp {
    {$request['name']}State: {$request['return']};
}

TEXT;

			$bar->advance();
		}

		$output = <<<TEXT
import {combineReducers, Reducer, AnyAction} from "redux";
import {FormReducer, FormStateMap, reducer as formReducer} from 'redux-form';
import {ActionType} from "./actions";
import {GlobalActionType} from 'lumen-react-javascript';

TEXT;

		$types = $this->getTypes();
		if ($types) {
			$types = implode(', ', $types);
			$output .= <<<TEXT
import {{$types}} from './types';


TEXT;
		}

		$actionPropsMapping = implode(PHP_EOL, $actionPropsMapping);
		$output .= $actionPropsMapping;
		$output .= PHP_EOL;

		$actionProps = implode(PHP_EOL, $actionProps);
		$output .= $actionProps;
		$output .= PHP_EOL;

		$statesWithTypes = $states;
		array_walk($statesWithTypes, function(&$state, $index) use ($stateTypes) {
			$state = "\t$state: {$stateTypes[$index]};";
		});
		$statesWithTypes = implode(PHP_EOL, $statesWithTypes);
		$output .= <<<TEXT
export interface BaseAppState {
    form: FormStateMap;
$statesWithTypes
}


TEXT;

		$statesWithReducers = $states;
		array_walk($statesWithReducers, function(&$state, $index) use ($stateTypes) {
			$state = "\t$state: Reducer<{$stateTypes[$index]}>;";
		});
		$statesWithReducers = implode(PHP_EOL, $statesWithReducers);
		$output .= <<<TEXT
export interface BaseAppStateReducer<T extends BaseAppState> extends Reducer<T> {
    form: FormReducer;
$statesWithReducers
}


TEXT;

		$defaultReducerCreator = $states;
		array_walk($defaultReducerCreator, function(&$state, $index) use ($stateTypes) {
			$reducer = str_replace('State', 'Reducer', $state);
			$state = "\t\t\t$state: $reducer,";
		});
		$defaultReducerCreator = implode(PHP_EOL, $defaultReducerCreator);
		$output .= <<<TEXT
export function createAppStateReducers<T extends BaseAppState, U extends BaseAppStateReducer<T>>(reducerOverrides: Partial<U> = {}): U {
	return {
		...{
			form: formReducer,
$defaultReducerCreator
		},
		...(reducerOverrides as any)
	};
}


TEXT;

		$output .= <<<TEXT
export function createRootReducer<T extends BaseAppState>(appStateReducers): Reducer<T> {
	return combineReducers<T>(appStateReducers);
}


TEXT;

		$reducers = implode(PHP_EOL, $reducers);
		$output .= <<<TEXT
/*
 * THE FOLLOWING ARE DEFAULT REDUCERS, COPY OR USE AS WANTED
 */
$reducers
TEXT;

		return $output;
	}

	protected function getJsArguments($args, $controller, $action, $noTypes) {
		$jsArgs = [];
		$resource = $controller::input($action);
		if ($resource) {
			$refResource = new \ReflectionClass($resource);
			$resourceName = null;
			$importName = null;
			if (is_subclass_of($resource, AbstractResource::class)) {
				$importName = $resourceName = str_replace("Resource", "", $refResource->getShortName());
			} else if (is_subclass_of($resource, AbstractCollection::class)) {
				$importName = str_replace("Collection", "", $refResource->getShortName());
				$resourceName = "Array<$importName>";
			} else {
				$importName = $resourceName = $refResource->getShortName();
			}

			if (!!$noTypes) {
				$jsArgs[] = "input";
			} else {
				$jsArgs[] = "input: $resourceName";
			}
		}

		foreach ($args as $arg) {
			if ($arg['argument'] != null) {
				if (!!$noTypes) {
					$jsArgs[] = $arg['argument'];
				} else {
					$jsArgs[] = "{$arg['argument']}: {$arg['type']}";
				}
			}
		}

		return $jsArgs;
	}

}