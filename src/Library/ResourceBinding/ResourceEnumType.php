<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use RajeevSiewnath\LumenReact\Library\TypeScript\TypeScriptDefinitions;
use RajeevSiewnath\LumenReact\Library\TypeScript\TypeScriptEnum;

class ResourceEnumType {

	private $enum;

	/**
	 * ResourceInputMorphType constructor.
	 * @param $enum
	 */
	public function __construct($enum) {
		if (!($enum instanceof TypeScriptEnum)) {
			$enum = app(TypeScriptDefinitions::class)->getEnum($enum);
		}
		$this->enum = $enum;
	}

	/**
	 * @param TypeScriptEnum|string $enum $enum
	 * @return ResourceEnumType
	 */
	public static function make($enum) {
		return new static($enum);
	}

	/**
	 * @param $type
	 * @return int|null|string
	 */
	public function getMorphedValue($type) {
		return $this->enum->getValue($type);
	}

	/**
	 * @param $class
	 * @return int|null|string
	 */
	public function getMorphedKey($class) {
		return $this->enum->getKey($class);
	}

	/**
	 * @return TypeScriptEnum
	 */
	public function getEnum(): TypeScriptEnum {
		return $this->enum;
	}

}