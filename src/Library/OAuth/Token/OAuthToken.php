<?php

namespace RajeevSiewnath\LumenReact\Library\OAuth\Token;

use Illuminate\Support\Facades\Crypt;
use RajeevSiewnath\LumenReact\Library\OAuth\OAuthBaseToken;

class OAuthToken extends OAuthBaseToken {

	private $offline;
	private $extra;

	/**
	 * OAuthToken constructor.
	 * @param $accessToken
	 * @param $provider
	 * @param $offline
	 * @param $extra
	 */
	public function __construct(string $accessToken, string $provider, bool $offline, $extra = null) {
		parent::__construct($accessToken, $provider, 'user');
		$this->offline = $offline;
		$this->extra = $extra;
	}

	/**
	 * @param $encrypted
	 * @return OAuthToken
	 */
	public static function fromEncrypted($encrypted) {
		$data = json_decode(Crypt::decrypt($encrypted), true);
		return new OAuthToken(
			$data['access_token'],
			$data['provider'],
			$data['offline'],
			$data['extra']
		);
	}

	/**
	 * @return mixed
	 */
	public function getOffline() {
		return $this->offline;
	}

	/**
	 * @return null
	 */
	public function hasExtra() {
		return !!$this->extra;
	}

	/**
	 * @return null
	 */
	public function getExtra() {
		return $this->extra;
	}

	/**
	 * @return array
	 */
	public function toArray() {
		return [
			'access_token' => $this->accessToken,
			'provider'     => $this->provider,
			'type'         => $this->type,
			'offline'      => $this->offline,
			'extra'        => $this->extra,
		];
	}

}