<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use Illuminate\Http\Request;

class ResourceInputMorphData {

	private $input;

	/**
	 * ResourceBindingParameter|callable constructor.
	 * @param $input
	 */
	public function __construct($input) {
		$this->input = $input;
	}

	/**
	 * @param ResourceInput|callable $input
	 * @return ResourceInputMorphData
	 */
	public static function make($input) {
		return new static($input);
	}

	/**
	 * @return string
	 */
	public function getMorphedClass(Request $request) {
		if ($this->input instanceof ResourceInput && $this->input->hasEnumType()) {
			return $this->input->getValue($request);
		} else if ($this->input instanceof ResourceCallable) {
			return call_user_func($this->input->getCallable(), $request);
		} else {
			return null;
		}
	}

}