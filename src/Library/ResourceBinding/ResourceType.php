<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;
use RajeevSiewnath\LumenReact\Library\TypeScript\TypeScriptEnum;

class ResourceType {

	private $type;

	/**
	 * ResourceOutputType constructor.
	 * @param $type
	 */
	public function __construct($type) {
		$this->type = $type;
	}

	/**
	 * @param $type
	 * @return ResourceType
	 *
	 */
	public static function make($type) {
		return new static($type);
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->readType($this->type);
	}

	private function readType($type) {
		if (is_subclass_of($type, AbstractResource::class) || is_subclass_of($type, AbstractCollection::class)) {
			return $type::getType();
		} else if ($type instanceof TypeScriptEnum) {
			return $type->getIdentifier();
		} else if (is_array($type)) {
			$returns = [];
			foreach ($type as $t) {
				$returns[] = $this->readType($t);
			}
			return implode(" | ", $returns);
		} else if ($type != null) {
			return $type;
		} else {
			return 'any';
		}
	}

	/**
	 * @param mixed $type
	 * @return ResourceType
	 */
	public function setType($type) {
		$this->type = $type;
		return $this;
	}

	/**
	 * @param mixed $type
	 * @return ResourceType
	 */
	public function setAutoType($resourceClass) {
		$this->type = $resourceClass;
		return $this;
	}

}