<?php

namespace RajeevSiewnath\LumenReact\Library\TypeScript;

class TypeScriptDefinitions {

	private $enums = [];

	/**
	 * @param TypeScriptEnum $enum
	 * @return TypeScriptDefinitions
	 */
	public function addEnum(TypeScriptEnum $enum) {
		$this->enums[] = $enum;
	}

	/**
	 * @param TypeScriptEnum[] $enums
	 * @return TypeScriptDefinitions
	 */
	public function addEnums(...$enums) {
		foreach ($enums as $enum) {
			$this->addEnum($enum);
		}
	}

	/**
	 * @return TypeScriptEnum[]
	 */
	public function getEnums(): array {
		return $this->enums;
	}

	/**
	 * @param $identifier
	 * @return TypeScriptEnum|null
	 */
	public function getEnum($identifier) {
		foreach ($this->enums as $enum) {
			if ($enum->getIdentifier() == $identifier) {
				return $enum;
			}
		}
		return null;
	}

}