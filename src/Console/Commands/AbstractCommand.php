<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

use Illuminate\Console\Command;

abstract class AbstractCommand extends Command {

	protected function exec($command) {
		$o = null;
		return execute($command, function($output) use ($o) {
			$o = $output;
		}, function($response) use ($o) {
			if (!!$response) {
				throw new \Exception($o);
			} else {
				$this->info($o);
			}
		});
	}

}
