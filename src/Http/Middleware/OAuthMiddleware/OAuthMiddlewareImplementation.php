<?php

namespace RajeevSiewnath\LumenReact\Http\Middleware\OAuthMiddleware;

use Closure;
use GuzzleHttp\Psr7\Response;
use Illuminate\Http\Request;
use RajeevSiewnath\LumenReact\Exceptions\IllegalOperationException;
use RajeevSiewnath\LumenReact\Library\OAuth\OAuthBaseToken;
use RajeevSiewnath\LumenReact\Library\OAuth\OAuthBaseUser;
use RajeevSiewnath\LumenReact\Library\OAuth\OAuthState;
use RajeevSiewnath\LumenReact\Library\OAuth\Token\OAuthClientToken;
use RajeevSiewnath\LumenReact\Library\OAuth\Token\OAuthToken;
use RajeevSiewnath\LumenReact\Library\OAuth\User\OAuthClientUser;
use RajeevSiewnath\LumenReact\Library\OAuth\User\OAuthUser;
use Symfony\Component\HttpFoundation\Cookie;

abstract class OAuthMiddlewareImplementation {

	protected static $name = "";
	private static $providers = [
		GoogleOAuthMiddlewareImplementation::class,
		FacebookOAuthMiddlewareImplementation::class,
	];

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @param $action
	 * @return mixed
	 * @throws IllegalOperationException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function handle(Request $request, Closure $next, $action) {
		switch ($action) {
			case "login":
				return $this->loginHandle($request, $next, $request->has("offline") && !!$request->get("offline"));
			case "logout":
				return $this->logoutHandle($request, $next);
			case "check":
				return $this->checkHandle($request, $next);
			case "who":
				return $this->whoHandle($request, $next);
			default:
				$name = static::$name;
				throw new IllegalOperationException("Unsupported {$name} OAuth action");
		}
	}

	/**
	 * @param $name
	 * @return mixed|null
	 */
	public static function getProviderClassByName($name) {
		foreach (self::$providers as $provider) {
			if ($provider::$name === $name) {
				return $provider;
			}
		}

		return null;
	}

	/**
	 * @param Request $request
	 * @param $action
	 * @return mixed|null
	 * @throws IllegalOperationException
	 */
	public static function getOAuthProviderFromRequest(Request $request, $action) {
		$implementation = null;
		if ($action === "login") {
			if ($request->has("redirect") && $request->has('provider')) {
				$implementation = $request->get('provider');
			} else if ($request->has("code") && $request->has("state")) {
				$state = OAuthState::fromEncrypted($request->get("state"));
				$implementation = $state->getProvider();
			}
		} else {
			$accessToken = static::getAccessTokenFromRequest($request);
			if ($accessToken) {
				$token = OAuthToken::fromEncrypted($accessToken);
				$implementation = $token->getProvider();
			}
		}

		if (!$implementation) {
			throw new IllegalOperationException("The oAuth provider cannot be resolved");
		}

		return static::getProviderClassByName($implementation);
	}

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @param bool $offline
	 * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
	 * @throws IllegalOperationException
	 */
	private function loginHandle(Request $request, Closure $next, bool $offline) {
		if ($login = $this->doLogin($request, $next, $offline)) {
			return $login;
		}
		$name = static::$name;
		throw new IllegalOperationException("Broke out of {$name} OAuth flow");
	}

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @param bool $offline
	 * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
	 */
	abstract protected function doLogin(Request $request, Closure $next, bool $offline);

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @return mixed
	 * @throws IllegalOperationException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	private function logoutHandle(Request $request, Closure $next) {
		$response = $this->doLogout($request, $next);
		if ($response->getStatusCode() >= 400) {
			$name = static::$name;
			throw new IllegalOperationException("Invalid {$name} OAuth access token");
		}
		return $next($request);
	}

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	abstract protected function doLogout(Request $request, Closure $next);

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @return mixed
	 * @throws IllegalOperationException
	 */
	private function checkHandle(Request $request, Closure $next) {
		/** @var Response $response */
		$response = null;
		$user = static::getOAuthUserDataFromAccessToken(OAuthToken::fromEncrypted(static::getAccessTokenFromRequest($request)), $response);
		if ($response->getStatusCode() >= 400) {
			$name = static::$name;
			throw new IllegalOperationException("Invalid {$name} OAuth access token", $response->getStatusCode());
		}
		$request->merge(["oauth_user" => $user]);
		return $next($request);
	}

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @return mixed
	 */
	private function whoHandle(Request $request, Closure $next) {
		/** @var Response $response */
		$response = false;
		$user = static::getOAuthUserDataFromAccessToken(OAuthToken::fromEncrypted(static::getAccessTokenFromRequest($request)), $response);
		$request->merge(["oauth_user" => $user]);
		return $next($request);
	}

	/**
	 * @param OAuthToken $accessToken
	 * @param false|Response $response
	 * @param bool $newAccessToken
	 * @return mixed|null
	 */
	abstract public static function getOAuthUserDataFromAccessToken(OAuthToken $accessToken, &$response = false, &$newAccessToken = false);

	/**
	 * @param $encryptedToken
	 * @return OAuthClientUser|OAuthUser
	 */
	public static function getOAuthUserFromEncryptedToken($encryptedToken) {
		$token = OAuthToken::fromEncrypted($encryptedToken);
		/** @var Response $response */
		$response = null;
		/** @var OAuthMiddlewareImplementation $class */
		$class = static::getProviderClassByName($token->getProvider());
		$newAccessToken = null;
		if ($class) {
			$data = $class::getOAuthUserDataFromAccessToken($token, $response, $newAccessToken);
			if ($data) {
				if ($newAccessToken) {
					$token->setAccessToken($newAccessToken);
				}
				return $class::generateOAuthUser($data, $token->encrypt());
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	/**
	 * @param Request $request
	 * @return array|mixed|null|string
	 */
	public static function getAccessTokenFromRequest(Request $request) {
		if ($request->hasHeader("token")) {
			return $request->header("token");
		} else if ($request->has("token")) {
			return $request->get("token");
		} else {
			return "";
		}
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
	 * @throws IllegalOperationException
	 */
	public static function redirectWithQueryString(Request $request) {
		$state = OAuthState::fromEncrypted($request->get('state'));
		$token = $state->convertToOAuthToken($request->get("access_token"), $request->get("oauth_extra", null));
		$queryString = "token={$token->encrypt()}";
		$parsedUrl = parse_url($state->getRedirect());
		if (isset($parsedUrl["query"])) {
			$parsedUrl["query"] = "{$parsedUrl["query"]}&{$queryString}";
		} else {
			$parsedUrl["query"] = $queryString;
		}
		return redirect(unparseUrl($parsedUrl), 302);
	}

	/**
	 * @param Request $request
	 * @param null $domain
	 * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
	 * @throws IllegalOperationException
	 */
	public static function redirectWithCookie(Request $request, $domain = null) {
		$state = OAuthState::fromEncrypted($request->get('state'));
		if ($domain === null) {
			$parsedUrl = parse_url($state->getRedirect());
			$parts = explode('.', $parsedUrl["host"]);
			if (sizeof($parts) === 1) {
				$domain = $parts[0];
			} else {
				$domain = array_pop($parts);
				$name = array_pop($parts);
				$domain = "$name.$domain";
			}
		}
		$token = $state->convertToOAuthToken($request->get("access_token"), $request->get("oauth_extra", null));
		return redirect($state['redirect'])->withCookie(
			Cookie::create(
				"AUTH_COOKIE",
				json_encode(["token" => $token->encrypt(), "user" => []]),
				0,
				"/",
				$domain,
				isset($parsedUrl) && isset($parsedUrl['scheme']) && $parsedUrl['scheme'] === 'https' ? null : false,
				false,
				false,
				null
			)
		);
	}

	/**
	 * @param Request $request
	 * @return string
	 * @throws IllegalOperationException
	 */
	public static function resolveRedirectUrl(Request $request) {
		$url = null;
		if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
			$url = $request->url();
			if (!starts_with('https', $url)) {
				$url = 'https' . ltrim($url, 'http');
			}
		} else {
			$url = $request->url();
		}

		$validUrls = [];
		$frontEndUrl = env('APP_FRONTEND_URL');
		$backendUrl = env('APP_URL');
		if ($frontEndUrl) {
			$validUrls[] = $frontEndUrl;
		}
		if ($backendUrl) {
			$validUrls[] = $backendUrl;
		}

		if (empty($validUrls) || !starts_with($url, $validUrls)) {
			throw new IllegalOperationException("Invalid redirect url");
		}

		return $url;
	}

	/**
	 * @param $data
	 * @param $accessToken
	 * @return OAuthUser
	 */
	public abstract static function generateOAuthUser($data, $accessToken): OAuthUser;

	/**
	 * @return string
	 */
	public abstract static function createClientCredentialsToken(): string;

	/**
	 * @param $encryptedToken
	 * @return OAuthClientUser
	 */
	public abstract static function getOAuthClientUserFromEncryptedToken($encryptedToken);

	/**
	 * @param $encryptedToken
	 * @return OAuthClientUser|OAuthUser|OAuthBaseUser
	 * @throws IllegalOperationException
	 */
	public static function getOAuthAuthenticatableFromEncryptedToken($encryptedToken) {
		$token = OAuthBaseToken::fromEncrypted($encryptedToken);
		/** @var OAuthMiddlewareImplementation $class */
		$class = static::getProviderClassByName($token->getProvider());
		if ($token->getType() === 'user') {
			return $class::getOAuthUserFromEncryptedToken($encryptedToken);
		} else if ($token->getType() === 'client') {
			return $class::getOAuthClientUserFromEncryptedToken($encryptedToken);
		}
		throw new IllegalOperationException("Invalid OAuth token");
	}

	/**
	 * @param $encryptedToken
	 * @return OAuthToken|OAuthClientToken|OAuthBaseToken
	 * @throws IllegalOperationException
	 */
	public static function getOAuthDecryptedToken($encryptedToken) {
		$token = OAuthBaseToken::fromEncrypted($encryptedToken);
		/** @var OAuthMiddlewareImplementation $class */
		if ($token->getType() === 'user') {
			return OAuthToken::fromEncrypted($encryptedToken);
		} else if ($token->getType() === 'client') {
			return OAuthClientToken::fromEncrypted($encryptedToken);
		}
		throw new IllegalOperationException("Invalid OAuth token");
	}

}
