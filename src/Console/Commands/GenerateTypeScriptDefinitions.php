<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

use RajeevSiewnath\LumenReact\Library\TypeScript\TypeScriptDefinitions;

class GenerateTypeScriptDefinitions extends AbstractCommand {

	protected $signature = 'rs:lr:ts:types {outputPath? : Where typescript goes}';
	protected $description = 'Generate TypeScript templates';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle(TypeScriptDefinitions $typeScriptDefinitions) {
		if ($this->hasOption("outputPath")) {
			// TODO fix this
			$out = trim($this->argument("outputPath", "/\\"));
		} else {
			$out = config('custom.typeScriptDefOutputFolder');
		}

		if (trim($out) == "") {
			$this->error("no output path defined");
			return 1;
		}

		$defs = [];

		// Fetch all enums
		foreach ($typeScriptDefinitions->getEnums() as $enum) {
			$entries = $enum->getKeys();
			array_walk($entries, function(&$entry) {
				$entry = "$entry = \"$entry\"";
			});
			$defs[] = "export enum {$enum->getIdentifier()} {" . PHP_EOL . "\t" . implode(',' . PHP_EOL . "\t", $entries) . ',' . PHP_EOL . "}";
		}

		// Fetch all model files in folder
		foreach (glob(app()->path() . "/Http/Resources/*Resource.php") as $file) {
			require_once $file;

			// get the file name of the current file without the extension
			// which is essentially the class name
			$class = basename($file, '.php');
			if ($class == 'AbstractResource') {
				continue;
			}

			$fullClass = "\App\Http\Resources\\$class";
			if (class_exists($fullClass)) {
				$defs[] = $this->getFields($fullClass);
			} else {
				$this->error("class $class does not exists");
			}
		}
		$bar = $this->output->createProgressBar(sizeof($defs));

		$output = "";
		foreach ($defs as $def) {
			$bar->advance();
			$output .= $def . PHP_EOL . PHP_EOL;
		}

		$dest = rtrim($out, "/\\") . "/types.ts";
		file_put_contents($dest, $output);
		$this->info(PHP_EOL . "wrote typedefs to $dest");
		return 0;
	}

	private function getFields($class) {
		$className = $class::getType();
		$outputs = $class::bindings()->getOutputs();
		$fields = [];
		foreach ($outputs as $output) {
			$field = $output->getKey();
			$isOptional = $output->isTypeOptional() ? '?' : '';
			$fields[] = "\t{$field}{$isOptional}: {$output->getTypeString()};";
		}

		return "export interface {$className} {" . PHP_EOL . implode(PHP_EOL, $fields) . PHP_EOL . "}";
	}

}