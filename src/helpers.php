<?php

use RajeevSiewnath\LumenReact\Exceptions\IllegalOperationException;
use RajeevSiewnath\LumenReact\Library\RouterEntry\AbstractRouterEntry;

/**
 * @param $array
 * @param string $prefix
 * @return array
 */
function flattenArrayKeys($array, $prefix = '') {
	$keys = [];
	foreach ($array as $key => $value) {
		if (!is_array($value)) {
			$keys[] = trim("{$prefix}.{$key}", '.');
		} else {
			$keys = array_merge($keys, flattenArrayKeys($value, $key));
		}
	}
	return $keys;
}

/**
 * @param $array
 * @return array
 */
function arrayFlatExpand($array) {
	$return = [];

	function _expandAssoc($row) {
		$return = $row;
		$hasAssoc = false;

		do {
			$hasAssoc = false;
			foreach ($return as $key => $value) {
				if (is_array($value)) {
					if (empty($value)) {
						$return[$key] = null;
					} else {
						if (isAssoc($value)) {
							foreach ($value as $k => $v) {
								$return["{$key}.{$k}"] = $v;
							}
							$hasAssoc = true;
							unset($return[$key]);
							break;
						} else {
							foreach ($value as $k => $v) {
								$return[$key][$k] = (is_array($v) && isAssoc($v)) ? _expandAssoc($v) : $v;
							}
						}
					}
				} else {
					$return[$key] = $value;
				}
			}
		} while ($hasAssoc);

		return $return;
	}

	function _expandIndexed($row) {
		$rows = [_expandAssoc($row)];
		$hasNested = false;

		do {
			$hasNested = false;
			foreach ($rows as $index => $innerRow) {
				foreach ($innerRow as $key => $value) {
					if (is_array($value) && !isAssoc($value)) {
						$newRows = [];
						foreach ($value as $v) {
							$newRow = $innerRow;
							$newRow[$key] = (is_array($v) && !isAssoc($v)) ? _expandIndexed($v) : $v;
							$newRows[] = $newRow;
						}
						array_splice($rows, $index, 1);
						$rows = array_merge($rows, $newRows);
						$hasNested = true;
						break 2;
					}
				}
			}
		} while ($hasNested);

		return $rows;
	}

	foreach ($array as $row) {
		$return = array_merge($return, _expandIndexed($row));
	}

	foreach ($return as $index => $row) {
		$return[$index] = _expandAssoc($row);
	}

	return $return;
}

/**
 * @param array $arr
 * @return bool
 */
function isAssoc(array $arr) {
	if ([] === $arr)
		return false;
	return array_keys($arr) !== range(0, count($arr) - 1);
}

/**
 * @param $action
 * @param $class
 * @return string
 */
function canDo($action, $class) {
	return "can:{$action},{$class}";
}

/**
 * @param $command
 * @param null $outCallback
 * @param null $responseCallback
 * @return string
 */
function execute($command, $outCallback = null, $responseCallback = null) {
	$output = null;
	$response = null;
	$return = exec($command, $output, $response);
	if ($outCallback) {
		call_user_func($outCallback, $output);
	}
	if ($responseCallback) {
		call_user_func($responseCallback, $response);
	}
	return $return;
}

/**
 * @param $router
 * @param $path
 * @param AbstractRouterEntry ...$entries
 */
function registerGenericRoute($router, $path, AbstractRouterEntry ...$entries) {
	$middlewares = [];
	$routeName = '';
	foreach ($entries as $entry) {
		$middlewares = array_merge($middlewares, $entry->getMiddlewares());
		$routeName .= ucfirst($entry->getRouteName());
	}
	$routeName = lcfirst($routeName);
	$router->get($path, ['middleware' => array_merge(['genericRouterEntry'], $middlewares), 'as' => $routeName, function(\Illuminate\Http\Request $request) use ($router, $entries) {
		$response = [];
		$isError = false;
		foreach ($entries as $entry) {
			if (!$entry->isSecure($request)) {
				throw new IllegalOperationException("This request is not permitted");
			}
			$data = $entry->doRouteAction($request, $router);
			$isError = $isError || $entry->isError($data);
			if (is_array($data)) {
				$response = array_merge($response, $data);
			}
		}
		return response($response, $isError ? 500 : 200);
	}]);
}

/**
 * @param $parsedUrl
 * @return string
 */
function unparseUrl($parsedUrl) {
	$scheme = isset($parsedUrl['scheme']) ? $parsedUrl['scheme'] . '://' : '';
	$host = isset($parsedUrl['host']) ? $parsedUrl['host'] : '';
	$port = isset($parsedUrl['port']) ? ':' . $parsedUrl['port'] : '';
	$user = isset($parsedUrl['user']) ? $parsedUrl['user'] : '';
	$pass = isset($parsedUrl['pass']) ? ':' . $parsedUrl['pass'] : '';
	$pass = ($user || $pass) ? "$pass@" : '';
	$path = isset($parsedUrl['path']) ? $parsedUrl['path'] : '';
	$query = isset($parsedUrl['query']) ? '?' . $parsedUrl['query'] : '';
	$fragment = isset($parsedUrl['fragment']) ? '#' . $parsedUrl['fragment'] : '';
	return "$scheme$user$pass$host$port$path$query$fragment";
}

/**
 * @param $data
 * @return string
 */
function base64url_encode($data) {
	return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}