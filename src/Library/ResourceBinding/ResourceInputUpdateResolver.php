<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

class ResourceInputUpdateResolver {

	private $resolver;

	/**
	 * ResourceInputUpdateResolver constructor.
	 * @param $resolver
	 */
	public function __construct($resolver) {
		$this->resolver = $resolver;
	}

	public static function make($resolver) {
		return new static($resolver);
	}

	/**
	 * @return mixed
	 */
	public function getResolver() {
		return $this->resolver;
	}

	/**
	 * @param mixed $resolver
	 * @return ResourceInputUpdateResolver
	 */
	public function setResolver($resolver) {
		$this->resolver = $resolver;
		return $this;
	}

	/**
	 * @param $oldValue
	 * @param $newValue
	 * @param $allOldValues
	 * @param $allNewValues
	 * @return mixed
	 */
	public function resolve($oldValue, $newValue, $allOldValues, $allNewValues) {
		if ($this->resolver instanceof ResourceCallable) {
			return call_user_func($this->resolver->getCallable(), $oldValue, $newValue, $allOldValues, $allNewValues);
		} else {
			return $this->resolver;
		}
	}

}