<?php

namespace RajeevSiewnath\LumenReact\Providers;

use Illuminate\Support\ServiceProvider;
use RajeevSiewnath\LumenReact\Http\Middleware\TypeScriptActionable;
use RajeevSiewnath\LumenReact\Http\Middleware\TypeScriptActionableMiddleware;
use RajeevSiewnath\LumenReact\Library\TypeScript\TypeScriptDefinitions;

class TypeScriptServiceProvider extends ServiceProvider {

	public function boot() {
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->singleton(TypeScriptDefinitions::class, function($app) {
			return new TypeScriptDefinitions();
		});

		$this->app->routeMiddleWare([
			'typeScriptActionable' => TypeScriptActionableMiddleware::class,
		]);
	}
}
