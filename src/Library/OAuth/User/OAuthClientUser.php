<?php

namespace RajeevSiewnath\LumenReact\Library\OAuth\User;

use RajeevSiewnath\LumenReact\Library\OAuth\OAuthBaseUser;

class OAuthClientUser extends OAuthBaseUser {

	public function __construct($id, $accessToken, $provider) {
		parent::__construct($id, $accessToken, $provider, true);
	}

	public function toArray() {
		return [
			'id'           => $this->id,
			'provider'     => $this->provider,
			'accessToken'  => $this->accessToken,
			'isClientUser' => $this->isClientUser,
		];
	}

}