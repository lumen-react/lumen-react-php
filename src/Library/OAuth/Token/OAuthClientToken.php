<?php

namespace RajeevSiewnath\LumenReact\Library\OAuth\Token;

use Illuminate\Support\Facades\Crypt;
use RajeevSiewnath\LumenReact\Library\OAuth\OAuthBaseToken;

class OAuthClientToken extends OAuthBaseToken {

	/**
	 * OAuthClientToken constructor.
	 * @param string $accessToken
	 * @param string $provider
	 */
	public function __construct(string $accessToken, string $provider) {
		parent::__construct($accessToken, $provider, 'client');
	}

	/**
	 * @param $encrypted
	 * @return OAuthClientToken
	 */
	public static function fromEncrypted($encrypted) {
		$data = json_decode(Crypt::decrypt($encrypted), true);
		return new OAuthClientToken(
			$data['access_token'],
			$data['provider']
		);
	}

}