<?php

namespace RajeevSiewnath\LumenReact\Library\RouterEntry;

use Illuminate\Http\Request;
use RajeevSiewnath\LumenReact\Exceptions\IllegalOperationException;

abstract class AbstractRouterEntry {

	protected $secure = true;

	/**
	 * AbstractRouterEntry constructor.
	 * @param bool|callable $secure
	 */
	public function __construct($secure = true) {
		$this->secure = $secure;

	}

	/**
	 * @return bool
	 * @throws IllegalOperationException
	 */
	public function isSecure(Request $request): bool {
		if (is_bool($this->secure)) {
			return $this->secure;
		} else if (is_callable($this->secure)) {
			return call_user_func($this->secure, $request);
		} else {
			throw new IllegalOperationException("Not a supported type");
		}
	}

	/**
	 * @param bool|callable $secure
	 */
	public function setSecure($secure) {
		$this->secure = $secure;
	}

	/**
	 * @return mixed
	 */
	abstract function doRouteAction(Request $request, $router);

	/**
	 * @return string
	 */
	abstract function getRouteName();

	/**
	 * @return array
	 */
	public function getMiddlewares(): array {
		return [];
	}

	/**
	 * @param $data
	 * @return bool
	 */
	public function isError($data) {
		return false;
	}

}