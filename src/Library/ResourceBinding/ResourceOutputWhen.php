<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;
use Illuminate\Http\Request;

class ResourceOutputWhen {

	private $when;

	/**
	 * ResourceInputWhen constructor.
	 * @param $when
	 */
	public function __construct($when) {
		$this->when = $when;
	}

	/**
	 * @param $when
	 * @return ResourceOutputWhen
	 */
	public static function make($when) {
		return new static($when);
	}

	/**
	 * @param AbstractResource $resource
	 * @param Request $request
	 * @return mixed
	 */
	public function isWhen(AbstractResource $resource, Request $request) {
		if ($this->when instanceof ResourceCallable) {
			return call_user_func($this->when->getCallable(), $resource, $request);
		} else {
			return $this->when;
		}
	}

	/**
	 * @return mixed
	 */
	public function getWhen() {
		return $this->when;
	}

	/**
	 * @param mixed $when
	 * @return ResourceOutputWhen
	 */
	public function setWhen($when) {
		$this->when = $when;
		return $this;
	}

}