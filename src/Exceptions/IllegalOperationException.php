<?php

namespace RajeevSiewnath\LumenReact\Exceptions;

use Throwable;

class IllegalOperationException extends \Exception {

	private $meta = null;

	public function __construct(string $message = "", int $code = 400, $meta = null, Throwable $previous = null) {
		parent::__construct($message, $code, $previous);
		$this->meta = $meta;
	}

	public function meta() {
		return $this->meta;
	}

}