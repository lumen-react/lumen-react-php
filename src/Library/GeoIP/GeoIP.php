<?php

namespace RajeevSiewnath\LumenReact\Library\GeoIP;

use GeoIp2\Database\Reader;
use GeoIp2\Exception\AddressNotFoundException;

class GeoIP {

	private $reader;

	/**
	 * @throws \GeoIp2\Exception\AddressNotFoundException
	 * @throws \MaxMind\Db\Reader\InvalidDatabaseException
	 * @throws \Exception
	 */
	public function init() {
		// This creates the Reader object, which should be reused across
		// lookups.
		$dir = __DIR__;
		$file = "$dir/../../geo-ip-db.mmdb";
		if (!file_exists($file)) {
			throw new \Exception("Geo IP database is not set");
		}
		$this->reader = new Reader($file);
	}

	/**
	 * @param $ip
	 * @return null|GeoIPData
	 */
	public function get($ip) {
		// Replace "city" with the appropriate method for your database, e.g.,
		// "country".
		// Test IP: 194.110.177.46
		try {
			$record = $this->reader->city($ip);
			return new GeoIPData(
				$record->country->isoCode,
				$record->country->name,
				$record->country->names,
				$record->mostSpecificSubdivision->name,
				$record->mostSpecificSubdivision->isoCode,
				$record->city->name,
				$record->postal->code,
				$record->location->latitude,
				$record->location->longitude
			);
		} catch (AddressNotFoundException $e) {
			;
		}
		return null;
	}

}