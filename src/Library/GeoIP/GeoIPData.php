<?php

namespace RajeevSiewnath\LumenReact\Library\GeoIP;

class GeoIPData {

	private $countryIso;
	private $countryName;
	private $countryNames;

	private $subdivisionName;
	private $subdivisionIso;

	private $cityName;
	private $postalCode;

	private $latitude;
	private $longitude;

	/**
	 * GeoIPData constructor.
	 * @param $countryIso
	 * @param $countryName
	 * @param $countryNames
	 * @param $subdivisionName
	 * @param $subdivisionIso
	 * @param $cityName
	 * @param $postalCode
	 * @param $latitude
	 * @param $longitude
	 */
	public function __construct($countryIso, $countryName, $countryNames, $subdivisionName, $subdivisionIso, $cityName, $postalCode, $latitude, $longitude) {
		$this->countryIso = $countryIso;
		$this->countryName = $countryName;
		$this->countryNames = $countryNames;
		$this->subdivisionName = $subdivisionName;
		$this->subdivisionIso = $subdivisionIso;
		$this->cityName = $cityName;
		$this->postalCode = $postalCode;
		$this->latitude = $latitude;
		$this->longitude = $longitude;
	}

	/**
	 * @return mixed
	 */
	public function getCountryIso() {
		return $this->countryIso;
	}

	/**
	 * @return mixed
	 */
	public function getCountryName() {
		return $this->countryName;
	}

	/**
	 * @return mixed
	 */
	public function getCountryNames() {
		return $this->countryNames;
	}

	/**
	 * @return mixed
	 */
	public function getSubdivisionName() {
		return $this->subdivisionName;
	}

	/**
	 * @return mixed
	 */
	public function getSubdivisionIso() {
		return $this->subdivisionIso;
	}

	/**
	 * @return mixed
	 */
	public function getCityName() {
		return $this->cityName;
	}

	/**
	 * @return mixed
	 */
	public function getPostalCode() {
		return $this->postalCode;
	}

	/**
	 * @return mixed
	 */
	public function getLatitude() {
		return $this->latitude;
	}

	/**
	 * @return mixed
	 */
	public function getLongitude() {
		return $this->longitude;
	}

	public function toArray() {
		return [
			'countryIso'      => $this->countryIso,
			'countryName'     => $this->countryName,
			'countryNames'    => $this->countryNames,
			'subdivisionName' => $this->subdivisionName,
			'subdivisionIso'  => $this->subdivisionIso,
			'cityName'        => $this->cityName,
			'postalCode'      => $this->postalCode,
			'latitude'        => $this->latitude,
			'longitude'       => $this->longitude,
		];
	}

}