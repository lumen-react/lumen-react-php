<?php

namespace RajeevSiewnath\LumenReact\Http\Controllers;

use RajeevSiewnath\LumenReact\AbstractModel;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Laravel\Lumen\Routing\Controller as BaseController;
use RajeevSiewnath\LumenReact\Library\ResponseScope\ResponseScope;

abstract class AbstractController extends BaseController {

	public static $resourceClass;

	public function __construct() {
//		$this->middleware(function(){throw new \Exception("boom");});
	}

	/**
	 * @param string $action
	 * @param Request $request
	 * @param AbstractModel|AbstractCollection $modelOrCollection
	 * @return bool
	 */
	public static function doAuthorization(string $action, Request $request, $modelOrCollection) {
		return true;
	}

	/**
	 * @return AbstractResource
	 */
	public static function resourceClass() {
		return static::$resourceClass;
	}

	/**
	 * @param string $action
	 * @return AbstractResource|AbstractCollection|null
	 */
	public static function input($action) {
		switch ($action) {
			case "store":
			case "update":
				return static::resourceClass();
			case "index":
			case "show":
			case "destroy":
			default:
				return null;
		}
	}

	public function executePostRequestActions(Request $request, $response) {
		if ($request->has('redirect')) {
			return redirect()->to($request->get('redirect'));
		}
		$download = $request->query('download');

		if ($download) {
			$filename = $request->query('filename');
			$filename = $filename ? $filename : isset($request->route()[1]['as']) ? $request->route()[1]['as'] : 'download';
			switch ($download) {
				case 'csv':
					$tmp = tempnam(sys_get_temp_dir(), 'csv');
					$fp = fopen($tmp, 'w');

					$json = json_decode($response->response($request)->getContent(), true);

					if ($json) {
						if (isAssoc($json)) {
							$json = [$json];
						}
						$flattened = arrayFlatExpand($json);
						fputcsv($fp, array_keys($flattened[0]));
						foreach ($flattened as $flattenedRow) {
							fputcsv($fp, $flattenedRow);
						}
					}

					fclose($fp);

					return response()->download($tmp, "{$filename}.csv", [
						'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
						'Content-Type'        => 'text/csv',
						'Content-Disposition' => "attachment; filename={$filename}.csv",
						'Expires'             => '0',
						'Pragma'              => 'public',
					]);
			}
		}

		return response($response)->withHeaders(app(ResponseScope::class)->getOutputHeaders());
	}

	/**
	 * @param string $action
	 * @return AbstractResource|AbstractCollection|null
	 */
	public static function output($action) {
		switch ($action) {
			case "index":
				if (is_subclass_of(static::resourceClass(), AbstractResource::class)) {
					return static::resourceClass()::collectionClass();
				} else {
					return static::resourceClass();
				}
			case "store":
			case "show":
			case "update":
				return static::resourceClass();
			case "destroy":
			default:
				return null;
		}
	}

	/**
	 * @param Request $request
	 * @return AbstractCollection
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	public function index(Request $request) {
		return $this->indexLike($request, function($staticContext) {
			/** @var mixed $staticContext */
			return $staticContext->get();
		});
	}

	/**
	 * @param Request $request
	 * @param callable $staticContentCallback
	 * @param string $action
	 * @return AbstractCollection
	 * @throws \Illuminate\Auth\Access\AuthorizationException|\Exception
	 */
	protected function indexLike(Request $request, $staticContentCallback, $action = 'index') {
		$this->preIndex($request);

		if (static::doAuthorization($action, $request, null)) {
			$this->authorize($action, static::resourceClass()::modelClass());
		}

		$this->postIndex($request);

		return $this->executePostRequestActions($request,
			$this->getResourceCollectionFromEloquent(
				$request,
				static::output($action),
				$staticContentCallback
			)
		);
	}

	/**
	 * @param Request $request
	 */
	public function preIndex(Request $request) {
	}

	/**
	 * @param Request $request
	 */
	public function postIndex(Request $request) {
	}

	/**
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	public function store(Request $request) {
		return $this->storeLike($request,
			function($model) {
				return function($staticContext) use ($model) {
					/** @var mixed $staticContext */
					return $staticContext->findOrFail($model->getPrimaryKey());
				};
			}
		);
	}

	/**
	 * @param Request $request
	 * @param $staticContentCallback
	 * @param string $action
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	protected function storeLike(Request $request, $staticContentCallback, $action = 'store') {
		$this->preStore($request);
		$this->preSave($request, null);

		if (static::doAuthorization($action, $request, null)) {
			$this->authorize($action, static::resourceClass()::modelClass());
		}

		$this->validateResource($request, static::input($action), $action);
		$model = static::input($action)::fillModelFromRequest($request);
		$model->save();

		if (static::doAuthorization($action, $request, $model)) {
			$this->authorize('post' . ucfirst($action), $model);
		}

		$this->postStore($request, $model);
		$this->postSave($request, $model);

		return $this->executePostRequestActions($request,
			$this->getResourceFromEloquent(
				$request,
				static::output($action),
				call_user_func($staticContentCallback, $model)
			)
		);
	}

	/**
	 * @param Request $request
	 * @param AbstractModel|null $model
	 */
	public function preSave(Request $request, $model) {
	}

	/**
	 * @param Request $request
	 * @param AbstractModel $model
	 */
	public function postSave(Request $request, AbstractModel $model) {
	}

	/**
	 * @param Request $request
	 */
	public function preStore(Request $request) {
	}

	/**
	 * @param Request $request
	 * @param AbstractModel $model
	 */
	public function postStore(Request $request, AbstractModel $model) {
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return mixed
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	public function show(Request $request, $id) {
		return $this->showLike($request, function($staticContext) use ($id) {
			/** @var mixed $staticContext */
			return $staticContext->findOrFail($id);
		});
	}

	/**
	 * @param Request $request
	 * @param $staticContextCallback
	 * @param string $action
	 * @return mixed
	 * @throws \Illuminate\Auth\Access\AuthorizationException|\Exception
	 */
	protected function showLike(Request $request, $staticContextCallback, $action = 'show') {
		$this->preShow($request);

		$model = $this->getModelFromEloquent(
			$request,
			static::output($action),
			$staticContextCallback
		);

		if (static::doAuthorization('show', $request, $model)) {
			$this->authorize('show', $model);
		}

		$this->postShow($request, $model);

		return $this->executePostRequestActions($request, $model->toResource());
	}

	/**
	 * @param Request $request
	 */
	public function preShow(Request $request) {
	}


	/**
	 * @param Request $request
	 * @param AbstractModel $model
	 */
	public function postShow(Request $request, AbstractModel $model) {
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	public function update(Request $request, $id) {
		return $this->updateLike($request, $id,
			function($staticContext) use ($id) {
				/** @var mixed $staticContext */
				return $staticContext->findOrFail($id);
			},
			function($model) {
				return function($staticContext) use ($model) {
					/** @var mixed $staticContext */
					return $staticContext->findOrFail($model->getPrimaryKey());
				};
			}
		);
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @param $staticContextCallback1
	 * @param $staticContextCallback2
	 * @param string $action
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	protected function updateLike(Request $request, $id, $staticContextCallback1, $staticContextCallback2, $action = 'update') {
		$this->preUpdateModelQuery($request);

		$modelClass = static::resourceClass()::modelClass();
		/** @var mixed $modelClass */
		$model = $this->getModelFromEloquent(
			$request,
			static::input($action),
			$staticContextCallback1);

		$this->preUpdate($request, $model);
		$this->preSave($request, $model);

		$this->validateResource($request, static::input($action), $action, $model);

		if (static::doAuthorization($action, $request, $model)) {
			$this->authorize($action, $model);
		}

		$model = static::resourceClass()::fillModelFromRequest($request, $model);
		$model->save();

		if (static::doAuthorization($action, $request, $model)) {
			$this->authorize('post' . ucfirst($action), $model);
		}

		$this->postUpdate($request, $model);
		$this->postSave($request, $model);

		return $this->executePostRequestActions($request,
			$this->getResourceFromEloquent(
				$request,
				static::output($action),
				call_user_func($staticContextCallback2, $model))
		);
	}

	/**
	 * @param Request $request
	 */
	public function preUpdateModelQuery(Request $request) {
	}

	/**
	 * @param Request $request
	 * @param AbstractModel $model
	 */
	public function preUpdate(Request $request, AbstractModel $model) {
	}

	/**
	 * @param Request $request
	 * @param AbstractModel $model
	 */
	public function postUpdate(Request $request, AbstractModel $model) {
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 * @throws \Exception
	 */
	public function destroy(Request $request, $id) {
		return $this->destroyLike($request, $id, function($staticContext) use ($id) {
			/** @var mixed $staticContext */
			return $staticContext->findOrFail($id);
		});
	}

	/**
	 * @param Request $request
	 * @param $id
	 * @param $staticContextCallback
	 * @param string $action
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws \Illuminate\Auth\Access\AuthorizationException
	 */
	protected function destroyLike(Request $request, $id, $staticContextCallback, $action = 'destroy') {
		$this->preDestroyModelQuery($request);

		$modelClass = static::resourceClass()::modelClass();

		/** @var mixed $modelClass */
		$model = $this->getModelFromEloquent(
			$request,
			static::input('store'),
			$staticContextCallback);

		$this->preDestroy($request, $model);

		if (static::doAuthorization($action, $request, $model)) {
			$this->authorize($action, $model);
		}

		/** @var mixed $model */
		$this->postDestroy($request, $model, $model->delete());

		return $this->executePostRequestActions($request, null);
	}

	/**
	 * @param Request $request
	 */
	public function preDestroyModelQuery(Request $request) {
	}

	/**
	 * @param Request $request
	 * @param AbstractModel $model
	 */
	public function preDestroy(Request $request, AbstractModel $model) {
	}

	/**
	 * @param Request $request
	 * @param AbstractModel $model
	 * @param boolean $deleted
	 */
	public function postDestroy(Request $request, AbstractModel $model, $deleted) {
	}

	/**
	 * @param Request $request
	 * @param AbstractResource|string $resourceClass
	 * @param string $action
	 * @param AbstractModel|null $model
	 */
	public function validateResource(Request $request, $resourceClass, $action = 'default', AbstractModel $model = null) {
		$this->validate($request, $resourceClass::validators($request, $action, $model));
	}

	/**
	 * @param Request $request
	 * @param static $resourceClass
	 * @return AbstractResource
	 */
	public function getResource(Request $request, $resourceClass) {
		return new $resourceClass($request);
	}

	/**
	 * @param Request $request
	 * @param AbstractResource $resourceClass
	 * @param callable $modelCallback
	 * @return AbstractResource
	 * @throws \Exception
	 */
	public function getResourceFromEloquent(Request $request, $resourceClass, $modelCallback) {
		$model = $resourceClass::resourceWrapper(
			$request,
			$modelCallback
		);
		return $model ? $model->toResource() : null;
	}

	/**
	 * @param Request $request
	 * @param AbstractResource $resourceClass
	 * @param $modelCallback
	 * @return mixed
	 * @throws \Exception
	 */
	public function getModelFromEloquent(Request $request, $resourceClass, $modelCallback) {
		return $resourceClass::resourceWrapper(
			$request,
			$modelCallback
		);
	}

	/**
	 * @param Request $request
	 * @param AbstractResource|AbstractCollection $resourceClass
	 * @param callable $modelCallback
	 * @return AbstractCollection
	 * @throws \Exception
	 */
	public function getResourceCollectionFromEloquent(Request $request, $resourceClass, $modelCallback) {
		if (is_subclass_of($resourceClass, AbstractResource::class)) {
			return $resourceClass::toCollection(
				$resourceClass::resourceWrapper(
					$request,
					$modelCallback
				));
		} else if (is_subclass_of($resourceClass, AbstractCollection::class)) {
			/** @var AbstractResource $actualResourceClass */
			$actualResourceClass = $resourceClass::resourceClass();
			return new $resourceClass(
				$actualResourceClass::resourceWrapper(
					$request,
					$modelCallback
				));
		} else {
			throw new \Exception("Cannot create resource collection");
		}
	}

	/**
	 * @param $instance
	 * @param $field
	 * @param $instanceCallback
	 * @return mixed
	 */
	private function getFileInstance($instance, $field, $instanceCallback) {
		if (!is_callable($instanceCallback)) {
			return $instance->{$field};
		} else {
			return call_user_func($instanceCallback, $instance, $field, null);
		}
	}

	/**
	 * @param Model $instance
	 * @param $field
	 * @param $instanceCallback
	 * @param $value
	 * @return mixed
	 */
	private function setFileInstance($instance, $field, $instanceCallback, $value) {
		if (!is_callable($instanceCallback)) {
			$instance->{$field} = $value;
			$instance->save();
		} else {
			call_user_func($instanceCallback, $instance, $field, $value);
		}
	}

	/**
	 * @param Request $request
	 * @param $instance
	 * @param $field
	 * @param $path
	 * @param bool $instanceCallback
	 * @param array $validations
	 * @param bool $clean
	 * @param array $sizes
	 * @throws \Exception
	 */
	protected function saveImageWithThumbs(Request $request, $instance, $field, $path, $instanceCallback = true, $validations = ['image', 'file', 'max:' . 1024 * 5], $clean = true, $sizes = [20, 40, 60, 100]) {
		list($filePath, $pathInfo) = $this->saveFile($request, $instance, $field, $path, $instanceCallback, $validations, $clean);

		foreach ($sizes as $size) {
			$thumb = Image::make($filePath);
			$thumb->fit($size, $size, function($constraint) {
				$constraint->aspectRatio();
			});
			$thumb = (string)$thumb->encode();
			Storage::put("{$path}/{$pathInfo['filename']}-{$size}x{$size}.{$pathInfo['extension']}", $thumb);
		}
	}

	/**
	 * @param Request $request
	 * @param $instance
	 * @param $field
	 * @param $path
	 * @param bool $instanceCallback
	 * @param array $validations
	 * @param bool $clean
	 * @return array
	 * @throws \Exception
	 */
	protected function saveFile(Request $request, $instance, $field, $path, $instanceCallback = true, $validations = [], $clean = true) {
		// Validate
		$this->validate($request, [$field => $validations]);

		// Delete old file
		$file = $this->getFileInstance($instance, $field, $instanceCallback);
		if ($clean && $file) {
			$filePath = Storage::getDriver()->getAdapter()->applyPathPrefix($file);
			$pathInfo = pathinfo($filePath);
			$globPattern = str_replace($pathInfo['filename'], "{$pathInfo['filename']}*", $filePath);
			foreach (glob($globPattern) as $filename) {
				$pathInfo = pathinfo($filename);
				Storage::delete("{$path}/{$pathInfo['filename']}.{$pathInfo['extension']}");
			}
		}

		// Store new file
		$file = $request->file($field)->store($path);

		// Save file to database
		try {
			$this->setFileInstance($instance, $field, $instanceCallback, $file);
		} catch (\Exception $e) {
			Storage::delete($file);
			throw($e);
		}

		// Create different sized files
		$filePath = Storage::getDriver()->getAdapter()->applyPathPrefix($file);
		$pathInfo = pathinfo($filePath);

		return [$filePath, $pathInfo];
	}

}
