<?php

namespace RajeevSiewnath\LumenReact\Providers;

use Illuminate\Support\ServiceProvider;
use RajeevSiewnath\LumenReact\Http\Middleware\DatabaseTransactionMiddleware;

class DatabaseTransactionServiceProvider extends ServiceProvider {

	public function boot() {
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->routeMiddleWare([
			'databaseTransaction' => DatabaseTransactionMiddleware::class,
		]);
	}
}
