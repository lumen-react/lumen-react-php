<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

class GenerateModelClass extends AbstractCommand {

	protected $signature = 'rs:lr:resource:model {--o|overwrite : Overwrite if the file already exists} {resourceName : Where typescript goes} {table? : Table name (defaults to resource name)}';
	protected $description = 'Generate resource model';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$resourceName = $this->argument('resourceName');
		$overwrite = $this->option('overwrite');
		if ($this->hasOption("table")) {
			$table = $this->argument("table");
		} else {
			$table = $table = snake_case($resourceName);
		}

		$appPath = app()->path();
		$className = ucfirst($resourceName);
		$resource = "${className}Resource";
		$filename = "${appPath}/{$className}.php";

		if (file_exists($filename)) {
			if ($overwrite) {
				$this->warn("${filename} already exists, but overwrite is enabled");
			} else {
				$this->warn("${filename} already exists, exiting");
				return 0;
			}
		}

		$template = $this->getTemplate();
		$template = preg_replace('/\[CLASS\]/', $className, $template);
		$template = preg_replace('/\[RESOURCE\]/', $resource, $template);
		$template = preg_replace('/\[TABLE\]/', $table, $template);

		file_put_contents($filename, $template);

		$this->info( "created $filename for class $className");
		return 0;
	}

	private function getTemplate() {
		return <<< STRING
<?php

namespace App;

use App\Http\Resources\[RESOURCE];
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class [CLASS] extends \RajeevSiewnath\LumenReact\AbstractModel {

	use SoftDeletes;

	public static \$resourceClass = [RESOURCE]::class;

	protected \$table = '[TABLE]';
	protected \$primaryKey = '[TABLE]_id';
	protected \$dates = ['deleted_at'];
	protected \$fillable = [];

	protected \$casts = [
		'[TABLE]_id' => 'integer',
	];

	protected static function boot() {
		parent::boot();

		static::addGlobalScope('select', function(Builder \$builder) {
			\$builder->select('[TABLE].*');
		});
	}

}
STRING;

	}

}