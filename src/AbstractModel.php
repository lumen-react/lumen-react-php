<?php

namespace RajeevSiewnath\LumenReact;

use RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

abstract class AbstractModel extends Model {

	public static $resourceClass;

	/**
	 * @return AbstractResource
	 */
	public static function resourceClass() {
		return static::$resourceClass;
	}

	/**
	 * @return mixed
	 */
	public function getPrimaryKey() {
		return $this->{$this->primaryKey};
	}

	/**
	 * @return string
	 */
	public function getPrimaryKeyKey() {
		return $this->primaryKey;
	}

	/**
	 * @return AbstractResource
	 */
	public function toResource() {
		return new static::$resourceClass($this);
	}

	/**
	 * @param $collection
	 * @return AbstractCollection
	 */
	public static function toCollection($collection) {
		return static::resourceClass()::toCollection($collection);
	}

	/**
	 * @param string $key
	 * @return null|string
	 */
	public function getCastType($key) {
		return !empty($this->casts[$key]) ? $this->casts[$key] : null;
	}

	/**
	 * @param $request
	 * @param null $model
	 * @return AbstractModel
	 */
	public static function fillModelFromRequest($request, $model = null) {
		return static::resourceClass()::fillModelFromRequest($request, $model);
	}

	/**
	 * @param $storagePath
	 * @return array|null
	 */
	protected function getImageWithThumbs($storagePath) {
		if (Storage::exists($storagePath)) {
			$return = array();
			$return['file'] = Storage::url($storagePath);

			$thumbs = array();
			$filePath = Storage::getDriver()->getAdapter()->applyPathPrefix($storagePath);
			$pathInfo = pathinfo($filePath);
			$originalFilename = $pathInfo['filename'];
			$globPattern = str_replace($pathInfo['filename'], "{$pathInfo['filename']}*", $filePath);
			$regexp = "/.*{$pathInfo['filename']}-(.*)\.{$pathInfo['extension']}$/";
			foreach (glob($globPattern) as $filename) {
				$pathInfo = pathinfo($filename);
				$matches = array();
				if (preg_match($regexp, $filename, $matches)) {
					$thumbs[$matches[1]] = str_replace($originalFilename, $pathInfo['filename'], $storagePath);
					$thumbs[$matches[1]] = Storage::url($thumbs[$matches[1]]);
				}
			}
			if (!empty($thumbs)) {
				$return['thumbs'] = $thumbs;
			}

			return $return;
		} else {
			return null;
		}
	}

	/**
	 * @param $storagePath
	 * @return array|null
	 */
	protected function getImageWithDimensions($storagePath) {
		if (Storage::exists($storagePath)) {
			$return = array();
			$return['file'] = Storage::url($storagePath);
			$filePath = Storage::getDriver()->getAdapter()->applyPathPrefix($storagePath);
			$file = Image::make($filePath);
			$return['width'] = $file->width();
			$return['height'] = $file->height();
			return $return;
		} else {
			return null;
		}
	}

}