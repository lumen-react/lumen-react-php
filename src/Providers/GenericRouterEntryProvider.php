<?php

namespace RajeevSiewnath\LumenReact\Providers;

use Illuminate\Support\ServiceProvider;
use RajeevSiewnath\LumenReact\Http\Middleware\GenericRouterEntryMiddleware;

class GenericRouterEntryProvider extends ServiceProvider {

	public function boot() {
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->routeMiddleWare([
			'genericRouterEntry' => GenericRouterEntryMiddleware::class,
		]);
	}
}
