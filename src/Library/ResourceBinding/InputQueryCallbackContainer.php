<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

class InputQueryCallbackContainer {

	private $inputQueryCallbacks = [];

	/**
	 * @param string $key
	 * @return InputQueryCallback
	 */
	public function get(string $key) {
		foreach ($this->inputQueryCallbacks as $inputQueryCallback) {
			if ($inputQueryCallback->getKey() === $key) {
				return $inputQueryCallback;
			}
		}
		$newInputQueryCallback = new InputQueryCallback($key);
		$this->inputQueryCallbacks[] = $newInputQueryCallback;
		return $newInputQueryCallback;
	}

	/**
	 * @param $key
	 * @return bool
	 */
	public function has(string $key) {
		foreach ($this->inputQueryCallbacks as $inputQueryCallback) {
			if ($inputQueryCallback->getKey() === $key) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param $staticContainer
	 */
	public function process($staticContainer) {
		foreach ($this->inputQueryCallbacks as $inputQueryCallback) {
			if ($inputQueryCallback->hasCallbacks()) {
				$callbacks = $inputQueryCallback->getCallbacks();
				$staticContainer->with([$inputQueryCallback->getKey() => function($query) use ($callbacks) {
					foreach ($callbacks as $callback) {
						$callback($query);
					}
				}]);
			} else {
				$staticContainer->with($inputQueryCallback->getKey());
			}
		}
	}

}