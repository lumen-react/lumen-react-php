<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;
use Illuminate\Http\Request;

class ResourceOutputWhenLoaded {

	private $whenLoaded = true;

	/**
	 * ResourceOutputWhenLoaded constructor.
	 * @param callable|null $withCallback
	 * @param bool $whenLoaded
	 */
	public function __construct($whenLoaded = true) {
		$this->whenLoaded = $whenLoaded;
	}

	/**
	 * @param bool $whenLoaded
	 * @param null $withCallback
	 * @return ResourceOutputWhenLoaded
	 */
	public static function make($whenLoaded = true) {
		return new static($whenLoaded);
	}

	/**
	 * @param $key
	 * @param AbstractResource $resource
	 * @param Request $request
	 * @return bool|mixed
	 */
	public function getWhenLoadedKey($key, AbstractResource $resource, Request $request) {
		if ($this->whenLoaded instanceof ResourceCallable) {
			return call_user_func($this->whenLoaded->getCallable(), $resource, $request, $key);
		} else if ($this->whenLoaded === true) {
			return $key;
		} else {
			return $this->whenLoaded;
		}
	}

	/**
	 * @param mixed $whenLoaded
	 * @return ResourceOutputWhenLoaded
	 */
	public function setWhenLoaded($whenLoaded) {
		$this->whenLoaded = $whenLoaded;
		return $this;
	}

}