<?php

namespace RajeevSiewnath\LumenReact\Providers;

use RajeevSiewnath\LumenReact\Http\Middleware\GlobalScopeMiddleware;
use RajeevSiewnath\LumenReact\Library\GlobalScope\GlobalScope;
use Illuminate\Support\ServiceProvider;

class GlobalScopeServiceProvider extends ServiceProvider {

	public function boot() {
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->singleton(GlobalScope::class, function($app) {
			return new GlobalScope();
		});

		$this->app->routeMiddleWare([
			'globalScope' => GlobalScopeMiddleware::class,
		]);
	}
}
