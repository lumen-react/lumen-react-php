<?php

namespace RajeevSiewnath\LumenReact\Http\Middleware;

use Closure;
use RajeevSiewnath\LumenReact\Library\AppSettings\AppSettings;

class GenericRouterEntryMiddleware {

	private $appSettings;

	public function __construct(AppSettings $appSettings) {
		$this->appSettings = $appSettings;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		return $next($request);
	}
}
