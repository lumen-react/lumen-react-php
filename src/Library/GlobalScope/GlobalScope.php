<?php

namespace RajeevSiewnath\LumenReact\Library\GlobalScope;

class GlobalScope {

	private $scopes = [];
	private $enabled = false;

	/**
	 * @return array
	 */
	public function getScopes() {
		return $this->scopes;
	}

	/**
	 * @param array $scopes
	 */
	public function setScopes(array $scopes) {
		$this->scopes = $scopes;
	}

	/**
	 *
	 */
	public function enable() {
		$this->enabled = true;
	}

	/**
	 * @return bool
	 */
	public function enabled() {
		return $this->enabled;
	}

}