<?php

namespace RajeevSiewnath\LumenReact\Http\Middleware\OAuthMiddleware;

use Closure;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use RajeevSiewnath\LumenReact\Exceptions\IllegalOperationException;
use RajeevSiewnath\LumenReact\Library\OAuth\OAuthState;
use RajeevSiewnath\LumenReact\Library\OAuth\Token\OAuthClientToken;
use RajeevSiewnath\LumenReact\Library\OAuth\Token\OAuthToken;
use RajeevSiewnath\LumenReact\Library\OAuth\User\GoogleOAuthUser;
use RajeevSiewnath\LumenReact\Library\OAuth\User\OAuthClientUser;
use RajeevSiewnath\LumenReact\Library\OAuth\User\OAuthUser;

class GoogleOAuthMiddlewareImplementation extends OAuthMiddlewareImplementation {

	protected static $name = "google";

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @param bool $offline
	 * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 * @throws \RajeevSiewnath\LumenReact\Exceptions\IllegalOperationException
	 */
	protected function doLogin(Request $request, Closure $next, bool $offline) {
		$GOOGLE_OAUTH_CLIENT_ID = env("GOOGLE_OAUTH_CLIENT_ID");
		$GOOGLE_OAUTH_CLIENT_SECRET = env("GOOGLE_OAUTH_CLIENT_SECRET");
		$GOOGLE_OAUTH_SCOPES = env("GOOGLE_OAUTH_SCOPES");
		$url = static::resolveRedirectUrl($request);
		if ($request->has("redirect") && $request->has('provider')) {
			$state = new OAuthState($request->get('redirect'), static::$name, $offline);
			$queryParams = [];
			$queryParams['state'] = $state->encrypt();
			$queryParams['scope'] = $GOOGLE_OAUTH_SCOPES;
			$queryParams['redirect_uri'] = $url;
			$queryParams['response_type'] = 'code';
			$queryParams['client_id'] = $GOOGLE_OAUTH_CLIENT_ID;
			$queryParams['prompt'] = 'consent';
			$queryParams['include_granted_scopes'] = 'true';
			if ($offline) {
				$queryParams['access_type'] = 'offline';
			}
			$queryParamsParts = [];
			foreach ($queryParams as $key => $value) {
				$queryParamsParts[] = "{$key}={$value}";
			}
			$qs = implode('&', $queryParamsParts);
			return redirect("https://accounts.google.com/o/oauth2/v2/auth?{$qs}");
		} else if ($request->has("code") && $request->has("scope") && $request->has("state")) {
			$client = new Client();
			$response = $client->request("POST", "https://www.googleapis.com/oauth2/v4/token", [
				"form_params" => [
					"code"          => $request->get("code"),
					"client_id"     => $GOOGLE_OAUTH_CLIENT_ID,
					"client_secret" => $GOOGLE_OAUTH_CLIENT_SECRET,
					"redirect_uri"  => $url,
					"grant_type"    => "authorization_code",
				],
			]);
			$response = json_decode((string)$response->getBody(), true);
			if (isset($response['refresh_token'])) {
				$request->merge(['oauth_extra' => $response['refresh_token']]);
			}
			$request->merge($response);
			return $next($request);
		}
		return null;
	}

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	protected function doLogout(Request $request, Closure $next) {
		$client = new Client();
		$token = OAuthToken::fromEncrypted(static::getAccessTokenFromRequest($request));
		return $client->request("POST", "https://accounts.google.com/o/oauth2/revoke", [
			"http_errors" => false,
			"form_params" => [
				"token" => $token->getAccessToken(),
			],
		]);
	}

	/**
	 * @param OAuthToken $accessToken
	 * @param bool $response
	 * @param bool $newAccessToken
	 * @return mixed|null
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function getOAuthUserDataFromAccessToken(OAuthToken $accessToken, &$response = false, &$newAccessToken = false) {
		$client = new Client();
		$r = $client->request("GET", "https://www.googleapis.com/oauth2/v2/userinfo", [
			"http_errors" => false,
			"headers"     => [
				"Authorization" => "Bearer {$accessToken->getAccessToken()}",
			],
		]);
		if ($r->getStatusCode() < 400) {
			if ($response !== false) {
				$response = $r;
			}
			return json_decode((string)$r->getBody(), true);
		} else {
			if ($newAccessToken !== false) {
				if ($accessToken->getOffline()) {
					$GOOGLE_OAUTH_CLIENT_ID = env("GOOGLE_OAUTH_CLIENT_ID");
					$GOOGLE_OAUTH_CLIENT_SECRET = env("GOOGLE_OAUTH_CLIENT_SECRET");
					$client = new Client();
					$r2 = $client->request("POST", "https://www.googleapis.com/oauth2/v4/token", [
						"http_errors" => false,
						"form_params" => [
							"client_id"     => $GOOGLE_OAUTH_CLIENT_ID,
							"client_secret" => $GOOGLE_OAUTH_CLIENT_SECRET,
							"refresh_token" => $accessToken->getExtra(),
							"grant_type"    => "refresh_token",
						],
					]);
					if ($r2->getStatusCode() < 400) {
						$r2 = json_decode((string)$r2->getBody(), true);
						$newAccessToken = $r2['access_token'];
						$client = new Client();
						$r3 = $client->request("GET", "https://www.googleapis.com/oauth2/v2/userinfo", [
							"http_errors" => false,
							"headers"     => [
								"Authorization" => "Bearer {$newAccessToken}",
							],
						]);
						if ($r3->getStatusCode() < 400) {
							if ($response !== false) {
								$response = $r3;
							}
							return json_decode((string)$r3->getBody(), true);
						}
					}
				}
			}
			return null;
		}
	}

	/**
	 * @param $data
	 * @param $accessToken
	 * @return OAuthUser
	 */
	public static function generateOAuthUser($data, $accessToken): OAuthUser {
		return new GoogleOAuthUser($data, $accessToken, static::$name);
	}

	/**
	 * @return string
	 * @throws IllegalOperationException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function createClientCredentialsToken(): string {
		$clientCredentialsJwt = static::createClientCredentialsJwt();
		$client = new Client();
		$r = $client->request("POST", "https://www.googleapis.com/oauth2/v4/token", [
			"http_errors" => false,
			"form_params" => [
				"grant_type" => "urn:ietf:params:oauth:grant-type:jwt-bearer",
				"assertion"  => $clientCredentialsJwt,
			],
		]);
		if ($r->getStatusCode() < 400) {
			$r = json_decode((string)$r->getBody(), true);
			$oAuthClientToken = new OAuthClientToken($r['access_token'], self::$name);
			return $oAuthClientToken->encrypt();
		} else {
			throw new IllegalOperationException("Cannot create google client credentials");
		}
	}

	/**
	 * @param bool $allowCheckingOfUsers
	 * @return string
	 */
	private static function createClientCredentialsJwt($allowCheckingOfUsers = false): string {
		$GOOGLE_OAUTH_CLIENT_CREDENTIAL_JSON = env("GOOGLE_OAUTH_CLIENT_CREDENTIAL_JSON");
		$GOOGLE_OAUTH_CLIENT_CREDENTIAL_ISSUER = env("GOOGLE_OAUTH_CLIENT_CREDENTIAL_ISSUER");
		$OAUTH_CLIENT_CREDENTIALS_TTL = env("OAUTH_CLIENT_CREDENTIALS_TTL");

		$header = base64url_encode(json_encode(["alg" => "RS256", "typ" => "JWT"]));
		$claimSet = base64url_encode(json_encode([
			"iss"   => $GOOGLE_OAUTH_CLIENT_CREDENTIAL_ISSUER,
			"scope" => "https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile" . ($allowCheckingOfUsers ? " https://www.googleapis.com/auth/cloud-platform" : ""),
			"aud"   => "https://www.googleapis.com/oauth2/v4/token",
			"exp"   => time() + $OAUTH_CLIENT_CREDENTIALS_TTL,
			"iat"   => time(),
		]));
		$token = "$header.$claimSet";
		$keyFile = json_decode(file_get_contents(rtrim(base_path(), '/') . '/' . $GOOGLE_OAUTH_CLIENT_CREDENTIAL_JSON), true);
		$signature = null;
		openssl_sign(
			$token,
			$signature,
			$keyFile['private_key'],
			OPENSSL_ALGO_SHA256
		);
		$signature = base64url_encode($signature);
		return "$token.$signature";
	}

	/**
	 * @param $encryptedToken
	 * @return OAuthClientUser
	 * @throws IllegalOperationException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function getOAuthClientUserFromEncryptedToken($encryptedToken) {
		$clientCredentialsJwt = static::createClientCredentialsJwt(true);
		$client = new Client();
		$r = $client->request("POST", "https://www.googleapis.com/oauth2/v4/token", [
			"http_errors" => false,
			"form_params" => [
				"grant_type" => "urn:ietf:params:oauth:grant-type:jwt-bearer",
				"assertion"  => $clientCredentialsJwt,
			],
		]);
		if ($r->getStatusCode() < 400) {
			$r = json_decode((string)$r->getBody(), true);
			$ownerAccessToken = $r['access_token'];
		} else {
			throw new IllegalOperationException("Cannot create google owner client credentials");
		}

		$issuerAccessToken = OAuthClientToken::fromEncrypted($encryptedToken);

		if (!$ownerAccessToken || !$issuerAccessToken) {
			throw new IllegalOperationException("Something went wrong during google oAuth client credential authentication process");
		}

		// Retrieve a list of service accounts for the owner account
		$GOOGLE_OAUTH_CLIENT_CREDENTIAL_JSON = env("GOOGLE_OAUTH_CLIENT_CREDENTIAL_JSON");
		$keyFile = json_decode(file_get_contents(rtrim(base_path(), '/') . '/' . $GOOGLE_OAUTH_CLIENT_CREDENTIAL_JSON), true);

		$client = new Client();
		$r = $client->request("GET", "https://iam.googleapis.com/v1/projects/{$keyFile['project_id']}/serviceAccounts", [
			"http_errors" => false,
			"headers"     => [
				"Authorization" => "Bearer {$ownerAccessToken}",
			],
		]);
		if ($r->getStatusCode() < 400) {
			$client = new Client();
			$r2 = $client->request("GET", "https://www.googleapis.com/oauth2/v2/userinfo", [
				"http_errors" => false,
				"headers"     => [
					"Authorization" => "Bearer {$issuerAccessToken->getAccessToken()}",
				],
			]);
			if ($r2->getStatusCode() < 400) {
				$r = json_decode((string)$r->getBody(), true);
				$r2 = json_decode((string)$r2->getBody(), true);
				$legitUser = false;
				foreach ($r["accounts"] as $account) {
					if ($account["uniqueId"] === $r2["id"]) {
						$legitUser = true;
						break;
					}
				}
				if ($legitUser) {
					return new OAuthClientUser($r2["id"], $encryptedToken, self::$name);
				}
			} else {
				throw new IllegalOperationException("Cannot fetch google service account issuer");
			}
		} else {
			throw new IllegalOperationException("Cannot fetch google service accounts");
		}
		return null;
	}
}
