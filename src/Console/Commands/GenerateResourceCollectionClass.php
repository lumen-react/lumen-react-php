<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

class GenerateResourceCollectionClass extends AbstractCommand {

	protected $signature = 'rs:lr:resource:collection {--o|overwrite : Overwrite if the file already exists} {resourceName : Where typescript goes}';
	protected $description = 'Generate resource collection';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$resourceName = $this->argument('resourceName');
		$overwrite = $this->option('overwrite');

		$appPath = app()->path();
		$className = ucfirst($resourceName);
		$resource = "${className}Resource";
		$collection = "${className}Collection";
		$filename = "${appPath}/Http/Resources/{$collection}.php";

		if (file_exists($filename)) {
			if ($overwrite) {
				$this->warn("${filename} already exists, but overwrite is enabled");
			} else {
				$this->warn("${filename} already exists, exiting");
				return 0;
			}
		}

		$template = $this->getTemplate();
		$template = preg_replace('/\[RESOURCE\]/', $resource, $template);
		$template = preg_replace('/\[COLLECTION\]/', $collection, $template);

		file_put_contents($filename, $template);

		$this->info( "created $filename for class $className");
		return 0;
	}

	private function getTemplate() {
		return <<< STRING
<?php

namespace App\Http\Resources;

class [COLLECTION] extends \RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection {

	public static \$resourceClass = [RESOURCE]::class;

}
STRING;

	}

}