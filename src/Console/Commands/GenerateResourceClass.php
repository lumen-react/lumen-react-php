<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

class GenerateResourceClass extends AbstractCommand {

	protected $signature = 'rs:lr:resource:resource {--o|overwrite : Overwrite if the file already exists} {resourceName : Where typescript goes} {table? : Table name (defaults to resource name)}';
	protected $description = 'Generate resource template';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$resourceName = $this->argument('resourceName');
		$overwrite = $this->option('overwrite');
		if ($this->hasOption("table")) {
			$table = $this->argument("table");
		} else {
			$table = $table = snake_case($resourceName);
		}

		$appPath = app()->path();
		$className = ucfirst($resourceName);
		$resource = "${className}Resource";
		$collection = "${className}Collection";
		$filename = "${appPath}/Http/Resources/{$resource}.php";

		if (file_exists($filename)) {
			if ($overwrite) {
				$this->warn("${filename} already exists, but overwrite is enabled");
			} else {
				$this->warn("${filename} already exists, exiting");
				return 0;
			}
		}

		$template = $this->getTemplate();
		$template = preg_replace('/\[MODEL\]/', $className, $template);
		$template = preg_replace('/\[RESOURCE\]/', $resource, $template);
		$template = preg_replace('/\[COLLECTION\]/', $collection, $template);
		$template = preg_replace('/\[TABLE\]/', $table, $template);

		file_put_contents($filename, $template);

		$this->info( "created $filename for class $className");
		return 0;
	}

	private function getTemplate() {
		return <<< STRING
<?php

namespace App\Http\Resources;

use App\[MODEL];
use RajeevSiewnath\LumenReact\Library\ResourceBinding\ResourceBinding;
use RajeevSiewnath\LumenReact\Library\ResourceBinding\ResourceOutput;

class [RESOURCE] extends \RajeevSiewnath\LumenReact\Http\Resources\AbstractResource {

	public static \$modelClass = [MODEL]::class;
	public static \$collectionClass = [COLLECTION]::class;

	protected static function createBindings() {
		return ResourceBinding::make(
			ResourceOutput::make('id', '[TABLE]_id')
			// Inputs
		);
	}

}
STRING;

	}

}