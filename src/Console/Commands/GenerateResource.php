<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

class GenerateResource extends AbstractCommand {

	protected $signature = 'rs:lr:resource {--o|overwrite : Overwrite if the file already exists} {resourceName : Where typescript goes} {table? : Table name (defaults to resource name)}';
	protected $description = 'Generate resource classes';

	public function handle() {
		$resourceName = $this->argument('resourceName');
		$overwrite = $this->option('overwrite');
		if ($this->hasOption("table")) {
			$table = $this->argument("table");
		} else {
			$table = $table = snake_case($resourceName);
		}

		$this->call('rs:lr:resource:resource', [
			'resourceName' => $resourceName,
			'table' => $table,
			'--overwrite' => $overwrite,
		]);

		$this->call('rs:lr:resource:collection', [
			'resourceName' => $resourceName,
			'--overwrite' => $overwrite,
		]);

		$this->call('rs:lr:resource:model', [
			'resourceName' => $resourceName,
			'table' => $table,
			'--overwrite' => $overwrite,
		]);

		$this->call('rs:lr:resource:controller', [
			'resourceName' => $resourceName,
			'--overwrite' => $overwrite,
		]);

	}

}