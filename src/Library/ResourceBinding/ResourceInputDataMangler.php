<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use Illuminate\Http\Request;

class ResourceInputDataMangler {

	private $dataManglerValue;

	/**
	 * ResourceInputPopulator constructor.
	 * @param $dataManglerValue
	 */
	public function __construct($dataManglerValue) {
		$this->dataManglerValue = $dataManglerValue;
	}

	/**
	 * @param $dataManglerValue
	 * @return ResourceInputDataMangler
	 */
	public static function make($dataManglerValue) {
		return new static($dataManglerValue);
	}

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function getDataManglerValue(Request $request) {
		if ($this->dataManglerValue instanceof ResourceCallable) {
			return call_user_func($this->dataManglerValue->getCallable(), $request);
		} else {
			return $this->dataManglerValue;
		}
	}

	/**
	 * @param mixed $dataManglerValue
	 * @return ResourceInputDataMangler
	 */
	public function setDataManglerValue($dataManglerValue) {
		$this->dataManglerValue = $dataManglerValue;
		return $this;
	}

}