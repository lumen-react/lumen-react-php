<?php

namespace RajeevSiewnath\LumenReact\Http\Middleware\OAuthMiddleware;

use Closure;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use RajeevSiewnath\LumenReact\Exceptions\IllegalOperationException;
use RajeevSiewnath\LumenReact\Library\OAuth\OAuthState;
use RajeevSiewnath\LumenReact\Library\OAuth\Token\OAuthClientToken;
use RajeevSiewnath\LumenReact\Library\OAuth\Token\OAuthToken;
use RajeevSiewnath\LumenReact\Library\OAuth\User\FacebookOAuthUser;
use RajeevSiewnath\LumenReact\Library\OAuth\User\OAuthClientUser;
use RajeevSiewnath\LumenReact\Library\OAuth\User\OAuthUser;

class FacebookOAuthMiddlewareImplementation extends OAuthMiddlewareImplementation {

	protected static $name = "facebook";

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @param bool $offline
	 * @return \Illuminate\Http\RedirectResponse|\Laravel\Lumen\Http\Redirector
	 * @throws IllegalOperationException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	protected function doLogin(Request $request, Closure $next, bool $offline) {
		$FACEBOOK_OAUTH_CLIENT_ID = env("FACEBOOK_OAUTH_CLIENT_ID");
		$FACEBOOK_OAUTH_CLIENT_SECRET = env("FACEBOOK_OAUTH_CLIENT_SECRET");
		$FACEBOOK_OAUTH_SCOPES = env("FACEBOOK_OAUTH_SCOPES");
		$url = static::resolveRedirectUrl($request);
		if ($request->has("redirect") && $request->has('provider')) {
			$state = new OAuthState($request->get('redirect'), static::$name, $offline);
			$queryParams = [];
			$queryParams['state'] = $state->encrypt();
			$queryParams['scope'] = $FACEBOOK_OAUTH_SCOPES;
			$queryParams['redirect_uri'] = $url;
			$queryParams['response_type'] = 'code';
			$queryParams['client_id'] = $FACEBOOK_OAUTH_CLIENT_ID;
			$queryParamsParts = [];
			foreach ($queryParams as $key => $value) {
				$queryParamsParts[] = "{$key}={$value}";
			}
			$qs = implode('&', $queryParamsParts);
			return redirect("https://www.facebook.com/v3.2/dialog/oauth?$qs");
		} else if ($request->has("code") && $request->has("state")) {
			$client = new Client();
			$response = $client->request("POST", "https://graph.facebook.com/v3.2/oauth/access_token", [
				"form_params" => [
					"code"          => $request->get("code"),
					"client_id"     => $FACEBOOK_OAUTH_CLIENT_ID,
					"client_secret" => $FACEBOOK_OAUTH_CLIENT_SECRET,
					"redirect_uri"  => $url,
				],
			]);
			$response = json_decode((string)$response->getBody(), true);
			$state = OAuthState::fromEncrypted($request->get('state'));
			if ($state->getOffline()) {
				$client = new Client();
				$response = $client->request("POST", "https://graph.facebook.com/v3.2/oauth/access_token", [
					"form_params" => [
						"grant_type"        => 'fb_exchange_token',
						"client_id"         => $FACEBOOK_OAUTH_CLIENT_ID,
						"client_secret"     => $FACEBOOK_OAUTH_CLIENT_SECRET,
						"fb_exchange_token" => $response['access_token'],
					],
				]);
				$response = json_decode((string)$response->getBody(), true);
			}

			$request->merge($response);
			return $next($request);
		}
		return null;
	}

	/**
	 * @param Request $request
	 * @param Closure $next
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	protected function doLogout(Request $request, Closure $next) {
		$token = OAuthToken::fromEncrypted(static::getAccessTokenFromRequest($request));
		$FACEBOOK_OAUTH_CLIENT_SECRET = env("FACEBOOK_OAUTH_CLIENT_SECRET");
		$appSecretProof = hash_hmac('sha256', $token->getAccessToken(), $FACEBOOK_OAUTH_CLIENT_SECRET);
		$client = new Client();
		return $client->request("DELETE", "https://graph.facebook.com/v3.2/me/permissions", [
			"http_errors" => false,
			"query"       => [
				"access_token"    => $token->getAccessToken(),
				"appsecret_proof" => $appSecretProof,
			],
		]);
	}

	/**
	 * @param OAuthToken $accessToken
	 * @param bool $response
	 * @param bool $newAccessToken
	 * @return mixed|null
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function getOAuthUserDataFromAccessToken(OAuthToken $accessToken, &$response = false, &$newAccessToken = false) {
		$FACEBOOK_OAUTH_CLIENT_SECRET = env("FACEBOOK_OAUTH_CLIENT_SECRET");
		$FACEBOOK_OAUTH_CLIENT_FIELDS = env("FACEBOOK_OAUTH_CLIENT_FIELDS", '');
		$fields = trim("id,first_name,last_name,middle_name,name,name_format,picture,short_name,email,$FACEBOOK_OAUTH_CLIENT_FIELDS", ',');
		$appSecretProof = hash_hmac('sha256', $accessToken->getAccessToken(), $FACEBOOK_OAUTH_CLIENT_SECRET);
		$client = new Client();
		$response = $client->request("GET", "https://graph.facebook.com/v3.2/me", [
			"http_errors" => false,
			"query"       => [
				"access_token"    => $accessToken->getAccessToken(),
				"appsecret_proof" => $appSecretProof,
				"fields"          => $fields,
			],
		]);
		if ($response->getStatusCode() < 400) {
			return json_decode((string)$response->getBody(), true);
		} else {
			return null;
		}
	}

	/**
	 * @param $data
	 * @param $accessToken
	 * @return OAuthUser
	 */
	public static function generateOAuthUser($data, $accessToken): OAuthUser {
		return new FacebookOAuthUser($data, $accessToken, static::$name);
	}

	/**
	 * @return string
	 * @throws IllegalOperationException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 * @throws \Exception
	 */
	public static function createClientCredentialsToken(): string {
		$FACEBOOK_OAUTH_CLIENT_ID = env("FACEBOOK_OAUTH_CLIENT_ID");
		$FACEBOOK_OAUTH_CLIENT_SECRET = env("FACEBOOK_OAUTH_CLIENT_SECRET");
		$OAUTH_CLIENT_CREDENTIALS_TTL = env("OAUTH_CLIENT_CREDENTIALS_TTL");

		$client = new Client();
		$r = $client->request("GET", "https://graph.facebook.com/oauth/access_token", [
			"http_errors" => false,
			"query"       => [
				"client_id"     => $FACEBOOK_OAUTH_CLIENT_ID,
				"client_secret" => $FACEBOOK_OAUTH_CLIENT_SECRET,
				"grant_type"    => "client_credentials",
			],
		]);
		if ($r->getStatusCode() < 400) {
			$r = json_decode((string)$r->getBody(), true);
			$accessToken = [
				'access_token' => $r['access_token'],
				'time'         => time(),
				'ttl'          => time() + $OAUTH_CLIENT_CREDENTIALS_TTL,
				'hash'         => str_random(256),
			];
			$oAuthClientToken = new OAuthClientToken(Crypt::encrypt(json_encode($accessToken)), self::$name);
			return $oAuthClientToken->encrypt();
		} else {
			throw new IllegalOperationException("Cannot create facebook client credentials");
		}
	}

	/**
	 * @param $encryptedToken
	 * @return OAuthClientUser
	 * @throws IllegalOperationException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public static function getOAuthClientUserFromEncryptedToken($encryptedToken) {
		$FACEBOOK_OAUTH_CLIENT_ID = env("FACEBOOK_OAUTH_CLIENT_ID");
		$issuerAccessToken = OAuthClientToken::fromEncrypted($encryptedToken);
		$issuer = json_decode(Crypt::decrypt($issuerAccessToken->getAccessToken()), true);
		if ($issuer) {
			if ($issuer['time'] > time() || $issuer['ttl'] < time()) {
				throw new IllegalOperationException("The facebook client credentials token has expired");
			}
		} else {
			throw new IllegalOperationException("Could not deconstruct facebook issuer");
		}

		$client = new Client();
		$r = $client->request("GET", "https://graph.facebook.com/v3.2/$FACEBOOK_OAUTH_CLIENT_ID", [
			"http_errors" => false,
			"query"       => [
				"access_token" => $issuer['access_token'],
			],
		]);
		if ($r->getStatusCode() < 400) {
			$r = json_decode((string)$r->getBody(), true);
			if ($r["id"] === $FACEBOOK_OAUTH_CLIENT_ID) {
				return new OAuthClientUser($r["id"], $encryptedToken, self::$name);
			}
		} else {
			throw new IllegalOperationException("Cannot authenticate facebook client credentials");
		}
		return null;
	}
}
