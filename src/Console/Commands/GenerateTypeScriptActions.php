<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

use RajeevSiewnath\LumenReact\Http\Controllers\AbstractController;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;

class GenerateTypeScriptActions extends AbstractEndpointReader {

	protected $signature = 'rs:lr:ts:actions {outputPath? : Where actions goes}';
	protected $description = 'Generate redux reducers';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		if ($this->hasOption("outputPath")) {
			$out = trim($this->argument("outputPath", "/\\"));
		} else {
			$out = config('custom.typeScriptDefOutputFolder');
		}

		if (trim($out) == "") {
			$this->error("no output path defined");

			return 1;
		}

		$output = $this->getFile($this->getRouteCollection());

		$dest = rtrim($out, "/\\") . "/actions.ts";
		file_put_contents($dest, $output);
		$this->info(PHP_EOL . "wrote actions to $dest");

		return 0;
	}

	private function fromCamelCase($input) {
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
		$ret = $matches[0];
		foreach ($ret as &$match) {
			$match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
		}
		return implode('_', $ret);
	}

	private function getFile($routeCollection) {
		$requests = [];
		foreach ($routeCollection as $row) {
			if (!$row['namedRoute'] || !in_array('typeScriptActionable', $row['middlewareArray'])) {
				continue;
			}

			if (is_subclass_of($row['controller'], AbstractController::class)) {
				$row['args'] = $this->generateJsArgumentMetadata($row['args'], $row['controller'], $row['action']);
				$requests[] = [
					'name'          => $row['namedRoute'],
					'jsArgs'        => $this->getJsArguments($row['args'], $row['controller'], $row['action'], false),
					'jsArgsNoTypes' => $this->getJsArguments($row['args'], $row['controller'], $row['action'], true),
					'return'        => $this->getOutputType($row['controller'], $row['action']),
					'hasInput'      => $this->hasInput($row['controller'], $row['action']),
				];
			}
		}

		$imports = [];
		$actionTypes = [];
		$actionCallbackOptions = [];
		$actionCallback = [];
		$actionInterface = [];
		$actionDispatchBinders = [];
		$actionDispatcher = [];
		$actionCreator = [];
		$bar = $this->output->createProgressBar(sizeof($requests));

		foreach ($requests as $row) {
			$NamedRoute = ucfirst($row['name']);
			$imports[] = "${row['name']}Request";
			$actionType = strtoupper($this->fromCamelCase($row['name']));
			$actionTypes[] = "\t$actionType = '$actionType',";
			$actionCallbackOptions[] = "export type {$NamedRoute}ActionCallbackOptions = GenericCallbackOptions<ActionType, {$row['return']}>;";
			$jsArgs = implode(', ', array_merge($row['jsArgs'], ["options?: {$NamedRoute}ActionCallbackOptions | Dispatch<{$NamedRoute}Action>"]));
			$actionCallback[] = "export type {$NamedRoute}ActionCallback = ({$jsArgs}) => {$NamedRoute}Action;";

			$actionInterface[] = <<<TEXT
export interface {$NamedRoute}Action extends BaseAction<{$row['return']}> {
    type: ActionType;
}
TEXT;

			$actionDispatchBinders[] = <<<TEXT
export function {$row['name']}DispatchBinder(dispatch: Dispatch<{$NamedRoute}Action>) {
	return {{$row['name']}Dispatcher: {$row['name']}(dispatch)};
}
TEXT;

			$actionDispatcher[] = <<<TEXT
export interface ${NamedRoute}Dispatcher {
    {$row['name']}Dispatcher: ${NamedRoute}ActionCallback;
}
TEXT;
			// (dispatch: Dispatch<Promise<{$row['return']}>>): {$NamedRoute}ActionCallback =>
			// does not work
			$jsArgs = implode(', ', array_merge($row['jsArgs'], ["options?: {$NamedRoute}ActionCallbackOptions"]));
			$jsArgsNoTypesWithRequestConfig = implode(', ', array_merge($row['jsArgsNoTypes'], ["options.requestConfig"]));
			$jsArgsNoTypes = implode(', ', $row['jsArgsNoTypes']);
			$actionCreator[] = <<<TEXT
export const {$row['name']}: ActionCreator<{$NamedRoute}ActionCallback> =
    (dispatch: Dispatch<Promise<{$row['return']}>>): {$NamedRoute}ActionCallback =>
        ($jsArgs) =>
            callDispatcher<{$row['return']}, {$NamedRoute}ActionCallbackOptions, ActionType>(
            	dispatch, 
            	ActionType.{$actionType}, 
            	(options && options.requestConfig) ? 
					${row['name']}Request({$jsArgsNoTypesWithRequestConfig}) : 
					${row['name']}Request({$jsArgsNoTypes}), 
            	options);
TEXT;
			$bar->advance();
		}

		$types = $this->getTypes();
		$output = <<<TEXT
import {ActionCreator, Dispatch} from "redux";
import {BaseAction, callDispatcher, GenericCallbackOptions} from 'lumen-react-javascript';

TEXT;
		if ($types) {
			$types = implode(', ', $types);
			$output .= <<<TEXT
import { {$types} } from './types';

TEXT;
		}

		if ($imports) {
			$imports = implode(', ', $imports);
			$output .= <<<TEXT
import { {$imports} } from './endpoints';

TEXT;
		}

		$output .= PHP_EOL;

		$actionTypes = implode(PHP_EOL, $actionTypes);
		$actionCallbackOptions = implode(PHP_EOL, $actionCallbackOptions);
		$actionCallback = implode(PHP_EOL, $actionCallback);
		$actionInterface = implode(PHP_EOL . PHP_EOL, $actionInterface);
		$actionDispatchBinders = implode(PHP_EOL . PHP_EOL, $actionDispatchBinders);
		$actionDispatcher = implode(PHP_EOL . PHP_EOL, $actionDispatcher);
		$actionCreator = implode(PHP_EOL . PHP_EOL, $actionCreator);

		$output .= <<<TEXT
export enum ActionType {
$actionTypes
}

TEXT;

		$output .= PHP_EOL;
		$output .= $actionCallbackOptions;
		$output .= PHP_EOL;
		$output .= PHP_EOL;
		$output .= $actionCallback;
		$output .= PHP_EOL;
		$output .= PHP_EOL;
		$output .= $actionInterface;
		$output .= PHP_EOL;
		$output .= PHP_EOL;
		$output .= $actionDispatchBinders;
		$output .= PHP_EOL;
		$output .= PHP_EOL;
		$output .= $actionDispatcher;
		$output .= PHP_EOL;
		$output .= PHP_EOL;
		$output .= $actionCreator;

		return $output;
	}

	protected function getJsArguments($args, $controller, $action, $noTypes) {
		$jsArgs = [];
		$resource = $controller::input($action);
		if ($resource) {
			$refResource = new \ReflectionClass($resource);
			$resourceName = null;
			$importName = null;
			if (is_subclass_of($resource, AbstractResource::class)) {
				$importName = $resourceName = str_replace("Resource", "", $refResource->getShortName());
			} else if (is_subclass_of($resource, AbstractCollection::class)) {
				$importName = str_replace("Collection", "", $refResource->getShortName());
				$resourceName = "Array<$importName>";
			} else {
				$importName = $resourceName = $refResource->getShortName();
			}

			if (!!$noTypes) {
				$jsArgs[] = "input";
			} else {
				$jsArgs[] = "input: $resourceName";
			}
		}

		foreach ($args as $arg) {
			if ($arg['argument'] != null) {
				if (!!$noTypes) {
					$jsArgs[] = $arg['argument'];
				} else {
					$jsArgs[] = "{$arg['argument']}: {$arg['type']}";
				}
			}
		}

		return $jsArgs;
	}

}