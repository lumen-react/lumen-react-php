<?php

namespace RajeevSiewnath\LumenReact\Providers;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use RajeevSiewnath\LumenReact\Console\Commands\CreateTestStubs;
use RajeevSiewnath\LumenReact\Console\Commands\DownloadGeoIPDatabase;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateControllerClass;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateModelClass;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateResource;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateResourceClass;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateResourceCollectionClass;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateTypeScript;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateTypeScriptActions;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateTypeScriptDefinitions;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateTypeScriptEndpointStore;
use RajeevSiewnath\LumenReact\Console\Commands\GenerateTypeScriptReducerStore;
use RajeevSiewnath\LumenReact\Console\Commands\GetVersion;
use RajeevSiewnath\LumenReact\Console\Commands\OAuthClientCredentials;
use RajeevSiewnath\LumenReact\Console\Commands\OAuthTokenInfo;
use RajeevSiewnath\LumenReact\Console\Commands\OAuthVerify;
use RajeevSiewnath\LumenReact\Console\Commands\RoutesList;
use RajeevSiewnath\LumenReact\Library\AppSettings\AppSettings;
use RajeevSiewnath\LumenReact\Library\ResponseScope\ResponseScope;

class LumenReactServiceProvider extends ServiceProvider {

	public function boot() {
		Resource::withoutWrapping();

		Carbon::serializeUsing(function($carbon) {
			return $carbon->toIso8601String();
		});

		Validator::extend('notPresent', function($attribute, $value, $parameters, $validator) {
			return !$validator->validatePresent($attribute, $value);
		});

		if ($this->app->environment('local')) {
			DB::listen(function(QueryExecuted $query) {
				foreach ($query->bindings as $i => $binding) {
					if ($binding instanceof \DateTime) {
						$query->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
					} else {
						if (is_string($binding)) {
							$query->bindings[$i] = "'$binding'";
						}
					}
				}
				// Insert bindings into query
				$sql = str_replace(['%', '?'], ['%%', '%s'], $query->sql);
				$sql = vsprintf($sql, $query->bindings);
				Log::debug("Query {$sql} took {$query->time}");
			});
		}

		if ($this->app->runningInConsole()) {
			$this->commands([
				// TypeScript generators
				GenerateTypeScriptDefinitions::class,
				GenerateTypeScriptEndpointStore::class,
				GenerateTypeScriptActions::class,
				GenerateTypeScriptReducerStore::class,
				GenerateTypeScript::class,
				// Resource generators
				GenerateModelClass::class,
				GenerateControllerClass::class,
				GenerateResourceClass::class,
				GenerateResourceCollectionClass::class,
				GenerateResource::class,
				// OAuth
				OAuthClientCredentials::class,
				OAuthVerify::class,
				OAuthTokenInfo::class,
				// Other
				RoutesList::class,
				CreateTestStubs::class,
				DownloadGeoIPDatabase::class,
				GetVersion::class,
			]);
		}
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->singleton(AppSettings::class, function($app) {
			return new AppSettings();
		});
		$this->app->singleton(ResponseScope::class, function($app) {
			return new ResponseScope();
		});

		$this->app->register(DatabaseTransactionServiceProvider::class);
		$this->app->register(FileUploadServiceProvider::class);
		$this->app->register(LumenReactServiceProvider::class);
		$this->app->register(GlobalScopeServiceProvider::class);
		$this->app->register(TypeScriptServiceProvider::class);
		$this->app->register(GeoIPServiceProvider::class);
		$this->app->register(GenericRouterEntryProvider::class);
		$this->app->register(OAuthServiceProvider::class);
	}
}
