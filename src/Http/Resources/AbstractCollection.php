<?php

namespace RajeevSiewnath\LumenReact\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AbstractCollection extends ResourceCollection {

	public static $resourceClass;

	/**
	 * @return mixed
	 * @throws \ReflectionException
	 */
	public static function getType() {
		$reflectionClass = new \ReflectionClass(get_called_class());
		$class = str_replace("Collection", "", $reflectionClass->getShortName());
		return "Array<$class>";
	}

	/**
	 * @return AbstractResource
	 */
	public static function resourceClass() {
		return static::$resourceClass;
	}

	/**
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
	 */
	public function toArray($request) {
		return static::resourceClass()::collection($this->collection);
	}

}