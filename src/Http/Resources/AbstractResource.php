<?php

namespace RajeevSiewnath\LumenReact\Http\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;
use RajeevSiewnath\LumenReact\AbstractModel;
use RajeevSiewnath\LumenReact\Console\Commands\RoutesList;
use RajeevSiewnath\LumenReact\Exceptions\IllegalOperationException;
use RajeevSiewnath\LumenReact\Library\AppSettings\AppSettings;
use RajeevSiewnath\LumenReact\Library\GlobalScope\GlobalScope;
use RajeevSiewnath\LumenReact\Library\ResourceBinding\InputQueryCallbackContainer;
use RajeevSiewnath\LumenReact\Library\ResourceBinding\ResourceBinding;
use RajeevSiewnath\LumenReact\Library\ResponseScope\ResponseScope;

abstract class AbstractResource extends Resource {

	public static $modelClass;
	public static $collectionClass;
	protected static $cachedBindings;

	/**
	 * @return mixed
	 * @throws \ReflectionException
	 */
	public static function getType() {
		$reflectionClass = new \ReflectionClass(get_called_class());
		return str_replace("Resource", "", $reflectionClass->getShortName());
	}

	/**
	 * @return mixed
	 */
	public static function getTableName() {
		return with(new static::$modelClass)->getTable();
	}

	/**
	 * @return AbstractModel
	 */
	public static function modelClass() {
		return static::$modelClass;
	}

	/**
	 * @return AbstractCollection
	 */
	public static function collectionClass() {
		return static::$collectionClass;
	}

	/**
	 * @param Collection $collection
	 * @return AbstractCollection
	 */
	public static function toCollection(Collection $collection) {
		return new static::$collectionClass($collection);
	}

	/**
	 * @return ResourceBinding
	 */
	protected static abstract function createBindings();

	/**
	 * @return ResourceBinding
	 */
	public static function bindings() {
		if (empty(static::$cachedBindings[get_called_class()])) {
			static::$cachedBindings[get_called_class()] = static::createBindings();
			static::$cachedBindings[get_called_class()]->generateAutoAttributes(get_called_class());
		}
		return static::$cachedBindings[get_called_class()];
	}

	/**
	 * Retrieve a relationship if it has been loaded.
	 *
	 * @param  string $relationship
	 * @return \Illuminate\Http\Resources\MissingValue|mixed
	 */
	public function whenLoadedProxy($relationship) {
		return $this->whenLoaded($relationship);
	}

	/**
	 * Retrieve a relationship if it has been loaded.
	 *
	 * @param  string $relationship
	 * @param  mixed $value
	 * @return \Illuminate\Http\Resources\MissingValue|mixed
	 */
	public function whenLoadedProxy2($relationship, $value = null) {
		return $this->whenLoaded($relationship, $value);
	}

	/**
	 * Retrieve a relationship if it has been loaded.
	 *
	 * @param  string $relationship
	 * @param  mixed $value
	 * @param  mixed $default
	 * @return \Illuminate\Http\Resources\MissingValue|mixed
	 */
	public function whenLoadedProxy3($relationship, $value = null, $default = null) {
		return $this->whenLoaded($relationship, $value, $default);
	}

	/**
	 * Retrieve a value based on a given condition.
	 *
	 * @param  bool $condition
	 * @param  mixed $value
	 * @param  mixed $default
	 * @return \Illuminate\Http\Resources\MissingValue|mixed
	 */
	public function whenProxy2($condition, $value) {
		return $this->when($condition, $value);
	}

	/**
	 * Retrieve a value based on a given condition.
	 *
	 * @param  bool $condition
	 * @param  mixed $value
	 * @param  mixed $default
	 * @return \Illuminate\Http\Resources\MissingValue|mixed
	 */
	public function whenProxy3($condition, $value, $default = null) {
		return $this->when($condition, $value, $default);
	}

	/**
	 * Execute a callback if the given pivot table has been loaded.
	 *
	 * @param  string $accessor
	 * @param  string $table
	 * @param  mixed $value
	 * @param  mixed $default
	 * @return \Illuminate\Http\Resources\MissingValue|mixed
	 */
	public function whenPivotLoadedAsProxy($accessor, $table, $value) {
		return $this->whenPivotLoaded($accessor, $table, $value);
	}

	/**
	 * Execute a callback if the given pivot table has been loaded.
	 *
	 * @param  string $accessor
	 * @param  string $table
	 * @param  mixed $value
	 * @param  mixed $default
	 * @return \Illuminate\Http\Resources\MissingValue|mixed
	 */
	public function whenPivotLoadedAsProxy2($accessor, $table, $value, $default) {
		return $this->whenPivotLoaded($accessor, $table, $value, $default);
	}

	/**
	 * @param Request $request
	 * @return array
	 * @throws \Exception
	 */
	public function toArray($request) {
		return static::bindings()->generateArrayFromOutputs($this, $request);
	}

	/**
	 * @param Request $request
	 * @param bool $noSkip
	 * @return array
	 */
	public static function fromArray(Request $request) {
		return static::bindings()->generateArrayFromInputs($request);
	}

	/**
	 * @param Request $request
	 * @param $action
	 * @param AbstractModel|null $model
	 * @return array
	 */
	public static function validators(Request $request, $action, $model) {
		$validators = [];
		foreach (static::bindings()->getInputs() as $input) {
			if ($input->hasMorphData()) {
				if (RoutesList::isGenerating()) {
					$validators[$input->getSourceKey()] = ['dynamic'];
				} else {
					$morphClass = $input->getMorphData()->getMorphedClass($request);
					$morphValue = $input->getValue($request);
					if ($morphClass && $morphValue != null) {
						$class = $morphClass::resourceClass();
						$newRequest = new Request($request->query->all(), $request->request->all(), $request->attributes->all(), $request->cookies->all(), $request->files->all(), $request->server->all(), $request->getContent());
						$newRequest->replace($morphValue);
						$classValidators = $class::validators($newRequest, $action, !empty($model) ? $model->{$input->getKey()} : null);
						foreach ($classValidators as $key => $classValidator) {
							$validators[$input->getSourceKey() . '.' . $key] = $classValidator;
						}
					}
				}
			} else if ($input->hasValidator()) {
				if (RoutesList::isGenerating()) {
					try {
						$validators[$input->getSourceKey()] = $input->getValidator()->retrieveValidator($request, $action, $model);
					} catch (\Exception $e) {
						$validators[$input->getSourceKey()] = ['dynamic'];
					}
				} else {
					$validators[$input->getSourceKey()] = $input->getValidator()->retrieveValidator($request, $action, $model);
				}
			}
		};
		return $validators;
	}

	/**
	 * @param $request
	 * @param AbstractModel|null $model
	 * @return AbstractModel
	 */
	public static function fillModelFromRequest($request, AbstractModel $model = null) {
		$modelClass = new static::$modelClass;
		if ($model == null) {
			$model = new $modelClass();
		}

		$array = static::fromArray($request);
		$model->fill($array);

		return $model;
	}

	/**
	 * @param Request $request
	 * @param callable $callback
	 * @return AbstractModel|Collection
	 * @throws \Exception
	 */
	public static function resourceWrapper(Request $request, $callback) {
		$responseScope = app(ResponseScope::class);
		$staticContainer = static::modelClass()::query();

		/** @var GlobalScope $globalScope */
		$globalScope = app(GlobalScope::class);
		if ($globalScope->enabled()) {
			$scopes = $globalScope->getScopes();
			$responseScope->addHeader('X-RSLR-Unscoped', $scopes);
			$staticContainer->withoutGlobalScopes($scopes);
		}

		$inputWithQueryCallbacks = new InputQueryCallbackContainer();

		// Process order by
		static::processOrderQueryParam($request, $staticContainer);

		// Process with
		static::processWithQueryParam($request, $inputWithQueryCallbacks);

		// Process filters
		static::processFilterQueryParam($request, $staticContainer, $inputWithQueryCallbacks);

		// Process inputQueryCallbacks
		$inputWithQueryCallbacks->process($staticContainer);

		// Check total records before limit, offset and with
		$countQuery = clone $staticContainer;
		$responseScope->addHeader('X-RSLR-Count', $countQuery->count());

		// Process limit and offset
		$limit = $request->query('limit');
		if ($limit) {
			$responseScope->addHeader('X-RSLR-Limit', $limit);
			$staticContainer->take($limit);
		}

		$offset = $request->query('offset');
		if ($offset) {
			$responseScope->addHeader('X-RSLR-Offset', $offset);
			$staticContainer->skip($offset);
		}

		// Add additional response headers
		if (Auth::check()) {
			$responseScope->addHeader('X-RSLR-User', Auth::user()->getAuthIdentifier());
		}

		if ($responseScope->isExpiredToken()) {
			$responseScope->addHeader('X-RSLR-Expired', 1);
		}

		$appSettings = app(AppSettings::class);
		$responseScope->addHeader('X-RSLR-BuildVersion', $appSettings->getBuildVersion());

		return call_user_func($callback, $staticContainer);
	}

	/**
	 * @param Request $request
	 * @param $staticContainer
	 * @throws IllegalOperationException
	 */
	private static function processOrderQueryParam(Request $request, $staticContainer) {
		$responseScope = app(ResponseScope::class);
		$orders = $request->query('order');
		if ($orders) {
			$headers = [];
			$orders = explode(';', $orders);
			foreach ($orders as $order) {
				$dir = 'desc';
				if (preg_match('/(.*),(desc|asc)/', $order, $matches)) {
					$order = $matches[1];
					$dir = $matches[2];
				}

				$set = false;
				foreach (static::bindings()->getOutputs() as $binding) {
					if ($binding->getKey() == $order && $binding->getSourceKey()) {
						$staticContainer->orderBy($binding->getSourceKey(), $dir);
						$set = true;
						break;
					}
				}

				if (!$set) {
					throw new IllegalOperationException("Illegal order: $order");
				} else {
					$headers[$order] = $dir;
				}
			}
			$responseScope->addHeader('X-RSLR-Order', $headers);
		}
	}

	/**
	 * @param Request $request
	 * @param InputQueryCallbackContainer $inputWithQueryCallbacks
	 */
	private static function processWithQueryParam(Request $request, $inputWithQueryCallbacks) {
		$responseScope = app(ResponseScope::class);

		$with = $request->query('with');
		if ($with) {
			$with = explode(';', $with);
			$responseScope->addHeader('X-RSLR-With', $with);

			foreach ($with as $w) {
				$inputWithQueryCallbacks->get($w)->add();
			}
		}
	}

	/**
	 * @param Request $request
	 * @param $staticContainer
	 * @param InputQueryCallbackContainer $inputWithQueryCallbacks
	 * @throws IllegalOperationException
	 */
	private static function processFilterQueryParam(Request $request, $staticContainer, $inputWithQueryCallbacks) {
		$responseScope = app(ResponseScope::class);
		$filter = $request->query('filter');
		if ($filter) {
			$filter = explode(';', $filter);
			$headers = [];
			foreach ($filter as $f) {
				$headers[] = static::processFilterQueryParamPart($f, $staticContainer, $inputWithQueryCallbacks);
			}
			$responseScope->addHeader('X-RSLR-Filter', $headers);
		}
	}

	/**
	 * @param $filterPart
	 * @param $staticContainer
	 * @param InputQueryCallbackContainer $inputWithQueryCallbacks
	 * @return array
	 * @throws IllegalOperationException
	 * @throws \Exception
	 */
	private static function processFilterQueryParamPart($filterPart, $staticContainer, $inputWithQueryCallbacks) {
		$matches = [];
		if (preg_match('/^(?:([d]*),)?([A-Za-z0-9._-]+),(?:(?:(n?eq|like|gte?|lte?|n?in),(.*))|(?:(n?null)))?$/', $filterPart, $matches)) {
			$modifiers = array_key_exists(1, $matches) ? $matches[1] : null;
			$left = array_key_exists(2, $matches) ? $matches[2] : null;
			$operator = array_key_exists(3, $matches) ? $matches[3] : (array_key_exists(5, $matches) ? $matches[5] : null);
			$right = array_key_exists(4, $matches) ? $matches[4] : null;
			array_shift($matches);
		} else {
			throw new IllegalOperationException("Cannot parse filter $filterPart");
		}

		// Get left part
		list ($tableBinding, $propertyBinding, $table, $left) = static::processFilterQueryParamPartLeft($left);

		// Get operator part
		$operator = static::processFilterQueryParamPartOperator($operator);

		// Get right part
		$right = $right ? static::processFilterQueryParamPartRight($right, $propertyBinding, $operator) : null;

		// Get modifiers
		list($strictDeepFilter) = $modifiers ? static::processFilterQueryParamModifiers($modifiers) : [false];

		// Here we decide if we add the filter to the extended object or to the current object
		$whereHas = $tableBinding ? static::isCollectionTypeQuery($tableBinding) : false;

		if ($whereHas && $tableBinding) {
			$resource = $tableBinding->getKey();
			if (!$strictDeepFilter) {
				/**
				 * @param Builder|Relation $query
				 */
				$inputWithQueryCallbacks->get($resource)->add(function($query) use ($table, $left, $operator, $right) {
					if ($operator == 'null') {
						$query->whereNull("$table.$left");
					} else if ($operator == 'nnull') {
						$query->whereNotNull("$table.$left");
					} else if ($operator == 'in') {
						$query->whereIn("$table.$left", explode(',', $right));
					} else if ($operator == 'nin') {
						$query->whereNotIn("$table.$left", explode(',', $right));
					} else {
						$query->where("$table.$left", $operator, $right);
					}
				});
			} else {
				$staticContainer->whereHas($resource, function($query) use ($operator, $table, $left, $right) {
					if ($operator == 'null') {
						$query->whereNull("$table.$left");
					} else if ($operator == 'nnull') {
						$query->whereNotNull("$table.$left");
					} else if ($operator == 'in') {
						$query->whereIn("$table.$left", explode(',', $right));
					} else if ($operator == 'nin') {
						$query->whereNotIn("$table.$left", explode(',', $right));
					} else {
						$query->where("$table.$left", $operator, $right);
					}
				});
			}
		} else {
			if ($operator == 'null') {
				$staticContainer->whereNull("$table.$left");
			} else if ($operator == 'nnull') {
				$staticContainer->whereNotNull("$table.$left");
			} else if ($operator == 'in') {
				$staticContainer->whereIn("$table.$left", explode(',', $right));
			} else if ($operator == 'nin') {
				$staticContainer->whereNotIn("$table.$left", explode(',', $right));
			} else {
				$staticContainer->where("$table.$left", $operator, $right);
			}
		}
		return $matches;
	}

	/**
	 * @param $left
	 * @return array
	 * @throws IllegalOperationException
	 */
	private static function processFilterQueryParamPartLeft($left) {
		$resources = explode('.', $left);
		$propertyBinding = null;
		$property = array_pop($resources);
		$tableBinding = null;
		$table = static::getTableName();
		$bindings = static::bindings()->getOutputs();
		$nestedBinding = null;

		foreach ($resources as $resource) {
			$set = false;
			foreach ($bindings as $b) {
				if ($b->getKey() == $resource) {
					if ($b->hasNested()) {
						if ($b->getSourceKey()) {
							$tableBinding = $b;
							$nestedResource = $b->getNested()->getNestedResource();
							$bindings = $nestedResource::bindings()->getOutputs();
							$table = $nestedResource::getTableName();
							$set = true;
							break;
						} else {
							throw new IllegalOperationException("Parameter path {$resource} is not filterable");
						}
					} else {
						throw new IllegalOperationException("Parameter path {$resource} is not nested");
					}
				}
			}
			if (!$set) {
				throw new IllegalOperationException("Filter resource path {$resource} does not exists");
			}
		}

		foreach ($bindings as $b) {
			if ($b->getKey() == $property) {
				if ($b->getSourceKey()) {
					$propertyBinding = $b;
					break;
				} else {
					throw new IllegalOperationException("Parameter {$property} is not filterable");
				}
			}
		}

		if ($propertyBinding === null) {
			throw new IllegalOperationException("Parameter {$property} does not exists");
		}

		return [$tableBinding, $propertyBinding, $table, $property];
	}

	/**
	 * @param $operator
	 * @return string
	 * @throws \Exception
	 */
	private static function processFilterQueryParamPartOperator($operator) {
		$operator = self::convertQueryFilter($operator);
		return $operator;
	}

	/**
	 * @param $right
	 * @param $propertyBinding
	 * @param $operator
	 * @return string
	 */
	private static function processFilterQueryParamPartRight($right, $propertyBinding, $operator) {
		if ($propertyBinding->hasEnumType()) {
			$right = $propertyBinding->getEnumType()->getMorphedValue($right);
		}
		if ($operator == 'like') {
			$right = "%{$right}%";
		}
		return $right;
	}

	/**
	 * @param $modifiers
	 * @return array
	 */
	private static function processFilterQueryParamModifiers($modifiers) {
		$strictDeepFilter = str_contains($modifiers, 'd');
		return [$strictDeepFilter];
	}

	/**
	 * @param $tableBinding
	 * @return bool
	 */
	private static function isCollectionTypeQuery($tableBinding) {
		return (!$tableBinding->hasNested() || !is_subclass_of($tableBinding->getNested(), AbstractCollection::class));
	}

	/**
	 * @param string $queryFilter
	 * @return string
	 * @throws \Exception
	 */
	private static function convertQueryFilter($queryFilter) {
		switch ($queryFilter) {
			case 'eq':
				return '=';
			case 'neq':
				return '!=';
			case 'like':
				return 'like';
			case 'gt':
				return '>';
			case 'gte':
				return '>=';
			case 'lt':
				return '<';
			case 'lte':
				return '<=';
			case 'null':
			case 'nnull':
			case 'in':
			case 'nin':
				return $queryFilter;
			default:
				throw new \Exception("cannot parse filter");
		}
	}

}
