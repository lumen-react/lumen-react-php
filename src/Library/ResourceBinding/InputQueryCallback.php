<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

class InputQueryCallback {

	private $key;

	private $callbacks = [];

	/**
	 * InputQueryCallback constructor.
	 * @param $key
	 */
	public function __construct($key) {
		$this->key = $key;
	}

	/**
	 * @param callable $callback
	 */
	public function add(callable $callback = null) {
		if ($callback !== null) {
			$this->callbacks[] = $callback;
		}
	}

	/**
	 * @return mixed
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * @return array
	 */
	public function getCallbacks(): array {
		return $this->callbacks;
	}

	/**
	 * @return bool
	 */
	public function hasCallbacks(): bool {
		return count($this->callbacks) > 0;
	}

}