<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use Illuminate\Http\Request;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;

class ResourceOutputWithCallback {

	private $key = true;
	private $withCallback = null;
	private $params = null;

	/**
	 * ResourceOutputWith constructor.
	 * @param callable|string|true $key
	 * @param callable|null $withCallback
	 */
	public function __construct($key = true, $withCallback = null) {
		$this->key = $key;
		$this->withCallback = $withCallback;
	}

	/**
	 * @param callable|string|true $key
	 * @param callable|null $withCallback $withCallback
	 * @return ResourceOutputWithCallback
	 */
	public static function make($key = true, $withCallback = null) {
		return new static($key, $withCallback);
	}

	/**
	 * @return callable|null
	 */
	public function getWithCallback(): callable {
		return $this->withCallback;
	}

	/**
	 * @param callable|null
	 * @return ResourceOutputWithCallback
	 */
	public function setWithCallback(callable $withCallback) {
		$this->withCallback = $withCallback;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function hasWithCallback(): bool {
		return !empty($this->withCallback);
	}

	/**
	 * @param AbstractResource $resource
	 * @param Request $request
	 */
	public function setParams(AbstractResource $resource, Request $request) {
		$this->params = [$resource, $request];
	}

	/**
	 * @return null
	 */
	public function getParams() {
		return $this->params;
	}

	/**
	 * @return callable|string|null
	 */
	public function getWithCallbackKey($key) {
		if ($this->key instanceof ResourceCallable) {
			return call_user_func_array($this->key->getCallable(), $this->params);
		} else if ($this->key === true) {
			return $key;
		} else {
			return $this->key;
		}
	}

}