<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use Illuminate\Http\Request;

class ResourceInputModifier {

	private $modifierValue;

	/**
	 * ResourceInputPopulator constructor.
	 * @param $modifierValue
	 */
	public function __construct($modifierValue) {
		$this->modifierValue = $modifierValue;
	}

	/**
	 * @param $modifierValue
	 * @return ResourceInputModifier
	 */
	public static function make($modifierValue) {
		return new static($modifierValue);
	}

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function getModifierValue(Request $request, $value) {
		if ($this->modifierValue instanceof ResourceCallable) {
			return call_user_func($this->modifierValue->getCallable(), $request, $value);
		} else {
			return $this->modifierValue;
		}
	}

	/**
	 * @param mixed $modifierValue
	 * @return ResourceInputModifier
	 */
	public function setModifierValue($modifierValue) {
		$this->modifierValue = $modifierValue;
		return $this;
	}

}