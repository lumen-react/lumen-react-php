<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

abstract class AbstractResourceElement {

	protected $key;
	protected $source = true;
	protected $enumType = null;
	protected $isTypeOptional = false;
	protected $type = null;

	/**
	 * @param true|string|callable $source
	 * @return $this
	 */
	public function setSource($source) {
		$this->source = $source;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSource() {
		return $this->source;
	}

	/**
	 * @return bool|mixed|null|string
	 */
	public function getSourceKey() {
		$key = null;

		if ($this->source === true) {
			$key = $this->key;
		} else if (is_string($this->source)) {
			$key = $this->source;
		} else if ($this->source instanceof AbstractResourceElement) {
			$key = $this->source->getKey();
		}
		return $key;
	}

	/**
	 * @return mixed
	 */
	public function getKey() {
		if ($this->key instanceof AbstractResourceElement) {
			return $this->key->getKey();
		} else {
			return $this->key;
		}
	}

	/**
	 * @param ResourceEnumType|string $enumType
	 * @return $this
	 */
	public function setEnumType($enumType) {
		if (!($enumType instanceof ResourceEnumType)) {
			$enumType = ResourceEnumType::make($enumType);
		}
		$this->enumType = $enumType;
		return $this;
	}

	/**
	 * @return ResourceEnumType
	 */
	public function getEnumType() {
		return $this->enumType;
	}

	/**
	 * @return bool
	 */
	public function hasEnumType(): bool {
		return !empty($this->enumType);
	}

	/**
	 * @param bool $isTypeOptional
	 * @return $this
	 */
	public function setTypeOptional(bool $isTypeOptional = true): AbstractResourceElement {
		$this->isTypeOptional = $isTypeOptional;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isTypeOptional(): bool {
		return $this->isTypeOptional;
	}

	/**
	 * @param $type
	 * @return $this
	 */
	public function setType($type) {
		if (!($type instanceof ResourceType)) {
			$type = ResourceType::make($type);
		}
		$this->type = $type;
		return $this;
	}

	/**
	 * @return ResourceType
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getTypeString() {
		return $this->hasType() ? $this->getType()->getType() : 'any';
	}

	/**
	 * @return bool
	 */
	public function hasType(): bool {
		return !empty($this->type);
	}

}