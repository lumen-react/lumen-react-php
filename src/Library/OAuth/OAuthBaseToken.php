<?php

namespace RajeevSiewnath\LumenReact\Library\OAuth;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Support\Facades\Crypt;
use JsonSerializable;

class OAuthBaseToken implements Arrayable, Jsonable, JsonSerializable {

	protected $accessToken;
	protected $provider;
	protected $type;

	/**
	 * OAuthToken constructor.
	 * @param string $accessToken
	 * @param string $provider
	 * @param $type
	 */
	public function __construct(string $accessToken, string $provider, $type) {
		$this->accessToken = $accessToken;
		$this->provider = $provider;
		$this->type = $type;
	}

	/**
	 * @param $encrypted
	 * @return OAuthBaseToken
	 */
	public static function fromEncrypted($encrypted) {
		$data = json_decode(Crypt::decrypt($encrypted), true);
		return new OAuthBaseToken(
			$data['access_token'],
			$data['provider'],
			$data['type']
		);
	}

	/**
	 * @return string
	 */
	public function encrypt() {
		return Crypt::encrypt(json_encode($this->toArray()));
	}

	/**
	 * @return mixed
	 */
	public function getAccessToken() {
		return $this->accessToken;
	}

	/**
	 * @return mixed
	 */
	public function getProvider() {
		return $this->provider;
	}

	/**
	 * @param string $accessToken
	 */
	public function setAccessToken(string $accessToken) {
		$this->accessToken = $accessToken;
	}

	/**
	 * @return mixed
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return bool
	 */
	public function isClientUserToken() {
		return $this->type === 'client';
	}

	/**
	 * @return array
	 */
	public function toArray() {
		return [
			'access_token' => $this->accessToken,
			'provider'     => $this->provider,
			'type'         => $this->type,
		];
	}

	/**
	 * Convert the object to its JSON representation.
	 *
	 * @param  int $options
	 * @return string
	 */
	public function toJson($options = 0) {
		$json = json_encode($this->jsonSerialize(), $options);

		if (JSON_ERROR_NONE !== json_last_error()) {
			throw JsonEncodingException::forModel($this, json_last_error_msg());
		}

		return $json;
	}

	/**
	 * Specify data which should be serialized to JSON.
	 *
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 *
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 *               which is a value of any type other than a resource.
	 *
	 * @since 5.4.0
	 */
	public function jsonSerialize() {
		return $this->toArray();
	}

	/**
	 * Convert the model to its string representation.
	 *
	 * @return string
	 */
	public function __toString() {
		return $this->toJson();
	}
}