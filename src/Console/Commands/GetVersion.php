<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

class GetVersion extends AbstractCommand {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'rs:lr:version {--skip-write : Skip writing to file}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Write or read the version of the app';

	/**
	 * Execute the console command.
	 *
	 * @return void
	 * @throws \Exception
	 */
	public function handle() {
		$filename = base_path() . '/.version';
		if (!$this->option('skip-write')) {
			file_put_contents($filename, exec("git describe --tags"));
		}
		$this->info("The version is:");
		$this->info(file_get_contents($filename));
	}

}
