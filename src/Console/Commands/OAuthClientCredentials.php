<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

use RajeevSiewnath\LumenReact\Http\Middleware\OAuthMiddleware\OAuthMiddlewareImplementation;

class OAuthClientCredentials extends AbstractCommand {

	protected $signature = 'rs:lr:oauth:cc {--p|provider= : Which oAuth provider should be used?}';
	protected $description = 'Generate oAuth client credentials token';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		if ($this->hasOption("provider")) {
			$class = OAuthMiddlewareImplementation::getProviderClassByName($this->option("provider"));
			if (!$class) {
				$this->error("No such provider exists");
				return 1;
			}
			$this->info($class::createClientCredentialsToken());
		} else {
			$this->error("No provider given");
			return 1;
		}

		return 0;
	}

}