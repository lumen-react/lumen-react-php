<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

class ResourceOutputWhenPivotLoaded {

	private $accessor;
	private $table;
	private $whenPivotLoadedCallback;

	/**
	 * ResourceOutputWhenPivotLoaded constructor.
	 * @param callable|string $accessor
	 * @param callable|null $withCallback
	 * @param bool $whenPivotLoaded
	 */
	public function __construct($accessor, $table, $whenPivotLoadedCallback) {
		$this->accessor = $accessor;
		$this->table = $table;
		$this->whenPivotLoadedCallback = $whenPivotLoadedCallback;
	}

	/**
	 * @param bool $whenPivotLoaded
	 * @param null $withCallback
	 * @return ResourceOutputWhenPivotLoaded
	 */
	public static function make($accessor, $table, $whenPivotLoadedCallback) {
		return new static($accessor, $table, $whenPivotLoadedCallback);
	}

	/**
	 * @return ResourceCallable
	 */
	public function getWhenPivotLoadedCallback() {
		return $this->whenPivotLoadedCallback instanceof ResourceCallable ? $this->whenPivotLoadedCallback : ResourceCallable::make($this->whenPivotLoadedCallback);
	}

	/**
	 * @return mixed
	 */
	public function getTable() {
		return $this->table;
	}

	/**
	 * @return callable|string
	 */
	public function getAccessor() {
		return $this->accessor;
	}

}