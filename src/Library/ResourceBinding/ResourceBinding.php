<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use Illuminate\Http\Request;
use RajeevSiewnath\LumenReact\AbstractModel;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;

class ResourceBinding {

	private $inputs = [];
	private $outputs = [];
	private $options = null;

	/**
	 * ResourceBinding constructor.
	 * @param string $resourceClass
	 * @param ...$bindings
	 */
	public function __construct(...$bindings) {
		foreach ($bindings as $binding) {
			if ($binding instanceof ResourceInput) {
				$this->inputs[] = $binding;
			} else if ($binding instanceof ResourceOutput) {
				$this->outputs[] = $binding;
			} else if ($binding instanceof ResourceBindingOptions) {
				$this->options = $binding;
			}
		}

		if ($this->options == null) {
			$this->options = new ResourceBindingOptions();
		}
	}

	/**
	 * @param mixed ...$bindings
	 * @return static
	 */
	public static function make(...$bindings) {
		return new static(...$bindings);
	}

	/**
	 * @param $resourceClass
	 */
	public function generateAutoAttributes($resourceClass) {
		foreach ($this->inputs as $input) {
			if ($input->isAutoValidator()) {
				$rule = $input->generateAutoValidator($resourceClass);
				if ($rule) {
					if ($input->hasValidator()) {
						$validator = $input->getValidator();
					} else {
						$validator = new ResourceInputValidator([]);
						$input->setValidator($validator);
					}
					$validator->addStaticValidator($rule);
				}
			}
		}
		foreach ($this->outputs as $output) {
			if ($output->isAutoType() && !$output->hasType()) {
				$output->setType(ResourceType::make($output->generateAutoType($resourceClass)));
			}
		}
	}

	/**
	 * @return ResourceInput[]
	 */
	public function getInputs() {
		return $this->inputs;
	}

	/**
	 * @return ResourceOutput[]
	 */
	public function getOutputs() {
		return $this->options->isRequest() ? $this->inputs : $this->outputs;
	}

	/**
	 * @param AbstractResource $resource
	 * @param Request $request
	 * @return array
	 * @throws \Exception
	 */
	public function generateArrayFromOutputs(AbstractResource $resource, Request $request) {
		$array = [];
		foreach ($this->getOutputs() as $resourceOutput) {
			$array[$resourceOutput->getKey()] = $resourceOutput->getValue($resource, $request);
		}
		return $array;
	}

	/**
	 * @param Request $request
	 * @param null $morphData
	 * @return array
	 */
	public function generateArrayFromInputs(Request $request, &$morphData = null) {
		$array = [];
		foreach ($this->getInputs() as $resourceInput) {
			if (!$resourceInput->isPassToInput()) {
				continue;
			}

			if ($resourceInput->hasMorphData()) {
				$class = $resourceInput->getMorphData()->getMorphedClass($request);
				if ($morphData !== null) {
					$morphData[$resourceInput->getKey()] = $class::bindings()->generateArrayFromInputs($request, $morphData);
				}
			} else if ($resourceInput->isPresentInRequest($request) || $resourceInput->hasModifier()) {
				$array[$resourceInput->getKey()] = $resourceInput->getValue($request);
			}
		}
		foreach ($this->options as $option) {
			if ($option->hasPopulatedKeys()) {
				foreach ($option->getPopulatedKeys() as $key) {
					$array[$key] = $request->get($key, null);
				}
			}
		}
		return $array;
	}

	/**
	 * @param Request $request
	 * @param $morphKey
	 * @return mixed|null
	 */
	public function generateMorphDataFromInputs(Request $request, $morphKey, &$modelClass = null) {
		foreach ($this->getInputs() as $resourceInput) {
			if ($resourceInput->hasMorphData() && $morphKey == $resourceInput->getSourceKey()) {
				$class = $resourceInput->getMorphData()->getMorphedClass($request);

				$newRequest = new Request($request->query->all(), $request->request->all(), $request->attributes->all(), $request->cookies->all(), $request->files->all(), $request->server->all(), $request->getContent());
				$morphValue = $resourceInput->getValue($request) ?? [];
				$newRequest->replace($morphValue);
				$modelClass = $class;

				return $class::resourceClass()::bindings()->generateArrayFromInputs($newRequest);
			}
		}
		return null;
	}

	/**
	 * @param Request $request
	 * @param $morphKey
	 * @return AbstractModel|null
	 */
	public function makeMorphDataInstanceFromInputs(Request $request, $morphKey) {
		$modelClass = null;
		$data = $this->generateMorphDataFromInputs($request, $morphKey, $modelClass);
		return $modelClass::make($data);
	}

}