<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use RajeevSiewnath\LumenReact\AbstractModel;

class ResourceInput extends AbstractResourceElement {

	private $passToInput = true;
	private $presenceCheck = true;
	private $updateResolver = null;
	private $modifier = null;
	private $validator = null;
	private $dataMangler = null;
	private $morphData = null;
	private $autoValidator = true;

	/**
	 * ResourceInput constructor.
	 * @param mixed $key
	 * @param mixed $source
	 * @param mixed $passToInput
	 */
	public function __construct($key, $source = true, $passToInput = true) {
		$this->key = $key;
		$this->setSource($source);
		$this->passToInput = $passToInput;
	}

	/**
	 * @param mixed $key
	 * @param mixed $source
	 * @param mixed $passToInput
	 * @return ResourceInput
	 */
	public static function make($key, $source = true, $passToInput = true) {
		return new static($key, $source, $passToInput);
	}

	/**
	 * @return ResourceOutput
	 */
	public function generateResourceOutput($autoFill = true) {
		$output = new ResourceOutput($this->getSourceKey(), $this->key);

		if ($autoFill) {
			if ($this->hasEnumType()) {
				$output->setEnumType(ResourceEnumType::make($this->getEnumType()->getEnum()));
			}
			$output->setTypeOptional($this->isTypeOptional);
		}

		return $output;
	}

	/**
	 * @param Request $request
	 * @return mixed|null
	 */
	public function getValue(Request $request) {
		$return = null;
		$key = $this->getSourceKey();

		if ($this->hasDataMangler()) {
			$request = new Request($request->query->all(), $request->request->all(), $request->attributes->all(), $request->cookies->all(), $request->files->all(), $request->server->all(), $request->getContent());
			$request->replace($this->getDataMangler()->getDataManglerValue($request));
		}

		if ($key && $this->hasEnumType()) {
			$return = $this->getEnumType()->getMorphedValue($request->input($key));
		} else if ($this->source instanceof ResourceCallable) {
			$return = call_user_func($this->source->getCallable(), $request);
		} else if ($this->isPresentInRequest($request)) {
			$return = $request->input($key);
		}

		if ($this->hasModifier()) {
			return $this->getModifier()->getModifierValue($request, $return);
		} else {
			return $return;
		}
	}

	/**
	 * @return bool
	 */
	public function isPassToInput(): bool {
		return $this->passToInput;
	}

	/**
	 * @param Request $request
	 * @return bool|mixed|null
	 */
	public function shouldBePassedToInput(Request $request) {
		if (is_bool($this->passToInput)) {
			return $this->passToInput;
		} else if ($this->source instanceof ResourceCallable) {
			return call_user_func($this->source->getCallable(), $request);
		} else {
			return null;
		}
	}

	/**
	 * @param Request $request
	 * @return mixed|null
	 */
	public function isPresentInRequest(Request $request) {
		if ($this->presenceCheck === true) {
			return $request->has($this->key);
		} else if (is_string($this->presenceCheck)) {
			return $request->has($this->presenceCheck);
		} else if ($this->presenceCheck instanceof ResourceOutput) {
			return $request->has($this->presenceCheck->getKey());
		} else if ($this->presenceCheck instanceof ResourceCallable) {
			return call_user_func($this->presenceCheck->getCallable(), $request);
		} else {
			return null;
		}
	}

	/**
	 * @param true|string|callable $source
	 * @return AbstractResourceElement
	 */
	public function setSource($source, $setPresenceCheck = true) {
		if ($setPresenceCheck) {
			$this->presenceCheck = $source;
		}
		return parent::setSource($source);
	}

	/**
	 * @param bool|callable $passToInput
	 * @return ResourceInput
	 */
	public function setPassToInput($passToInput = false): ResourceInput {
		$this->passToInput = $passToInput;
		return $this;
	}

	/**
	 * @param bool|callable $presenceCheck
	 * @return ResourceInput
	 */
	public function setPresenceCheck($presenceCheck = false): ResourceInput {
		$this->presenceCheck = $presenceCheck;
		return $this;
	}

	/**
	 * @param $modifier
	 * @return ResourceInput
	 */
	public function setModifier($modifier) {
		if (!($modifier instanceof ResourceInputModifier)) {
			$modifier = ResourceInputModifier::make($modifier);
		}
		$this->modifier = $modifier;
		return $this;
	}

	/**
	 * @return ResourceInputModifier
	 */
	public function getModifier() {
		return $this->modifier;
	}

	/**
	 * @return array
	 */
	public function hasModifier(): bool {
		return $this->modifier != null;
	}

	/**
	 * @param $validator
	 * @return ResourceInput
	 */
	public function setValidator($validator) {
		if (!($validator instanceof ResourceInputValidator)) {
			$validator = ResourceInputValidator::make($validator);
		}
		$this->validator = $validator;
		return $this;
	}

	/**
	 * @return ResourceInputValidator
	 */
	public function getValidator() {
		return $this->validator;
	}

	/**
	 * @return bool
	 */
	public function hasValidator(): bool {
		return $this->validator != null;
	}

	/**
	 * @param ResourceInputUpdateResolver $updateResolver
	 * @return ResourceInput
	 */
	public function setUpdateResolver($updateResolver) {
		$this->updateResolver = $updateResolver;
		return $this;
	}

	/**
	 * @return ResourceInputUpdateResolver
	 */
	public function getUpdateResolver() {
		return $this->updateResolver;
	}

	/**
	 * @return bool
	 */
	public function hasUpdateResolver(): bool {
		return $this->updateResolver != null;
	}

	/**
	 * @param ResourceInputDataMangler $dataMangler
	 * @return ResourceInput
	 */
	public function setDataMangler($dataMangler) {
		$this->dataMangler = $dataMangler;
		return $this;
	}

	/**
	 * @return ResourceInputDataMangler
	 */
	public function getDataMangler() {
		return $this->dataMangler;
	}

	/**
	 * @return array
	 */
	public function hasDataMangler(): bool {
		return !empty($this->whenLoaded);
	}

	/**
	 * @param $morphData
	 * @return ResourceInput
	 */
	public function setMorphData($morphData) {
		if (!($morphData instanceof ResourceInputMorphData)) {
			$morphData = ResourceInputMorphData::make($morphData);
		}
		$this->morphData = $morphData;
		return $this;
	}

	/**
	 * @return ResourceInputMorphData
	 */
	public function getMorphData(): ResourceInputMorphData {
		return $this->morphData;
	}

	/**
	 * @return bool
	 */
	public function hasMorphData(): bool {
		return !empty($this->morphData);
	}

	/**
	 * @param bool $autoValidator
	 * @return ResourceInput
	 */
	public function setAutoValidator(bool $autoValidator = false): ResourceInput {
		$this->autoValidator = $autoValidator;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isAutoValidator(): bool {
		return $this->autoValidator;
	}

	/**
	 * @param $resourceClass
	 * @return array
	 */
	public function generateAutoValidator($resourceClass) {
		$validators = [];
		if ($this->hasEnumType()) {
			$validators[] = Rule::in($this->getEnumType()->getEnum()->getKeys());
		} else {
			$empty = $resourceClass::modelClass()::make();
			$key = $this->getKey();
			if ($empty instanceof AbstractModel) {
				$type = $empty->getCastType($key);
				if ($type) {
					switch ($type) {
						case 'int':
						case 'integer':
							$validators[] = 'integer';
							break;
						case 'real':
						case 'float':
						case 'double':
							$validators[] = 'numeric';
							break;
						case 'bool':
						case 'boolean':
							$validators[] = 'boolean';
							break;
						case 'object':
						case 'json':
							$validators[] = 'json';
							break;
						case 'array':
						case 'collection':
							$validators[] = 'array';
							break;
						case 'string':
						case 'date':
						case 'datetime':
						case 'timestamp':
						default:
							$validators[] = 'string';
							break;
					}
				}
			}
		}

		if (!$this->isTypeOptional) {
			$validators[] = 'present';
		} else {
			$validators[] = 'nullable';
		}

		return $validators;
	}

}