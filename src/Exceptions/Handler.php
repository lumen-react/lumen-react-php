<?php

namespace RajeevSiewnath\LumenReact\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use RajeevSiewnath\LumenReact\Library\AppSettings\AppSettings;
use RajeevSiewnath\LumenReact\Library\ResponseScope\ResponseScope;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		AuthorizationException::class,
		HttpException::class,
		ModelNotFoundException::class,
		ValidationException::class,
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception $e
	 * @return void
	 * @throws Exception
	 */
	public function report(Exception $e) {
		$appSettings = app(AppSettings::class);
		$errorReportCallback = $appSettings->getErrorReportCallback();
		if ($errorReportCallback) {
			$errorReportCallback($this, $e);
		}
		parent::report($e);
	}

	/**
	 * @param Exception $e
	 * @param $default
	 * @return string
	 */
	private function getMessage(Exception $e, $default) {
		$message = $e->getMessage();
		$message = empty($message) ? $default : $message;
		return $message;
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Exception $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e) {
		DB::rollback();

		// Add response headers
		$responseScope = app(ResponseScope::class);

		if (Auth::check()) {
			$responseScope->addHeader('X-RSLR-User', Auth::user()->getAuthIdentifier());
		}

		if ($responseScope->isExpiredToken()) {
			$responseScope->addHeader('X-RSLR-Expired', 1);
		}

		$headers = $responseScope->getOutputHeaders();

		// Handle errors

		if ($e instanceof HttpResponseException) {
			return response()->json(['error' => $this->getMessage($e, 'unexpected HTTP response')], 500, $headers);
		} else if ($e instanceof ModelNotFoundException || $e instanceof NotFoundHttpException) {
			return response()->json(['error' => $this->getMessage($e, 'not found')], 404, $headers);
		} else if ($e instanceof AuthorizationException) {
			if (Auth::check()) {
				return response()->json(['error' => $this->getMessage($e, 'unauthorized')], 403, $headers);
			} else {
				return response()->json(['error' => $this->getMessage($e, 'unauthenticated')], 401, $headers);
			}
		} else if ($e instanceof AuthenticationException) {
			return response()->json(['error' => $this->getMessage($e, 'unauthenticated')], 401, $headers);
		} else if ($e instanceof ValidationException && $e->getResponse()) {
			$errorStrings = [];
			foreach ($e->errors() as $k => $s) {
				$errorStrings[] = implode(",", $s);
			}
			return response()->json(['error' => sprintf("the following errors occurred: %s", implode(', ', $errorStrings)), 'fields' => $e->errors()], 400, $headers);
		} else if ($e instanceof IllegalOperationException) {
			$meta = $e->meta();
			if ($meta === null) {
				return response()->json(['error' => $this->getMessage($e, 'illegal operation')], $e->getCode(), $headers);
			} else {
				return response()->json(['error' => $this->getMessage($e, 'illegal operation'), 'meta' => $meta], $e->getCode(), $headers);
			}
		} else {
			if (env('APP_DEBUG')) {
				if (App::runningInConsole()) {
					return response()->make(<<<TEXT
[ERROR]
{$e->getMessage()} in {$e->getFile()} on line {$e->getLine()}
{$e->getTraceAsString()}
TEXT
					);
				} else {
					return parent::render($request, $e);
				}
			} else {
				return response()->json(['error' => 'unexpected error'], 500, $headers);
			}
		}
	}
}
