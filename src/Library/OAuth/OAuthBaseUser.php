<?php

namespace RajeevSiewnath\LumenReact\Library\OAuth;

use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\JsonEncodingException;
use JsonSerializable;

class OAuthBaseUser implements AuthenticatableContract, AuthorizableContract, Arrayable, Jsonable, JsonSerializable {

	protected $id;
	protected $provider;
	protected $accessToken;
	protected $isClientUser;

	public function __construct($id, $accessToken, $provider, $isClientUser) {
		$this->id = $id;
		$this->accessToken = $accessToken;
		$this->provider = $provider;
		$this->isClientUser = $isClientUser;
	}

	public function getAuthIdentifierName() {
		return $this->id;
	}

	public function getAuthIdentifier() {
		return $this->id;
	}

	public function getAuthPassword() {
		return null;
	}

	public function getRememberToken() {
		return null;
	}

	public function setRememberToken($value) {
	}

	public function getRememberTokenName() {
		return null;
	}

	/**
	 * @return mixed
	 */
	public function getProvider() {
		return $this->provider;
	}

	/**
	 * @return mixed
	 */
	public function getAccessToken() {
		return $this->accessToken;
	}

	/**
	 * Determine if the entity has a given ability.
	 *
	 * @param  string $ability
	 * @param  array|mixed $arguments
	 * @return bool
	 */
	public function can($ability, $arguments = []) {
		return app(Gate::class)->forUser($this)->check($ability, $arguments);
	}

	/**
	 * @return mixed
	 */
	public function isClientUser() {
		return $this->isClientUser;
	}

	/**
	 * Get the instance as an array.
	 *
	 * @return array
	 */
	public function toArray() {
		return [];
	}

	/**
	 * Convert the object to its JSON representation.
	 *
	 * @param  int $options
	 * @return string
	 */
	public function toJson($options = 0) {
		$json = json_encode($this->jsonSerialize(), $options);

		if (JSON_ERROR_NONE !== json_last_error()) {
			throw JsonEncodingException::forModel($this, json_last_error_msg());
		}

		return $json;
	}

	/**
	 * Specify data which should be serialized to JSON.
	 *
	 * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
	 *
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 *               which is a value of any type other than a resource.
	 *
	 * @since 5.4.0
	 */
	public function jsonSerialize() {
		return $this->toArray();
	}

	/**
	 * Convert the model to its string representation.
	 *
	 * @return string
	 */
	public function __toString() {
		return $this->toJson();
	}
}