<?php

namespace RajeevSiewnath\LumenReact\Providers;

use Illuminate\Support\ServiceProvider;
use RajeevSiewnath\LumenReact\Http\Middleware\FileUploadMiddleware;

class FileUploadServiceProvider extends ServiceProvider {

	public function boot() {
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->routeMiddleWare([
			'fileUpload' => FileUploadMiddleware::class,
		]);
	}
}
