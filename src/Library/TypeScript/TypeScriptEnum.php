<?php

namespace RajeevSiewnath\LumenReact\Library\TypeScript;

class TypeScriptEnum {

	private $identifier;
	private $map = [];

	/**
	 * TypeScriptEnum constructor.
	 * @param string $identifier
	 * @param mixed $array
	 */
	public function __construct($identifier, $map) {
		$this->identifier = $identifier;
		$this->map = $map;
	}

	/**
	 * @param $key
	 * @return mixed|null
	 */
	public function getValue($key) {
		foreach ($this->map as $k => $v) {
			if ($key == $k) {
				return $v;
			}
		}
		return null;
	}

	/**
	 * @return array
	 */
	public function getValues() {
		return array_values($this->map);
	}

	/**
	 * @return array
	 */
	public function getKeys() {
		return array_keys($this->map);
	}

	/**
	 * @param $value
	 * @return int|null|string
	 */
	public function getKey($value) {
		foreach ($this->map as $k => $v) {
			if ($value == $v) {
				return $k;
			}
		}
		return null;
	}

	/**
	 * @param $value
	 * @return int|null|string
	 */
	public function getKeyWithPath($key) {
		foreach ($this->map as $k => $v) {
			if ($key == $k) {
				return "{$this->getIdentifier()}.{$key}";
			}
		}
		return null;
	}

	/**
	 * @return array
	 */
	public function getTypes() {
		$classes = $this->getValues();
		array_walk($classes, function(&$class) {
			$class = $class::resourceClass()::getType();
		});
		return $classes;
	}

	/**
	 * @param $value
	 * @return array
	 */
	public function getKeysWithPath() {
		$return = [];
		foreach ($this->getKeys() as $k) {
			$return[] = $this->getKeyWithPath($k);
		}
		return $return;
	}

	/**
	 * @return string
	 */
	public function getIdentifier(): string {
		return $this->identifier;
	}

}