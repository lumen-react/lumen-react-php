<?php

namespace RajeevSiewnath\LumenReact\Console\Commands;

use RajeevSiewnath\LumenReact\Http\Controllers\AbstractController;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;

abstract class AbstractEndpointReader extends AbstractCommand {

	private $types = [];

	public function getRouteCollection() {
		global $app;
		$routeCollection = property_exists($app, 'router') ? $app->router->getRoutes() : $app->getRoutes();
		$rows = [];
		foreach ($routeCollection as $route) {
			$middleWares = $this->getMiddleware($route['action']);
			$middleWaresArray = explode(',', $middleWares);
			array_walk($middleWaresArray, function(&$value) {
				$value = trim($value);
			});

			$rows[] = [
				'verb'            => $route['method'],
				'path'            => $route['uri'],
				'args'            => $this->getPathArguments($route['uri']),
				'namedRoute'      => $this->getNamedRoute($route['action']),
				'controller'      => $this->getController($route['action']),
				'action'          => $this->getAction($route['action']),
				'middleware'      => $middleWares,
				'middlewareArray' => $middleWaresArray,
			];
		}
		return $rows;
	}

	protected function getTypes() {
		return array_unique($this->types);
	}

	public function getInputType($controller, $action) {
		if (is_subclass_of($controller, AbstractController::class)) {
			$resource = $controller::input($action);
			return $this->getTypeName($resource);
		} else {
			return null;
		}
	}

	public function getInputClass($controller, $action) {
		if (is_subclass_of($controller, AbstractController::class)) {
			return $controller::input($action);
		} else {
			return null;
		}
	}

	public function getOutputType($controller, $action) {
		if (is_subclass_of($controller, AbstractController::class)) {
			$resource = $controller::output($action);
			$resource = $this->getTypeName($resource);
			return $resource ? $resource : 'void';
		} else {
			return 'any';
		}
	}

	public function getOutputClass($controller, $action) {
		if (is_subclass_of($controller, AbstractController::class)) {
			return $controller::output($action);
		} else {
			return null;
		}
	}

	private function getTypeName($resource) {
		if ($resource) {
			$refResource = new \ReflectionClass($resource);
			$resourceName = null;
			$importName = null;
			if (is_subclass_of($resource, AbstractResource::class)) {
				$importName = $resourceName = str_replace("Resource", "", $refResource->getShortName());
			} else if (is_subclass_of($resource, AbstractCollection::class)) {
				$importName = str_replace("Collection", "", $refResource->getShortName());
				$resourceName = "Array<$importName>";
			} else {
				$importName = $resourceName = $refResource->getShortName();
			}

			$this->types[] = $importName;

			return $resourceName;
		}
		return null;
	}

	/**
	 * @param array $path
	 * @return array
	 */
	protected function getPathArguments(string $path) {
		if (preg_match_all('/{(.*?)}/', $path, $matches)) {
			array_shift($matches);
			$args = [];
			foreach ($matches[0] as $match) {
				$pos = strpos($match, ':');
				$entry = $match;
				if ($pos !== false) {
					$entry = substr($entry, 0, $pos);
				}
				$type = null;
				$parts = explode('_', $entry);
				if (sizeof($parts) == 2) {
					$entry = $parts[0];
					$type = $parts[1];
				}
				$args[] = ['argument' => $entry, 'type' => $type];
			}

			return $args;
		}

		return [];
	}

	/**
	 * @param array $action
	 * @return string
	 */
	protected function getNamedRoute(array $action) {
		return (!isset($action['as'])) ? "" : $action['as'];
	}

	/**
	 * @param array $action
	 * @return mixed|string
	 */
	protected function getController(array $action) {
		if (empty($action['uses'])) {
			return 'None';
		}

		return current(explode("@", $action['uses']));
	}

	/**
	 * @param array $action
	 * @return string
	 */
	protected function getAction(array $action) {
		if (!empty($action['uses'])) {
			$data = $action['uses'];
			if (($pos = strpos($data, "@")) !== false) {
				return substr($data, $pos + 1);
			} else {
				return "METHOD NOT FOUND";
			}
		} else {
			return 'Closure';
		}
	}

	/**
	 * @param array $action
	 * @return string
	 */
	protected function getMiddleware(array $action) {
		return (isset($action['middleware']))
			? (is_array($action['middleware']))
				? join(", ", $action['middleware'])
				: $action['middleware'] : '';
	}

	protected function hasInput($controller, $action) {
		if (is_subclass_of($controller, AbstractController::class)) {
			return !!$this->getInputClass($controller, $action);
		} else {
			return false;
		}
	}

	protected function generateJsArgumentMetadata($args, $controller, $action) {
		$resource = (is_subclass_of($controller, AbstractController::class)) ? $this->getInputType($controller, $action) : null;
		$newArgs = [];
		if ($resource) {
			foreach ($args as $arg) {
				$newArg = $arg;
				if ($arg['argument'] == lcfirst($resource)) {
					$prop = $arg['type'] ? $arg['type'] : 'id';
					$newArg['uriPart'] = "\${input.{$prop}}";
					$outputs = $this->getInputClass($controller, $action)::bindings()->getOutputs();
					$output = array_first($outputs, function($output) use ($prop) {
						return $output->getKey() == $prop;
					});
					$newArg['type'] = $output->getTypeString();
					$newArg['argument'] = null;
				} else {
					$newArg['uriPart'] = "\${{$arg['argument']}}";
					$newArg['type'] = $newArg['type'] ? $newArg['type'] : 'any';
				}
				$newArgs[] = $newArg;
			}
		} else {
			foreach ($args as $arg) {
				$newArg = $arg;
				$newArg['uriPart'] = "\${{$arg['argument']}}";
				$newArg['type'] = $newArg['type'] ? $newArg['type'] : 'any';
				$newArgs[] = $newArg;
			}
		}

		return $newArgs;
	}

}