<?php

namespace RajeevSiewnath\LumenReact\Library\ResourceBinding;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use RajeevSiewnath\LumenReact\AbstractModel;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractCollection;
use RajeevSiewnath\LumenReact\Http\Resources\AbstractResource;

class ResourceOutput extends AbstractResourceElement {

	private $when = null;
	private $whenLoaded = null;
	private $withCallback = null;
	private $nested = null;
	private $autoType = true;
	private $whenPivotLoaded = null;

	/**
	 * ResourceOutput constructor.
	 * @param $key
	 * @param bool $source
	 */
	public function __construct($key, $source = true) {
		$this->key = $key;
		$this->source = $source;
	}

	/**
	 * @param $key
	 * @param bool $source
	 * @return ResourceOutput
	 */
	public static function make($key, $source = true) {
		return new static($key, $source);
	}

	/**
	 * @return ResourceInput
	 */
	public function generateResourceInput($autoFill = true) {
		$input = new ResourceInput($this->getSourceKey(), $this->key);

		if ($autoFill) {
			if ($this->hasEnumType()) {
				$input->setEnumType(ResourceEnumType::make($this->getEnumType()->getEnum()));
			}
			if ($this->hasNested()) {
				if (is_subclass_of($this->getNested()->getNestedClass(), AbstractCollection::class)) {
					$input->setAutoValidator()->setValidator(['array']);
				}
			}
			$input->setTypeOptional($this->isTypeOptional);
		}

		return $input;
	}

	/**
	 * @param AbstractResource $resource
	 * @param Request $request
	 * @return \Illuminate\Http\Resources\MissingValue|int|mixed|null|string
	 * @throws \Exception
	 */
	public function getValue(AbstractResource $resource, Request $request) {
		if ($this->hasWhen()) {
			return $resource->whenProxy2($this->getWhen()->isWhen($resource, $request), function() use ($resource, $request) {
				return $this->getCalculatedValue($resource, $request);
			});
		} else {
			return $this->getCalculatedValue($resource, $request);
		}
	}

	/**
	 * @param AbstractResource $resource
	 * @param Request $request
	 * @return \Illuminate\Http\Resources\MissingValue|int|mixed|null|string
	 * @throws \Exception
	 */
	private function getCalculatedValue(AbstractResource $resource, Request $request) {
		$return = null;
		$key = $this->getSourceKey();

		if ($this->hasWithCallback()) {
			$this->getWithCallback()->setParams($resource, $request);
			$withKey = $this->getWithCallback()->getWithCallbackKey($key);
			if ($withKey) {
				if ($this->getWithCallback()->hasWithCallback()) {
					$resource->load([$withKey => function($query) use ($resource, $request) {
						call_user_func($this->getWithCallback()->getWithCallback(), $query, $resource, $request);
					}]);
				} else {
					$resource->load($withKey);
				}
			}
		}

		if ($key && $this->hasEnumType()) {
			$return = $this->getEnumType()->getMorphedKey($resource->{$key});
		} else if ($this->hasWhenLoaded()) {
			$return = $resource->whenLoadedProxy($this->getWhenLoaded()->getWhenLoadedKey($key, $resource, $request));
		} else if ($this->getWhenPivotLoaded()) {
			$return = $resource->whenPivotLoadedAsProxy($this->getWhenPivotLoaded()->getTable(), $this->getWhenPivotLoaded()->getAccessor(), call_user_func($this->getWhenPivotLoaded()->getWhenPivotLoadedCallback()->getCallable(), $key, $resource, $request));
		} else if ($this->source instanceof ResourceCallable) {
			$return = call_user_func($this->source->getCallable(), $resource, $request);
		} else if ($key) {
			if (str_contains($key, '.')) {
				$keyParts = explode('.', $key);
				$firstPart = array_shift($keyParts);
				if ($resource->offsetExists($firstPart)) {
					$return = $resource->{$firstPart};
					foreach ($keyParts as $keyPart) {
						if (property_exists($return, $keyPart)) {
							$return = $return->{$keyPart};
						} else {
							$return = null;
							break;
						}
					}
				} else {
					$return = null;
				}
			} else {
				$return = isset($resource->{$key}) ? $resource->{$key} : null;
			}
		}

		if (is_object($return)) {
			if ($this->hasNested() && $this->getNested()->isForceConversion()) {
				$class = $this->getNested()->getNestedClass();
				if (is_subclass_of($class, AbstractResource::class)) {
					$return = new $class($return);
				} else if (is_subclass_of($class, AbstractCollection::class)) {
					$return = new $class(collect($return));
				} else {
					throw new \Exception("Cannot set nested without proper class");
				}
			} else if (is_subclass_of($return, AbstractModel::class)) {
				$return = $return->toResource()->toArray($request);
			} else if (is_object($return) && get_class($return) == Collection::class) {
				if ($return->getQueueableClass()) {
					$return = $return->getQueueableClass()::toCollection($return);
				} else {
					$return = [];
				}
			}
		}

		return $return;
	}

	/**
	 * @param $when
	 * @return ResourceOutput
	 */
	public function setWhen($when) {
		if (!($when instanceof ResourceOutputWhen)) {
			$when = ResourceOutputWhen::make($when);
		}
		$this->when = $when;
		return $this;
	}

	/**
	 * @return ResourceOutputWhen
	 */
	public function getWhen() {
		return $this->when;
	}

	/**
	 * @return array
	 */
	public function hasWhen(): bool {
		return !empty($this->when);
	}

	/**
	 * @param ResourceOutputWhenLoaded|null $when
	 * @return ResourceOutput
	 */
	public function setWhenLoaded($whenLoaded = null) {
		$whenLoaded = $whenLoaded ?? ResourceOutputWhenLoaded::make();
		$this->whenLoaded = $whenLoaded;
		return $this;
	}

	/**
	 * @return ResourceOutputWhenLoaded
	 */
	public function getWhenLoaded() {
		return $this->whenLoaded;
	}

	/**
	 * @return bool
	 */
	public function hasWhenLoaded(): bool {
		return !empty($this->whenLoaded);
	}

	/**
	 * @param ResourceOutputWhenPivotLoaded|null $when
	 * @return ResourceOutput
	 */
	public function setWhenPivotLoaded(ResourceOutputWhenPivotLoaded $whenPivotLoaded) {
		$this->whenPivotLoaded = $whenPivotLoaded;
		return $this;
	}

	/**
	 * @return ResourceOutputWhenPivotLoaded
	 */
	public function getWhenPivotLoaded() {
		return $this->whenPivotLoaded;
	}

	/**
	 * @return bool
	 */
	public function hasWhenPivotLoaded(): bool {
		return !empty($this->whenPivotLoaded);
	}

	/**
	 * @param ResourceOutputWithCallback $withCallback
	 * @return ResourceOutput
	 */
	public function setWithCallback($withCallback = null) {
		$withCallback = $withCallback ?? ResourceOutputWithCallback::make();
		$this->withCallback = $withCallback;
		return $this;
	}

	/**
	 * @return ResourceOutputWithCallback
	 */
	public function getWithCallback(): ResourceOutputWithCallback {
		return $this->withCallback;
	}

	/**
	 * @return bool
	 */
	public function hasWithCallback(): bool {
		return !empty($this->withCallback);
	}

	/**
	 * @return string
	 */
	public function getWithCallbackKey(): string {
		if ($this->withCallback) {
			return $this->getWithCallback()->getWithCallbackKey($this->getSourceKey());
		}
	}

	/**
	 * @param ResourceOutputNested|string $nested
	 * @return ResourceOutput
	 */
	public function setNested($nested) {
		if (!($nested instanceof ResourceOutputNested)) {
			$nested = ResourceOutputNested::make($nested);
		}
		$this->nested = $nested;
		return $this;
	}

	/**
	 * @return ResourceOutputNested
	 */
	public function getNested() {
		return $this->nested;
	}

	/**
	 * @return bool
	 */
	public function hasNested(): bool {
		return !empty($this->nested);
	}

	/**
	 * @param bool $autoType
	 * @return ResourceOutput
	 */
	public function setAutoType(bool $autoType = false): ResourceOutput {
		$this->autoType = $autoType;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function isAutoType(): bool {
		return $this->autoType;
	}

	/**
	 * @param $resourceClass
	 * @return mixed|null
	 */
	public function generateAutoType($resourceClass) {
		$empty = $resourceClass::modelClass()::make();
		$key = $this->getSourceKey();
		if ($this->hasNested()) {
			return $this->getNested()->getNestedClass()::getType();
		} else if ($this->hasEnumType()) {
			return $this->getEnumType()->getEnum();
		} else if ($empty instanceof AbstractModel) {
			$type = $empty->getCastType($key);
			if ($empty->getPrimaryKeyKey() == $key) {
				return 'number';
			} else if (in_array($key, [$empty->getCreatedAtColumn(), $empty->getUpdatedAtColumn()]) || (method_exists($empty, 'getDeletedAtColumn') && $key == $empty->getDeletedAtColumn())) {
				return 'Date';
			} else if ($type) {
				return self::getTypeFromCast($type);
			}
		}
		return null;
	}

	public static function getTypeFromCast($type) {
		switch ($type) {
			case 'int':
			case 'integer':
			case 'real':
			case 'float':
			case 'double':
				return 'number';
			case 'bool':
			case 'boolean':
				return 'boolean';
			case 'object':
			case 'json':
				return '{ [key: string]: any }';
			case 'array':
			case 'collection':
				return 'Array<any>';
			case 'date':
			case 'datetime':
			case 'timestamp':
				return 'Date';
			case 'string':
			default:
				return 'string';
		}
	}

}