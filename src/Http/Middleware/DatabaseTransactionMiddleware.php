<?php

namespace RajeevSiewnath\LumenReact\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class DatabaseTransactionMiddleware {

	/**
	 * Create a new middleware instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Factory $auth
	 * @return void
	 */
	public function __construct() {
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 * @return mixed
	 * @throws \Exception
	 */
	public function handle($request, Closure $next) {
		DB::beginTransaction();
		$response = $next($request);
		if (!$request->has('noCommit') || !$request->input('noCommit')) {
			try {
				DB::commit();
			} catch (\Exception $exception) {
				DB::rollback();
				throw $exception;
			}
		}
		return $response;
	}
}
