<?php

namespace RajeevSiewnath\LumenReact\Library\RouterEntry;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use RajeevSiewnath\LumenReact\Library\GeoIP\GeoIP;

class RouteEntryHealth extends AbstractRouterEntry {

	/**
	 * @param Request $request
	 * @param $router
	 * @return array|mixed
	 */
	public function doRouteAction(Request $request, $router) {
		$health = [];
		try {
			$result = DB::select('SELECT DATABASE() as db, CURRENT_USER() as user');
			if ($result[0]->db !== env('DB_DATABASE')) {
				throw new \Exception('db name mismatch');
			}
			$dbUser = env('DB_USERNAME');
			preg_match("/(.*)@.*/", $result[0]->user, $userMatch);
			if ($userMatch[1] !== $dbUser) {
				throw new \Exception('db user mismatch');
			}
			$health['database'] = 'OK';
		} catch (\Exception $e) {
			$health['database'] = 'ERROR';
		}
		try {
			$geoIP = app(GeoIP::class);
			$geoIPData = $geoIP->get('8.8.8.8');
			if ($geoIPData->getCountryIso() != 'US') {
				throw new \Exception('google dns iso mismatch');
			}
			$health['geo'] = 'OK';
		} catch (\Exception $e) {
			$health['geo'] = 'ERROR';
		}
		try {
			$id = uniqid('test_');
			Storage::put($id, 'test');
			Storage::get($id);
			Storage::url($id);
			Storage::delete($id);
			$health['fs'] = 'OK';
		} catch (\Exception $e) {
			$health['fs'] = 'ERROR';
		}

		return $health;
	}

	/**
	 * @param $data
	 * @return bool
	 */
	public function isError($data) {
		foreach ($data as $key => $s) {
			if ($s == 'ERROR') {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return string
	 */
	public function getRouteName() {
		return 'health';
	}

	/**
	 * @return array
	 */
	public function getMiddlewares(): array {
		return ['geoIP'];
	}

}