<?php

namespace RajeevSiewnath\LumenReact\Providers;

use Illuminate\Support\ServiceProvider;
use RajeevSiewnath\LumenReact\Http\Middleware\OAuthMiddleware;
use RajeevSiewnath\LumenReact\Library\ResponseScope\ResponseScope;

class OAuthServiceProvider extends ServiceProvider {

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
	}

	/**
	 * Boot the authentication services for the application.
	 *
	 * @return void
	 */
	public function boot(ResponseScope $responseScope) {
		$this->app->routeMiddleWare([
			'oAuth' => OAuthMiddleware::class,
		]);
	}
}

